# Loyals Club API

- [Loyals Club API](#loyals-club-api)
  - [Requirements](#requirements)
  - [Deploy to AWS](#deploy-to-aws)
    - [Elastic Beanstalk](#elastic-beanstalk)
    - [Environment Deploy](#environment-deploy)
    - [Database Creation](#database-creation)
  - [DNS](#dns)
  - [Email Microservice](#email-microservice)
    - [SES](#ses)
      - [**Get contact list details**](#--get-contact-list-details--)

## Requirements

---

The core server is built using GraphQl on top of a NodeJs server. It's hosted on the AWS infraestructure.

- Mysql 5.7.26
- Node v10.16.3
- Npm 6.9.0

## Deploy to AWS

---

For deploying to AWS it's neccesary to have the corresponding key-pair credentials for AWS-CLI in the computer.

### Elastic Beanstalk

---

ElasticBeanstalk is the tool used to deploy the core code.
The configuration for the environment is placed under ./elasticbeanstalk/saved_configs/(loyalsclub-api.cfg.yml).

If the environment has not been created (Its not showing on the AWS WEB Console) then run:

```
eb create (environment-name) --cfg loyalsclub-api -c (subdomain)
```

- After the environment gets created, a security group (sc-XXXX) will be assigned to the EC2 instance.\*

### Environment Deploy

---

If the environment is created:

```
eb deploy environment-name
```

### Database Creation

---

AWS RDS is used to create the Database Environment. A Cloudformation template has been created for this purpose.
The template can be found in the S3 bucket,
_https://s3-sa-east-1.amazonaws.com/cf-templates-v3xfdai7e70e-sa-east-1/202004439F-loyalsdb5ipq6ahh6dl_

Follow instructions in the Cloudformation AWS Web panel for deployment.

Its **IMPORTANT** that when selecting the security-group the one from EB gets choosen. This will grant access to the RDS environment for
the EC2 instance.

Notice its **IMPORTANT** to update the EB Environment variables from the Environment Console.

## DNS

---

DNS are managed by GoDaddy under nocetti.tomas@gmail.com account.
SSL Certificate for the api is provided by AWS using de LoadBalancer.

api.loyals.club ---> loyalsclub-api.sa-east-1.elasticbeanstalk.com

## Email Microservice

---

The API uses AWS SNS Service to publish messages, and then a lambda function that acts as a microservice gets called.
It's important for the IAM User Role to have permission to publish SNS. Otherwise emails will not work.

### SES

---

We are currently using AWS SES V2 to handle customers related emails.

You can interact with this API using the CLI and/or the SDK.

We have made available the following npm commands that uses the SDK:

- `ses-update-template`: creates or updates the template. It asks for the name of the corresponding config on `/emails/templates`.
- `ses-update-contact-list`: creates or update a contact list. It asks for the name of the corresponding config on `/emails/contact-lists`. Bare in mind that AWS allows the creation of a unique contact list per account.

Below you can find some useful commands when working with this:

#### Get contact list details

```sh
aws sesv2 get-contact-list --contact-list-name LOYALS_CLUB_USERS
```
