const { Expo } = require('expo-server-sdk');

class ExpoPushNotifications {
  constructor() {
    this.expo = Expo;

    this.connection = new Expo({
      accessToken: process.env.EXPO_ACCESS_TOKEN,
    });
  }

  async send(messages) {
    const notifications = this.parseNotifications(messages);

    const tickets = await this.sendNotifications(notifications);

    this.checkNotificationsReceipt(tickets);
  }

  parseNotifications(messages) {
    const notifications = [];

    for (const { pushToken, ...message } of messages) {
      const isValidToken = this.expo.isExpoPushToken(pushToken);

      if (isValidToken) {
        notifications.push({
          to: pushToken,
          sound: 'default',
          'content-available': 1,
          ...message,
        });
      }
    }

    return notifications;
  }

  async sendNotifications(notifications) {
    const chunks = this.connection.chunkPushNotifications(notifications);
    const tickets = [];
    const errors = [];

    await Promise.all(
      chunks.map(async chunk => {
        try {
          const ticketChunk = await this.connection.sendPushNotificationsAsync(chunk);

          console.log('[Notifications:Success] Notification Sent', chunk, ticketChunk);

          tickets.push(...ticketChunk);
        } catch (error) {
          console.error('[Notifications:Error] sendNotifications exception', error);
          errors.push(error);
        }
      }),
    );

    return [tickets, errors];
  }

  checkNotificationsReceipt(tickets) {
    const receiptIds = [];

    for (const ticket of tickets) {
      if (ticket.id) {
        receiptIds.push(ticket.id);
      }
    }

    const receiptIdChunks = this.connection.chunkPushNotificationReceiptIds(receiptIds);

    return Promise.all(
      receiptIdChunks.map(async chunk => {
        try {
          const receipts = await this.connection.getPushNotificationReceiptsAsync(chunk);

          for (const receiptId of Object.keys(receipts)) {
            const { status, message, details } = receipts[receiptId];

            if (status === 'error') {
              console.error('[Notifications:Error] checkNotificationsReceipt status', message);

              if (details && details.error) {
                console.error('[Notifications:Error] checkNotificationsReceipt error code:', details.error);
              }

              return false;
            }
          }
        } catch (error) {
          console.error('[Notifications:Error] checkNotificationsReceipt exception', error);
          return false;
        }

        return true;
      }),
    );
  }
}

module.exports = ExpoPushNotifications;
