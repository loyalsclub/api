const Sentry = require('@sentry/node');

const ExpoPushNotifications = require('./expo/expo-push-notifications.service');

const PUSH_NOTIFICATIONS_ENABLED = process.env.PUSH_NOTIFICATIONS_ENABLED === 'true';

class PushNotificationsService {
  constructor() {
    this.pushNotificationsService = new ExpoPushNotifications();
  }

  async send(messages) {
    if (!PUSH_NOTIFICATIONS_ENABLED) {
      console.debug(messages);
      return true;
    }

    let response;

    try {
      response = await this.pushNotificationsService.send(messages);
    } catch (err) {
      console.error('[Notifications:Error] send exception', messages, err);

      Sentry.captureException(err.originalError || err);
    }

    return response;
  }

  sendOne(message) {
    return this.send([message]);
  }
}

module.exports = new PushNotificationsService();
