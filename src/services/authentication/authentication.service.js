const admin = require('firebase-admin');

const serviceAccount = require('@config/service-account.json');

const serviceAccountConfig = serviceAccount[process.env.NODE_ENV] || serviceAccount.development;

const isFirebaseAppInitialized = admin.apps.length > 0;

if (!isFirebaseAppInitialized) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccountConfig),
  });
}

module.exports = admin;
