const moment = require('moment');

const { USAGE_FREQUENCIES, FOREVER_FREQUENCY_MIN_VALID_HOURS } = require('@modules/core/constants/core.constants');

const { FOREVER, EVERY_CERTAIN_DAYS, EVERY_CERTAIN_HOURS, CERTAIN_WEEK_DAYS } = USAGE_FREQUENCIES;

class UsageValidatorService {
  isValidToRequestNow({ usageFrequency, lastTimeUsed }) {
    usageFrequency = usageFrequency || {};

    let isValid = true;

    if (lastTimeUsed && lastTimeUsed.createdAt) {
      const nextTime = this.getNextTimeAvailable(usageFrequency, lastTimeUsed.createdAt);

      isValid = moment().isSameOrAfter(nextTime);
    }

    const isCertainWeekDays = usageFrequency.type === CERTAIN_WEEK_DAYS;

    if (isValid && isCertainWeekDays) {
      const currentWeekday = moment().isoWeekday();
      isValid = usageFrequency.value.includes(currentWeekday);
    }

    return isValid;
  }

  getNextTimeAvailable({ type, value }, lastTimeUsed) {
    if (!lastTimeUsed) {
      if (type === CERTAIN_WEEK_DAYS) {
        const weekday = moment().isoWeekday();
        const values = value.split(',').map(Number).sort();
        const index = values.indexOf(weekday);

        let nextWeekDay = index >= 0 ? values[index] : values.find(v => v > weekday);

        if (!nextWeekDay) {
          nextWeekDay = values[0] + 7;
        }

        return moment().isoWeekday(nextWeekDay).startOf('date').toISOString();
      }

      return moment().startOf('date').toISOString();
    }

    if (type === FOREVER) {
      return moment(lastTimeUsed).add(FOREVER_FREQUENCY_MIN_VALID_HOURS, 'h').startOf('date').toISOString();
    }

    if (type === CERTAIN_WEEK_DAYS) {
      const weekday = moment(lastTimeUsed).isoWeekday();
      const values = value.split(',').map(Number).sort();
      const index = values.indexOf(weekday);
      const nextWeekDay = values[index + 1] || values[0] + 7;

      return moment(lastTimeUsed).isoWeekday(nextWeekDay).startOf('date').toISOString();
    }

    if (type === EVERY_CERTAIN_DAYS) {
      return moment(lastTimeUsed).add(value, 'd').startOf('date').toISOString();
    }

    if (type === EVERY_CERTAIN_HOURS) {
      return moment(lastTimeUsed).add(value, 'h').toISOString();
    }

    return '';
  }

  wasUsedToday(lastTimeUsed) {
    return moment().isSame(lastTimeUsed, 'day');
  }
}

const usageValidatorService = new UsageValidatorService();

module.exports = { usageValidatorService };
