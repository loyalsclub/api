const AWS = require('aws-sdk');

const emailsConstants = require('@modules/core/constants/emails.constants');

AWS.config.update({ region: 'sa-east-1' });

const SNS_TOPIC_ARN = process.env.EMAIL_SNS_TOPIC_ARN;
const EMAILS_ENABLED = process.env.EMAILS_ENABLED == 'true';
const SNS = new AWS.SNS({ apiVersion: '2010-03-31' });

if (!EMAILS_ENABLED) {
  SNS.publish = message => ({
    promise: async () => console.log(message),
  });
}

const MESSAGES = {
  ADD_PARTNER_TO_LIST: 'ADD_PARTNER_TO_LIST',
  ADD_USER_TO_LIST: 'ADD_USER_TO_LIST',
  DISCOUNT_EMAIL: 'DISCOUNT_EMAIL',
  PARTNER_WELCOME_EMAIL: 'PARTNER_WELCOME_EMAIL',
  USER_INVITATION_EMAIL: 'USER_INVITATION_EMAIL',
  USER_PARTNER_WELCOME_EMAIL: 'USER_PARTNER_WELCOME_EMAIL',
  USER_WELCOME_EMAIL: 'USER_WELCOME_EMAIL',
};

const publishSnsMessage = (message, isSesMessage = false) => async attributes => {
  const params = _messageBuilder({ message, attributes, isSesMessage });

  try {
    return await SNS.publish(params).promise();
  } catch (error) {
    console.error(`[Email ${message}]: ${error}`);
    console.error(`[Email ${message}]: ${JSON.stringify(params)}`);
  }
};

const publishSesMessage = message => ({ DATA, ...params }) => {
  const publish = publishSnsMessage(message, true);

  return publish({
    ...params,
    CONTACT_LIST_NAME: emailsConstants.CONTACT_LIST_NAME,
    TEMPLATE_DATA: JSON.stringify(DATA),
  });
};

const _messageBuilder = ({ message, attributes = {}, isSesMessage }) => {
  const params = {
    Message: message,
    MessageAttributes: {
      SES: {
        DataType: 'String',
        StringValue: String(isSesMessage),
      },
    },
    TopicArn: SNS_TOPIC_ARN,
  };

  for (const prop in attributes) {
    params.MessageAttributes[prop] = {
      DataType: 'String',
      StringValue: String(attributes[prop]),
    };
  }

  return params;
};

/**
 * Attributes need to be sent in an object
 * {
    FIRST_NAME: 'Tomas',
    LAST_NAME: 'Nocetti',
    EMAIL_TO: 'nocetti.tomas@gmail.com',
 * }
 */
const sendUserInvitationEmail = publishSnsMessage(MESSAGES.USER_INVITATION_EMAIL);

/**
 * Attributes need to be sent in an object
 * {
    FIRST_NAME: 'Tomas',
    LAST_NAME: 'Nocetti',
    EMAIL_TO: 'nocetti.tomas@gmail.com',
 * }
 */
const sendUserWelcomeEmail = publishSnsMessage(MESSAGES.USER_WELCOME_EMAIL);

/**
 * Attributes need to be sent in an object
 * {
    FIRST_NAME: 'Tomas',
    LAST_NAME: 'Nocetti',
    EMAIL_TO: 'nocetti.tomas@gmail.com'
 * }
 */
const sendUserPartnerWelcomeEmail = publishSnsMessage(MESSAGES.USER_PARTNER_WELCOME_EMAIL);

/**
 * Attributes need to be sent in an object
 * {
    FIRST_NAME: 'Tomas',
    LAST_NAME: 'Nocetti',
    EMAIL_TO: 'nocetti.tomas@gmail.com',
    STORE_NAME: 'Tuckson'
    STORE_PROFILE_IMAGE: 'https://lc.club/store-profile-images/1556376630905.png'
 * }
 */
const sendPartnerWelcomeEmail = publishSnsMessage(MESSAGES.PARTNER_WELCOME_EMAIL);

/**
 * Attributes need to be sent in an object
 * {
    FIRST_NAME: 'Tomas',
    LAST_NAME: 'Nocetti',
    EMAIL: 'nocetti.tomas@gmail.com',
    STORE_NAME: 'Tuckson'
 * }
 */
const addPartnerToList = publishSnsMessage(MESSAGES.ADD_PARTNER_TO_LIST);

/**
 * Attributes need to be sent in an object
 * {
    FIRST_NAME: 'Tomas',
    LAST_NAME: 'Nocetti',
    EMAIL: 'nocetti.tomas@gmail.com',
 * }
 */
const addUserToList = publishSnsMessage(MESSAGES.ADD_USER_TO_LIST);

/**
 * Attributes need to be sent in an object
 * {
 *    EMAIL_TO: 'graziano.ramiro@gmail.com',
      DATA: {
        CUSTOMER_FIRST_NAME: 'Ramiro',
        STORE_NAME: 'Lo Pibe',
        POINTS_AMOUNT: '253',
        STORE_PROFILE_IMAGE: 'https://files.loyals.club/eyJidWNrZXQiOiJsb3lhbHNjbHViLWZpbGVzLnRlc3RpbmciLCJrZXkiOiJ0ZXN0aW5nLWZpbGVzL3BpenplcmlhLWdvbWV6LmpwZyIsImVkaXRzIjp7InJlc2l6ZSI6eyJ3aWR0aCI6Mjg4LCJoZWlnaHQiOjI4OCwiZml0IjoiZmlsbCJ9fX0=',
        STORE_URL: 'https://loyals.club/tu.comercio',
      }
    }
 */
const sendCustomerPointsLoadedEmail = publishSesMessage(emailsConstants.TEMPLATES.POINTS_LOADED);

/**
 * Attributes need to be sent in an object
 * {
 *    EMAIL_TO: 'graziano.ramiro@gmail.com',
      DATA: {
        CUSTOMER_FIRST_NAME: 'Ramiro',
        STORE_NAME: 'Lo Pibe',
        STORE_PROFILE_IMAGE: 'https://files.loyals.club/eyJidWNrZXQiOiJsb3lhbHNjbHViLWZpbGVzLnRlc3RpbmciLCJrZXkiOiJ0ZXN0aW5nLWZpbGVzL3BpenplcmlhLWdvbWV6LmpwZyIsImVkaXRzIjp7InJlc2l6ZSI6eyJ3aWR0aCI6Mjg4LCJoZWlnaHQiOjI4OCwiZml0IjoiZmlsbCJ9fX0=',
        STORE_URL: 'https://loyals.club/tu.comercio',
        STORE_REWARDS: [{
          title: '1 Gaseosa de Regalo',
          usageFrequency: 'Martes a Jueves',
          description: ''Comprando tres pizzas,
        }],
      }
    }
 */
const sendCustomerWelcomeEmail = publishSesMessage(emailsConstants.TEMPLATES.WELCOME_CUSTOMER);

module.exports = {
  addPartnerToList,
  addUserToList,
  sendPartnerWelcomeEmail,
  sendUserInvitationEmail,
  sendUserPartnerWelcomeEmail,
  sendUserWelcomeEmail,
  sendCustomerPointsLoadedEmail,
  sendCustomerWelcomeEmail,
};
