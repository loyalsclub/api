var AWS = require('aws-sdk');
AWS.config.update({ region: 'sa-east-1' });
const S3 = new AWS.S3();

const IMAGE_CONFIG = {
  PATH_STORE_PROFILE_IMAGE: 'store-profile-images/',
  PATH_QR_PROFILE_IMAGE: 'qr-codes/',
};

const AWS_CONFIG = {
  STORE_PROFILE_IMAGE: {
    Bucket: process.env.AWS_FILES_BUCKET,
    ACL: 'public-read',
  },
  QR_IMAGE: {
    Bucket: process.env.AWS_FILES_BUCKET,
    ACL: 'public-read',
  },
};

/**
 * Attributes need to be sent in an object {mimeType, stream}
 * Returns: {Key: (relative-path)}
 */
const uploadStoreProfileImage = ({ mimetype, stream, name = +new Date() }) => {
  const extension = '.' + mimetype.split('/').pop();
  const Key = IMAGE_CONFIG.PATH_STORE_PROFILE_IMAGE + name + extension;

  let params = {
    ...AWS_CONFIG.STORE_PROFILE_IMAGE,
    Key,
    ContentType: mimetype,
    Body: stream,
  };

  return _genericUpload(params);
};

/**
 * Attributes need to be sent in an object {mimeType, stream}
 * Returns: {Key: (relative-path)}
 */
const uploadQrImage = ({ mimetype, stream, name = +new Date() }) => {
  const extension = '.' + mimetype.split('/').pop();
  const Key = IMAGE_CONFIG.PATH_QR_PROFILE_IMAGE + name + extension;

  let params = {
    ...AWS_CONFIG.QR_IMAGE,
    Key,
    ContentType: mimetype,
    Body: stream,
  };

  return _genericUpload(params);
};

const _genericUpload = params => {
  return new Promise((res, rej) => {
    /** The difference between s3.putobject and s3.upload is that upload handles unsized streams */
    S3.upload(params, function (err, data) {
      if (err) {
        return rej(err);
      } else {
        return res({ key: data.Key });
      }
    });
  });
};

module.exports = { uploadQrImage, uploadStoreProfileImage };
