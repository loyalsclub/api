const fetch = require('node-fetch');

const { GOOGLE_API_KEY, GOOGLE_SHORT_LINKS_API, DYN_LINK_BASE } = process.env;

class DynamicLinkService {
  constructor() {
    this.retries = 0;
    this.maxRetries = 5;
  }

  generateLongLink({ url, title, image, description }) {
    const baseLink = `${DYN_LINK_BASE}/?link=${url}`;

    let params = '';

    if (title) {
      params += `&st=${title}`;
    }

    if (image) {
      params += `&si=${image}`;
    }

    if (description) {
      params += `&sd=${description}`;
    }

    return baseLink + params;
  }

  async generateShortLink(longDynamicLink) {
    let response;

    try {
      response = await fetch(`${GOOGLE_SHORT_LINKS_API}?key=${GOOGLE_API_KEY}`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          longDynamicLink,
          suffix: {
            option: 'SHORT',
          },
        }),
      }).then(response => response.json());
    } catch (err) {
      const hasRemainingRetries = this.retries < this.maxRetries;

      if (hasRemainingRetries) {
        this.retries++;

        return new Promise(resolve => {
          setTimeout(() => {
            this.generateShortLink(longDynamicLink).then(resolve);
          }, 100);
        });
      }

      response = { shortLink: longDynamicLink };
    }

    this.retries = 0;

    return response.shortLink;
  }
}

const dynamicLinkService = new DynamicLinkService();

module.exports = { dynamicLinkService };
