const { Transform } = require('stream');

const QRCode = require('qrcode');

const { uploadQrImage } = require('@services/images.service');

/**
 * Attributes need to be sent TEXT TO ENCODE
 * Returns: {Key: (relative-path)}
 */
const generateQrUrl = text => {
  const inoutStream = new Transform({
    transform(chunk, encoding, callback) {
      this.push(chunk);
      callback();
    },
  });

  QRCode.toFileStream(inoutStream, text, {
    width: 250,
    margin: 2,
  });

  return uploadQrImage({
    stream: inoutStream,
    mimetype: 'image/png',
    name: text + '-' + +new Date(),
  });
};

module.exports = { generateQrUrl };
