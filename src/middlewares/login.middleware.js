const { AuthenticationError } = require('apollo-server');
const { BaseError } = require('sequelize');

const db = require('@db');
const { User } = require('@models');
const admin = require('@services/authentication/authentication.service');

const login = async req => {
  const res = {
    reqUser: function () {
      if (!this.user) {
        console.log('[DEBUG] Authentication Error on reqUser', req);
        throw new AuthenticationError();
      }
      return this.user;
    },
  };

  const headers = req.headers;

  if (!headers.authorization) {
    return res;
  }

  const token = headers.authorization.replace('Bearer ', '');

  try {
    const fbUser = await admin.auth().verifyIdToken(token);
    const dbUser = await db.User.findOne({ where: { uid: fbUser.uid } });

    res.user = new User(fbUser, dbUser);
  } catch (err) {
    console.log('[DEBUG] Authorization Header', headers.authorization);
    console.log('[DEBUG] Token', token);

    if (err instanceof BaseError) {
      console.log('[DEBUG] Catch BaseError', err);
      throw err; // Database related.
    }

    console.log('[DEBUG] Catch AuthenticationError', err);
    throw new AuthenticationError();
  }

  return res;
};

module.exports = login;
