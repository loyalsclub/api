const util = require('util');

const Sentry = require('@sentry/node');
const { AuthenticationError, ApolloError } = require('apollo-server');
const { BaseError } = require('sequelize');

const { sequelizeValidationErrorParser } = require('@utils/errors-parser');

const errorMiddleWare = err => {
  console.log('[DEBUG] Error Middleware: err', JSON.stringify(err || {}));
  console.log('[DEBUG] Error Middleware: originalErr', JSON.stringify(err.originalError || {}));
  console.log('[DEBUG] Error Middleware: isAuthErr', err instanceof AuthenticationError);
  console.log('[DEBUG] Error Middleware: isAuthErr 2', err.originalError instanceof AuthenticationError);

  let clientErr = new ApolloError(err.message, 'INTERNAL_SERVER_ERROR');

  const isAuthenticationError =
    err.originalError instanceof AuthenticationError || err.originalError instanceof ApolloError;

  if (isAuthenticationError) {
    console.log('[DEBUG] isAuthenticationError', isAuthenticationError);
    clientErr = err;
  }

  const isDatabaseRelated = err instanceof BaseError || err.originalError instanceof BaseError;

  if (isDatabaseRelated) {
    console.log('[DEBUG] isDatabaseRelated', isDatabaseRelated);
    clientErr = sequelizeValidationErrorParser(err.originalError || err);
  }

  const isUnhandledError =
    clientErr.message.startsWith('server') || clientErr.extensions.code === 'INTERNAL_SERVER_ERROR';

  if (isUnhandledError) {
    console.log('[DEBUG] isUnhandledError', isUnhandledError);
    console.error('ERROR GRAVE  -----> ', util.inspect(err, { depth: null }));
    Sentry.captureException(err.originalError || err);
  }

  return clientErr;
};

module.exports = errorMiddleWare;
