const { ApolloError } = require('apollo-server');
const R = require('ramda');
const Sequelize = require('sequelize');

const { FIREBASE_AUTH_ERRORS, SERVER_ERROR } = require('@modules/core/constants/error-codes.constants');

const { DatabaseError, ValidationError, ConnectionError } = Sequelize;

const sequelizeValidationErrorParser = error => {
  if (error instanceof ValidationError) {
    let errorsArray = [];
    R.reduce(
      (array, error) => {
        const message = R.lens(R.prop('message'), R.assoc('message'));
        const val = R.compose(R.over(message, R.replace(/ /g, '-')), R.pick(['message', 'path']))(error);
        array.push(val);
      },
      errorsArray,
      error.errors,
    );
  }

  if (error instanceof ConnectionError) {
    const err = SERVER_ERROR['server/database-connection'];
    return new ApolloError(err.msg, err.code);
  }

  if (error instanceof DatabaseError) {
    const err = SERVER_ERROR['server/database-error'];
    return new ApolloError(err.msg, err.code);
  }
};

const firebaseValidationErrorParser = error => {
  let obj = {
    message: error.errorInfo.code,
  };
  switch (error.errorInfo.code) {
    case FIREBASE_AUTH_ERRORS['auth/email-already-exists']:
      obj.path = ['email'];
      break;
  }
  return JSON.stringify([obj]);
};

module.exports = { firebaseValidationErrorParser, sequelizeValidationErrorParser };
