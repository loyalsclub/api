const { ApolloError } = require('apollo-server');

const { USER_ROLES } = require('@modules/core/constants/core.constants');
const { API_AUTH_ERRORS, API_STORE_ERRORS } = require('@modules/core/constants/error-codes.constants');

/** Void: Throws exception if role is not valid.
 *  'auth/user-is-not-authorized'
 */
const validateIsAdmin = user => {
  if (user.role === USER_ROLES.ADMIN) return;

  const error = API_AUTH_ERRORS['auth/user-is-not-authorized'];
  throw new ApolloError(error.msg, error.code);
};

const validateIsDemoStore = storeId => {
  if (isDemoStore(storeId)) {
    const error = API_STORE_ERRORS['api/store-modification-forbidden'];
    throw new ApolloError(error.msg, error.code);
  }
};

const isDemoStore = storeId => storeId <= 6;

module.exports = {
  validateIsAdmin,
  validateIsDemoStore,
  isDemoStore,
};
