const { unflatten } = require('./array-utils');

const history = async ({ db, id }) => {
  const data = await db.sequelize.query(
    `
    SELECT 
      'CouponTransaction' AS 'Action', 
      CouponTransactions.id, 
      CouponTransactions.customerId, 
      CouponTransactions.storeId, 
      CouponTransactions.createdAt, 
      CouponTransactions.updatedAt, 
      CouponTransactions.amount,
      CouponTransactions.couponCode, 
      Store.id AS 'Store.id',
      Store.name AS 'Store.name',
      Store.profileImage AS 'Store.profileImage'
    FROM CouponTransactions as CouponTransactions
    LEFT JOIN Stores AS Store ON CouponTransactions.storeId = Store.id
    WHERE customerId in (
      SELECT @ids := (SELECT GROUP_CONCAT(id SEPARATOR ',')
      FROM CustomerProfiles AS CustomerProfile  WHERE 
      ((CustomerProfile.deletedAt > :profileDeletedAt OR CustomerProfile.deletedAt IS NULL) 
      AND (CustomerProfile.userId = :id AND CustomerProfile.status = 'active')))
    )

    UNION ALL 
    SELECT 
      'Point',
      Points.id,  
      Points.customerId, 
      Points.storeId, 
      Points.createdAt, 
      Points.updatedAt, 
      Points.amount,
      null, 
      Store.id AS 'Store.id',
      Store.name AS 'Store.name',
      Store.profileImage AS 'Store.profileImage'
    FROM Points as Points
    LEFT JOIN Stores AS Store ON Points.storeId = Store.id
    WHERE customerId in (@ids)
    ORDER BY updatedAt DESC LIMIT 5;`,
    {
      type: db.sequelize.QueryTypes.SELECT,
      replacements: {
        id,
        profileDeletedAt: new Date().toISOString(),
      },
    },
  );

  const parsedData = [];
  for (let el of data) {
    parsedData.push(unflatten(el));
  }
  return parsedData;
};

module.exports = { history };
