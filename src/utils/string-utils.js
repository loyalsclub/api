const { USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');

function generateRandomAlphanumericString(longitude) {
  const limit = longitude || 9;
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let string = '';

  for (let i = 0; i < limit; i++) {
    string += characters.charAt(Math.floor(Math.random() * characters.length));
  }

  return string;
}

function escapeSpecialChars(string) {
  return string
    .replace(/[ÀÁÂÃÄÅ]/g, 'a')
    .replace(/[àáâãäå]/g, 'a')
    .replace(/[ÈÉÊË]/g, 'e')
    .replace(/[èéêëēėę]/g, 'e')
    .replace(/[ÎÏÍĪĮÌ]/g, 'i')
    .replace(/[îïíīįì]/g, 'i')
    .replace(/[ÔÖÒÓŒØŌÕ]/g, 'o')
    .replace(/[ôöòóœøōõ]/g, 'o')
    .replace(/[ÛÜÙÚŪ]/g, 'u')
    .replace(/[ûüùúū]/g, 'u')
    .replace(/[^a-zA-Z0-9-_\s]/g, '');
}

function getFrequency(UsageFrequency = {}) {
  const FREQUENCY_TYPE = {};
  FREQUENCY_TYPE[USAGE_FREQUENCIES.FOREVER] = 'Siempre disponible';
  FREQUENCY_TYPE[USAGE_FREQUENCIES.ONE_TIME] = 'Disponible para uso único';
  FREQUENCY_TYPE[USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS] = `Disponible cada ${Number(UsageFrequency.value)} días`;
  FREQUENCY_TYPE[USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS] = `Disponible cada ${Number(UsageFrequency.value)} horas`;

  if (USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS === UsageFrequency.type) {
    const days = UsageFrequency.value.split(',');
    const weekdays = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábados', 'Domingos'];

    if (days.length === 1) {
      return `Disponible los días ${weekdays[Number(days[0]) + 1]}`;
    }

    if (days.length > 1) {
      // To format as 'Lunes a Viernes'
      const hasFormat = days.every((val, index) => {
        if (index + 1 === days.length) return true;
        if (Number(val) + 1 === Number(days[index + 1])) return true;
        return false;
      });

      if (hasFormat && days.length > 2) {
        const startDay = weekdays[Number(days[0]) - 1];
        const endDay = weekdays[Number(days[days.length - 1]) - 1];
        return `Disponible de ${startDay} a ${endDay}`;
      }

      let ret = 'Disponible los ';

      ret += days
        .map((value, index) => {
          const day = weekdays[Number(value) - 1];
          if (index === 0) return `${day}`;
          if (index + 1 === days.length) return ` y ${day}`;
          return `, ${day}`;
        })
        .join('');

      return ret;
    }
  }

  if (!UsageFrequency || !UsageFrequency.type || !FREQUENCY_TYPE[UsageFrequency.type]) return 'Disponible';

  return FREQUENCY_TYPE[UsageFrequency.type];
}

module.exports = { generateRandomAlphanumericString, escapeSpecialChars, getFrequency };
