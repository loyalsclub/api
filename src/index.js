const Sentry = require('@sentry/node');
const { ApolloServer, ApolloError } = require('apollo-server');

const db = require('@db');

const { typeDefs, resolvers } = require('./app.graphql');
const errorMiddleware = require('./middlewares/error.middleware');
const loginMiddleware = require('./middlewares/login.middleware');

const PORT = process.env.PORT || 3001;

Sentry.init({
  dsn: 'https://bd16ba02144a4aba99d4b6733edf60a8@o383073.ingest.sentry.io/5212872',
  environment: process.env.NODE_ENV,
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  formatError: errorMiddleware,
  engine: {
    apiKey: 'service:tomasnocetti-9669:9GqZ1zsAXXuZp_AXz7bdFQ',
  },
  context: async ({ req }) => {
    return {
      ...(await loginMiddleware(req)),
    };
  },
});

// The GraphQL endpoint
server.listen(PORT).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}:${PORT}`);
  console.log('Go to http://localhost:3000/graphiql to run queries!');
  console.log(`${process.env.EMAILS_ENABLED === 'true' ? '✅' : '❌'} EMAILS`);
  console.log(`${process.env.PUSH_NOTIFICATIONS_ENABLED === 'true' ? '✅' : '❌'} PUSH NOTIFICATIONS`);

  db.sequelize
    .authenticate()
    .then(() => {
      console.log('✅ DATABASE CONNECTION');
    })
    .catch(err => {
      console.log('❌ DATABASE CONNECTION');
      console.error(err);
      throw new ApolloError();
      // throw Error
    });
});
