const Admin = require('@modules/admin/admin.model');
const NotificationManager = require('@modules/core/notification-manager.model');
const Coupon = require('@modules/coupon/coupon.model');
const Customer = require('@modules/customer/customer.model');
const Employee = require('@modules/employee/employee.model');
const StoreReward = require('@modules/store-reward/store-reward.model');
const Store = require('@modules/store/store.model');
const User = require('@modules/user/user.model');

module.exports = {
  Admin,
  Coupon,
  Customer,
  Employee,
  NotificationManager,
  Store,
  StoreReward,
  User,
};
