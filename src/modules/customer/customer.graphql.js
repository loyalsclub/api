const { Coupon, Customer } = require('@models');

const userCustomerProfiles = async (_, args, context) => {
  const { status } = args;
  const user = context.reqUser();
  const profiles = await user.getCustomerProfiles(status);

  return profiles;
};

const userCustomerProfileByStoreId = async (_, args, context) => {
  const { storeId } = args;
  const user = context.reqUser().raw();
  const customer = await Customer.initOrCreate(user.id, storeId);
  const profile = await customer.raw();
  profile.Store = await customer.getStoreWithLocations();
  return profile;
};

const modifyCustomerProfile = async (_, args, context) => {
  const { id, status } = args;
  const customerUser = context.reqUser().raw();
  const customer = await Customer.initById(customerUser.id, id);

  const profile = await customer.modifyStatus(status);
  return profile;
};

const registerCouponPoints = async (_, args, context) => {
  const user = context.reqUser().raw();

  const customer = await Customer.initOrCreate(user.id, args.storeId);

  const coupon = await Coupon.init(args.couponCode, args.storeId);

  return customer.registerCouponPoints(coupon);
};

const customerProfileGetStoreLevelsWithRewards = async (_, args, context) => {
  const user = context.reqUser().raw();
  const customer = await Customer.initOrCreate(user.id, args.storeId);

  return customer.getStoreLevelsWithRewards();
};

const getCustomerByCardId = async (_, args) => {
  const customer = await Customer.initByCardId(args.cardId);

  return Promise.all([customer.raw(), customer.getStoreLevelsWithRewards()]).then(([Customer, StoreLevels]) => ({
    Customer,
    StoreLevels,
  }));
};

const typeDefs = `
  type CustomerProfile {
    id: ID!
    userId: Int!
    storeId: Int!
    status: String!
    cardId: String
    Store: Store!
    CustomerLevel: CustomerLevel!
    User: User
    createdAt: DateTime!
  }

  type CustomerLevel {
    customerId: Int,
    activeRewards: Int,
    nextLevelPointsDif: Int,
    currentLevelPoints: Int,
    nextLevelPoints: Int,
    storeId: Int,
    level: Int,
    total: Int
  }

  type CustomerHistory {
    id: ID!
    type: String!
    value: String!
    createdAt: DateTime!
  }

  type CustomerAndStoreLevels {
    Customer: CustomerProfile
    StoreLevels: [StoreLevel]
  }

  enum AllowedStatusCustomerProfile {
    active
    pending
    deleted
  }

  extend type Query {
    userCustomerProfiles(status: AllowedStatusCustomerProfile): [CustomerProfile],
    userCustomerProfileByStoreId(storeId: ID!): CustomerProfile
    customerProfileGetStoreLevelsWithRewards(storeId: ID!): [StoreLevel]
    getCustomerByCardId(cardId: String!): CustomerAndStoreLevels
  }

  extend type Mutation {
    modifyCustomerProfile(
      id: ID!
      status: AllowedStatusCustomerProfile!
    ): CustomerProfile
    registerCouponPoints (
      couponCode: String!
      storeId: ID!
    ): Point!
  }
`;

const resolvers = {
  Query: {
    userCustomerProfiles,
    userCustomerProfileByStoreId,
    customerProfileGetStoreLevelsWithRewards,
    getCustomerByCardId,
  },
  Mutation: {
    modifyCustomerProfile,
    registerCouponPoints,
  },
};

module.exports = { typeDefs, resolvers };
