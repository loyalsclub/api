const { ApolloError } = require('apollo-server');

const db = require('@db');
const { CUSTOMER_STATUS, COUPON_STATUS, USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');
const emailsConstants = require('@modules/core/constants/emails.constants');
const { API_CUSTOMER_ERRORS } = require('@modules/core/constants/error-codes.constants');
const NotificationManager = require('@modules/core/notification-manager.model');
const { STORE_NOTIFICATIONS_TYPES } = require('@modules/store/constants/store.constants');
const { sendCustomerWelcomeEmail, sendCustomerPointsLoadedEmail } = require('@services/email.service');
const { usageValidatorService } = require('@services/usage-validator.service');
const { getFrequency } = require('@utils/string-utils');

const { CONSTRAINT_USER_STORE_UNIQUE, CUSTOMER_HISTORY_TYPES } = require('./constants/customer.constants');

const createCustomerPromises = {};

class Customer {
  constructor(dbCustomer) {
    this.dbCustomer = dbCustomer;
  }

  static async initByPk(customerId) {
    const profile = await db.CustomerProfile.findByPk(customerId, { include: db.CustomerLevel });

    if (!profile) {
      const error = API_CUSTOMER_ERRORS['api/user-is-not-a-customer'];
      throw new ApolloError(error.msg, error.code);
    }

    return new Customer(profile);
  }

  static async initById(userId, customerId) {
    const profile = await db.CustomerProfile.findOne({
      where: {
        id: customerId,
        userId,
      },
    });

    if (!profile) {
      const error = API_CUSTOMER_ERRORS['api/user-is-not-a-customer'];
      throw new ApolloError(error.msg, error.code);
    }

    return new Customer(profile);
  }

  static async initByCardId(cardId) {
    const profile = await db.CustomerProfile.findOne({
      where: { cardId },
      include: [db.CustomerLevel, db.Store],
    });

    if (!profile) {
      const error = API_CUSTOMER_ERRORS['api/user-is-not-a-customer'];
      throw new ApolloError(error.msg, error.code);
    }

    return new Customer(profile);
  }

  static async initOrCreate(userId, storeId) {
    let profile = await Customer.initIfExists(userId, storeId);

    if (!profile) {
      profile = await Customer.create({ userId, storeId });
    }

    return profile;
  }

  static async create({ userId, storeId, initialLevel = null, cardId = null }) {
    const createCustomerPromiseId = `${userId}-${storeId}`;
    const isCreationInProgress = Boolean(createCustomerPromises[createCustomerPromiseId]);

    const store = await db.Store.findByPk(storeId);

    if (!isCreationInProgress) {
      createCustomerPromises[createCustomerPromiseId] = db.CustomerProfile.create({
        userId,
        storeId,
        cardId: store.cardModule ? cardId : null,
        status: store.private ? CUSTOMER_STATUS.PENDING : CUSTOMER_STATUS.ACTIVE,
      })
        .then(async dbCustomer => {
          const isValidInitialLevel = store.fixedLevels && initialLevel > 1 && initialLevel <= 3;

          if (isValidInitialLevel) {
            await Customer.setCustomerLevel({ customerId: dbCustomer.id, storeId, level: initialLevel });
          }

          const user = await dbCustomer.getUser();

          Customer.sendWelcomeEmail(user, storeId);

          NotificationManager.sendPushToStore({
            type: STORE_NOTIFICATIONS_TYPES.NEW_CUSTOMER,
            storeId,
            data: {
              customerName: `${user.firstName} ${user.lastName}`,
            },
          });

          return dbCustomer;
        })
        .catch(err => {
          const hasAlreadyBeenCreated = Boolean(err.fields[CONSTRAINT_USER_STORE_UNIQUE]);

          if (hasAlreadyBeenCreated) {
            return db.CustomerProfile.findOne({ where: { userId, storeId } });
          }

          console.error('[Error] Race condition customer creation', err);

          const error = API_CUSTOMER_ERRORS['api/could-not-create-customer'];
          throw new ApolloError(error.msg, error.code);
        });
    }

    const dbCustomer = await createCustomerPromises[createCustomerPromiseId];

    delete createCustomerPromises[createCustomerPromiseId];

    dbCustomer.setDataValue('CustomerLevel', await dbCustomer.getCustomerLevel());
    dbCustomer.setDataValue('User', await dbCustomer.getUser());

    return new Customer(dbCustomer);
  }

  static sendWelcomeEmail(user, storeId) {
    return db.Store.findOne({
      where: { id: storeId },
      include: [
        {
          model: db.StoreLevel,
          include: [
            {
              model: db.StoreReward,
            },
          ],
        },
      ],
    }).then(store => {
      const storeRewards = store.StoreLevels[0].StoreRewards || [];
      const selectedRewards = storeRewards.slice(0, 3);

      return sendCustomerWelcomeEmail({
        SENDER_NAME: store.name,
        EMAIL_TO: user.email,
        TOPIC_NAME: emailsConstants.TOPICS.CUSTOMER_PROFILE_CREATED,
        DATA: {
          CUSTOMER_FIRST_NAME: user.firstName,
          STORE_NAME: store.name,
          STORE_PROFILE_IMAGE: store.profileImage,
          STORE_URL: `${process.env.USERS_BASE_URL}/${store.alias}`,
          STORE_REWARDS: selectedRewards.map(storeReward => ({
            title: storeReward.title,
            usageFrequency: getFrequency(storeReward.UsageFrequency),
            description: storeReward.description || '',
          })),
        },
      });
    });
  }

  static async initIfExists(userId, storeId) {
    let profile = await db.CustomerProfile.findOne({
      where: {
        userId,
        storeId,
      },
      include: [
        {
          model: db.CustomerLevel,
        },
      ],
    });

    if (!profile) {
      return null;
    }

    return new Customer(profile);
  }

  static async setCustomerLevel({ customerId, storeId, level }) {
    const customerLevel = await db.CustomerLevel.findOne({
      where: { customerId, storeId },
    });

    const shouldUpgradeLevel = customerLevel.level < level;

    if (shouldUpgradeLevel) {
      const storeLevel = await db.StoreLevel.findOne({
        where: { storeId, level },
      });

      await db.Point.create({
        storeId,
        customerId,
        amount: storeLevel.points,
      });
    } else {
      await db.Point.destroy({ where: { storeId, customerId } });

      const shouldSetPointsForSecondLevel = level == 2;

      if (shouldSetPointsForSecondLevel) {
        await Customer.setCustomerLevel({ customerId, storeId, level });
      }
    }
  }

  async modifyStatus(status) {
    const updatedProfile = await this.dbCustomer.update({
      status,
    });

    return updatedProfile.get({
      plain: true,
    });
  }

  async getStore() {
    const val = await this.dbCustomer.getStore();
    return val;
  }

  async getStoreWithLocations() {
    const val = await this.dbCustomer.getStore({
      include: [
        {
          model: db.StoreLocation,
        },
      ],
    });
    return val;
  }

  async getPoints() {
    const attributes = [[db.sequelize.fn('sum', db.sequelize.col('amount')), 'amount']];

    const p = this.dbCustomer.getPoints({ attributes });
    const [points] = await Promise.all([p]);

    return points[0].get('amount');
  }

  async raw() {
    const customer = this.dbCustomer.get({
      plain: true,
    });

    return {
      ...customer,
    };
  }

  async validateCouponUsage(coupon) {
    const usageFrequency = coupon.dbCoupon.get('UsageFrequency') || {};
    const isOneTimeUse = usageFrequency && usageFrequency.type === USAGE_FREQUENCIES.ONE_TIME;

    const where = { couponId: coupon.dbCoupon.id };

    if (!isOneTimeUse) {
      where.customerId = this.dbCustomer.id;
    }

    const dbPoint = await db.Point.findOne({
      where,
      order: [['createdAt', 'DESC']],
    });

    return usageValidatorService.isValidToRequestNow({
      usageFrequency,
      lastTimeUsed: dbPoint,
    });
  }

  async registerCouponPoints(coupon) {
    const isCouponActive = coupon.dbCoupon.status === COUPON_STATUS.ACTIVE;

    if (!isCouponActive) {
      const error = API_CUSTOMER_ERRORS['api/coupon-is-not-active'];
      throw new ApolloError(error.msg, error.code);
    }

    const isValidToApply = await this.validateCouponUsage(coupon);

    if (!isValidToApply) {
      const error = API_CUSTOMER_ERRORS['api/coupon-invalid-time-frame'];
      throw new ApolloError(error.msg, error.code);
    }

    const points = await db.Point.create({
      storeId: coupon.dbCoupon.storeId,
      couponId: coupon.dbCoupon.id,
      customerId: this.dbCustomer.id,
      amount: coupon.dbCoupon.points,
    });

    Promise.all([this.dbCustomer.getUser(), this.dbCustomer.getStore()]).then(([customerUser, store]) => {
      sendCustomerPointsLoadedEmail({
        SENDER_NAME: store.name,
        EMAIL_TO: customerUser.email,
        TOPIC_NAME: emailsConstants.TOPICS.CUSTOMER_POINTS_LOADED,
        DATA: {
          CUSTOMER_FIRST_NAME: customerUser.firstName,
          STORE_NAME: store.name,
          POINTS_AMOUNT: coupon.dbCoupon.points,
          STORE_PROFILE_IMAGE: store.profileImage,
          STORE_URL: `${process.env.USERS_BASE_URL}/${store.alias}`,
        },
      });

      NotificationManager.sendPushToStore({
        type: STORE_NOTIFICATIONS_TYPES.CUSTOMER_ACTIVATED_POINTS,
        storeId: store.id,
        data: {
          customerName: `${customerUser.firstName} ${customerUser.lastName}`,
          pointsAmount: coupon.dbCoupon.points,
        },
      });
    });

    return points;
  }

  async getStoreLevelsWithRewards(level = [1, 2, 3]) {
    const storeLevels = await db.StoreLevel.findAll({
      where: {
        storeId: this.dbCustomer.storeId,
        level,
      },
      include: [
        {
          model: db.StoreReward,
          include: [
            {
              model: db.StoreRewardUsage,
              where: { customerId: this.dbCustomer.id },
              limit: 1,
              order: [['createdAt', 'DESC']],
            },
          ],
        },
      ],
    });

    return storeLevels.map(storeLevel => {
      return {
        ...storeLevel.get({ plain: true }),
        StoreRewards: storeLevel.get('StoreRewards').map(storeReward => {
          const usageFrequency = storeReward.get('UsageFrequency');
          const [lastStoreRewardUsage] = storeReward.get('StoreRewardUsages');

          const isAvailable = usageValidatorService.isValidToRequestNow({
            usageFrequency,
            lastTimeUsed: lastStoreRewardUsage,
          });

          const lastTimeUsed = lastStoreRewardUsage && lastStoreRewardUsage.createdAt;
          const nextTimeAvailable = usageValidatorService.getNextTimeAvailable(usageFrequency, lastTimeUsed);
          const hasBeenUsed = Boolean(lastTimeUsed);

          let isConsideredUsed = false;

          if (hasBeenUsed) {
            const isOneTime = usageFrequency.type === USAGE_FREQUENCIES.ONE_TIME;
            const isEveryCertainHours = usageFrequency.type === USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS;

            if (isOneTime) {
              isConsideredUsed = true;
            } else if (isEveryCertainHours) {
              isConsideredUsed = !isAvailable;
            } else {
              isConsideredUsed = usageValidatorService.wasUsedToday(lastTimeUsed);
            }
          }

          return {
            ...storeReward.get({ plain: true }),
            isAvailable,
            lastTimeUsed,
            nextTimeAvailable,
            isConsideredUsed,
          };
        }),
      };
    });
  }

  getHistory() {
    const order = [['createdAt', 'DESC']];

    const storeRewardsPromise = db.StoreRewardUsage.findAll({
      order,
      where: { customerId: this.dbCustomer.id },
      include: [{ model: db.StoreReward }],
    }).then(storeRewards =>
      storeRewards.map(({ id, createdAt, StoreReward }) => ({
        id,
        type: CUSTOMER_HISTORY_TYPES.STORE_REWARD_USAGE,
        value: StoreReward.title,
        createdAt,
      })),
    );

    const pointsPromise = db.Point.findAll({
      where: {
        customerId: this.dbCustomer.id,
        storeId: this.dbCustomer.storeId,
      },
      order,
    }).then(points =>
      points.map(({ id, createdAt, amount }) => ({
        id,
        type: CUSTOMER_HISTORY_TYPES.POINT,
        value: amount.toString(),
        createdAt,
      })),
    );

    return Promise.all([storeRewardsPromise, pointsPromise]).then(([storeRewards, points]) =>
      storeRewards.concat(points).sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt)),
    );
  }
}

module.exports = Customer;
