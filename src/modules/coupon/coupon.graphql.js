const { Coupon } = require('@models');

const validateCouponCode = (_, args) => {
  return Coupon.validateCouponCode(args);
};

const typeDefs = `
  type Coupon {
    id: ID!
    couponCode: String!
    Store: Store!
    validatedAt: DateTime
    status: String!
    points: Int!
    url: String
    createdAt: DateTime!
    updatedAt: DateTime!
  }

  type CouponsEdge {
    cursor: String
    node: Coupon
  }

  type CouponsConnection {
    storeId: ID
    totalCount: Int
    pageInfo: PageInfo!
    edges: [CouponsEdge]
  }

  extend type Query {
    validateCouponCode (
      couponCode: String!
      storeId: ID!
    ): Boolean
  }
`;

const resolvers = {
  Mutation: {},
  Query: {
    validateCouponCode,
  },
};

module.exports = { typeDefs, resolvers };
