const { ApolloError } = require('apollo-server');

const db = require('@db');
const { COUPON_STATUS, USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');
const { API_CUSTOMER_ERRORS } = require('@modules/core/constants/error-codes.constants');
const { dynamicLinkService } = require('@services/dynamic-link.service');
const { generateRandomAlphanumericString } = require('@utils/string-utils');

const { USERS_BASE_URL } = process.env;

class Coupon {
  constructor(dbCoupon) {
    this.dbCoupon = dbCoupon;
  }

  static async init(couponCode, storeId) {
    const dbCoupon = await db.Coupon.findOne({
      where: { couponCode, storeId },
    });

    if (!dbCoupon) {
      const error = API_CUSTOMER_ERRORS['api/coupon-not-found'];
      throw new ApolloError(error.msg, error.code);
    }

    return new Coupon(dbCoupon);
  }

  static async initById(id) {
    const dbCoupon = await db.Coupon.findOne({ where: { id } });

    if (!dbCoupon) {
      const error = API_CUSTOMER_ERRORS['api/coupon-not-found'];
      throw new ApolloError(error.msg, error.code);
    }

    return new Coupon(dbCoupon);
  }

  static async create({ usageFrequency = {}, ...coupon }) {
    const isValid = await Coupon.validateCouponCode(coupon);

    if (!isValid) {
      const error = API_CUSTOMER_ERRORS['api/coupon-already-exists'];
      throw new ApolloError(error.msg, error.code);
    }

    const couponUsageFrequency = await db.UsageFrequency.create({
      type: USAGE_FREQUENCIES.FOREVER,
      ...usageFrequency,
    });

    const createdCoupon = await db.Coupon.create({
      ...coupon,
      status: COUPON_STATUS.ACTIVE,
      usageFrequencyId: couponUsageFrequency.id,
    });

    createdCoupon.setDataValue('UsageFrequency', await createdCoupon.getUsageFrequency());

    return new Coupon(createdCoupon);
  }

  static async validateCouponCode(coupon) {
    if (!coupon.couponCode || !coupon.storeId) {
      return false;
    }

    const dbCoupon = await db.Coupon.findOne({
      where: {
        couponCode: coupon.couponCode,
        storeId: coupon.storeId,
      },
    });

    return !dbCoupon;
  }

  static async generateRandomCode(storeId) {
    const couponCode = generateRandomAlphanumericString(6);

    const isValid = await Coupon.validateCouponCode({
      storeId,
      couponCode,
    });

    if (!isValid) {
      return await Coupon.generateRandomCode(storeId);
    }

    return couponCode;
  }

  async modifyStatus(status) {
    const updatedCoupon = await this.dbCoupon.update({
      status,
    });

    return updatedCoupon.get({
      plain: true,
    });
  }

  async raw() {
    this.dbCoupon.setDataValue('Store', await this.dbCoupon.getStore());
    const store = this.dbCoupon.get('Store');

    const rawCoupon = await this.dbCoupon.get({ plain: true });

    return {
      ...rawCoupon,
      url: await Coupon.generateCouponLink(this.dbCoupon.couponCode, store, true),
    };
  }

  static async generateCouponLink(couponCode, store, short = false) {
    const couponUrl = `${USERS_BASE_URL}/${store.alias}?cc=${couponCode}`;
    const encodedStoreName = encodeURIComponent(store.name);

    const longDynamicLink = dynamicLinkService.generateLongLink({
      url: couponUrl,
      title: encodedStoreName,
      image: store.profileImage,
    });

    if (short) {
      return dynamicLinkService.generateShortLink(longDynamicLink);
    }

    return longDynamicLink;
  }
}

module.exports = Coupon;
