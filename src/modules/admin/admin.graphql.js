const { Admin, Coupon, Store, StoreReward } = require('@models');
const { validateIsAdmin } = require('@utils/roles-validator');

const adminCreateStoreForUser = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  return Store.create(args.userId, args.store);
};

const adminModifyStore = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());
  const store = await Store.init(args.storeId);

  return store.update(args.store);
};

const adminListStores = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  const stores = await Admin.listStores();
  return stores;
};

const adminSearchUserByEmail = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  const users = Admin.searchUser(args);
  return users;
};

const adminSearchStore = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  return Admin.searchStore(args);
};

const adminGetStore = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  const store = await Admin.getStore(args);
  return store;
};

const adminGetStoreLevels = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  const storeLevels = await Admin.getStoreLevels(args);
  return storeLevels;
};

const adminRemoveStoreReward = (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());
  return StoreReward.remove(args.rewardId);
};

const adminAddStoreReward = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());
  const storeReward = await StoreReward.create(args.storeReward);
  return storeReward.raw();
};

const adminModifyStoreReward = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());
  const storeReward = await StoreReward.initById(args.id);
  return storeReward.update(args.storeReward);
};

const adminGetStatistics = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());
  return Admin.getStatistics();
};

const adminCreateCouponForStore = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  const coupon = await Coupon.create(args);

  return coupon.raw();
};

const adminGetCoupons = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());
  const totalCount = await Admin.totalCoupons(args.storeId);
  const pagination = {
    first: args.first || 20,
    after: args.after,
  };

  const coupons = await Admin.getCoupons(pagination, args.storeId);
  let endCursor = null;
  let hasNextPage = false;
  let offset = 0;
  if (coupons.length > 0) {
    if (coupons.length > pagination.first) {
      offset = -1;
      hasNextPage = true;
      endCursor = coupons[coupons.length - 2].id;
    } else {
      endCursor = coupons[coupons.length - 1].id;
    }
  }
  return {
    storeId: args.storeId,
    totalCount,
    pageInfo: {
      startCursor: coupons.length > 0 ? coupons[0].id : null,
      endCursor,
      hasNextPage,
    },
    edges: coupons.slice(0, coupons.length + offset).map(coupon => ({
      cursor: coupon.id,
      node: coupon,
    })),
  };
};

const adminModifyCoupon = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  const { id, status } = args;

  const coupon = await Coupon.initById(id);

  return coupon.modifyStatus(status);
};

const adminRemoveStore = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());

  const store = await Store.init(args.storeId);

  return store.remove();
};

const adminGetStoreStatistics = async (_, args, context) => {
  validateIsAdmin(context.reqUser().raw());
  return Admin.storeStatistics(args.storeId);
};

const typeDefs = `
  type AdminStatistics {
    totalUsers: Int
    totalCustomers: Int
    totalStores: Int
    totalLevel1: Int
    totalLevel2: Int
    totalLevel3: Int
    totalLoadPointsOperations: Int
  }

  type AdminStoreStatistics {
    customersOverTime: [CountStat]
    pointTransactionsOverTime: [CountStat]
    rewardTransactionsOverTime: [CountStat]
  }

  enum AllowedStatusCoupon {
    active
    paused
  }

  extend type Mutation {
    adminCreateStoreForUser(userId: ID!, store: CreateStoreInput!): Store!
    adminModifyStore(storeId: ID!, store: CreateStoreInput!): Store!
    adminRemoveStoreReward(rewardId: ID!): Boolean
    adminAddStoreReward(storeReward: CreateStoreRewardInput!): StoreReward!
    adminModifyStoreReward(
      id: ID!
      storeReward: CreateStoreRewardInput!
    ): StoreReward!
    adminCreateCouponForStore (
      couponCode: String!
      storeId: ID!
      points: Int!
    ): Coupon!
    adminModifyCoupon (
      id: ID!
      status: AllowedStatusCoupon!
    ): Coupon!
    adminRemoveStore(storeId: ID): Store
  }

  extend type Query {
    adminListStores: [Store]
    adminGetStore(storeId: ID!): Store
    adminGetStoreLevels(storeId: ID!): [StoreLevel]
    adminSearchUserByEmail (
      data: String!
    ): [User]
    adminSearchStore (
      data: String!
    ): [Store]
    adminGetStatistics: AdminStatistics
    adminGetStoreStatistics(storeId: ID): AdminStoreStatistics
    adminGetCoupons(
      storeId: ID
      first: Int
      after: String
    ): CouponsConnection
  }
`;

const resolvers = {
  Mutation: {
    adminCreateStoreForUser,
    adminModifyStore,
    adminRemoveStoreReward,
    adminAddStoreReward,
    adminModifyStoreReward,
    adminCreateCouponForStore,
    adminModifyCoupon,
    adminRemoveStore,
  },
  Query: {
    adminSearchUserByEmail,
    adminSearchStore,
    adminListStores,
    adminGetStore,
    adminGetStoreLevels,
    adminGetStatistics,
    adminGetCoupons,
    adminGetStoreStatistics,
  },
};

module.exports = { typeDefs, resolvers };
