const Op = require('sequelize').Op;
const { QueryTypes } = require('sequelize');

const db = require('@db');
const Coupon = require('@modules/coupon/coupon.model');
const { sequelizeValidationErrorParser } = require('@utils/errors-parser');

const listStores = async () => {
  try {
    const stores = await db.Store.findAll({
      order: [['id', 'DESC']],
      include: [{ model: db.EmployeeProfile }, { model: db.CustomerProfile }],
    });

    return stores;
  } catch (error) {
    const parsedErrors = sequelizeValidationErrorParser(error.errors);
    throw new Error(parsedErrors);
  }
};

const getStore = async ({ storeId }) => {
  const store = await db.Store.findOne({
    where: { id: storeId },
    include: [
      {
        model: db.EmployeeProfile,
        include: [{ model: db.User }],
      },
      { model: db.StoreLocation },
      { model: db.StoreSetting },
      {
        model: db.CustomerProfile,
        include: [{ model: db.User }, { model: db.CustomerLevel }],
      },
      {
        model: db.StoreReward,
        include: [
          {
            model: db.StoreRewardUsage,
            include: [
              {
                model: db.CustomerProfile,
                include: [
                  {
                    model: db.User,
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  });

  return store;
};

const getStoreLevels = async payload => {
  const { storeId } = payload;
  const levels = await db.StoreLevel.findAll({
    where: { storeId },
  });

  return levels;
};

const searchUser = async ({ data }) => {
  const dbUsers = await db.User.findAll({
    limit: 10,
    where: {
      [Op.or]: [
        {
          alias: data,
        },
        {
          email: {
            [Op.like]: `${data}%`,
          },
        },
      ],
    },
  });
  return dbUsers;
};

const searchStore = async ({ data }) => {
  if (!data) {
    return [];
  }

  const dbStores = await db.Store.findAll({
    limit: 10,
    where: {
      [Op.or]: [
        {
          alias: data,
        },
        {
          name: {
            [Op.like]: `${data}%`,
          },
        },
      ],
    },
  });

  return dbStores;
};

const getStatistics = async () => {
  let res = {};

  res.totalUsers = await db.User.count();
  res.totalStores = await db.Store.count();
  res.totalCustomers = await db.CustomerProfile.count();
  res.totalLevel1 = await db.CustomerLevel.count({
    where: {
      level: 1,
    },
  });
  res.totalLevel2 = await db.CustomerLevel.count({
    where: {
      level: 2,
    },
  });
  res.totalLevel3 = await db.CustomerLevel.count({
    where: {
      level: 3,
    },
  });
  res.totalLoadPointsOperations = await db.Point.count();

  return res;
};

const getCoupons = async (pagination = {}, storeId) => {
  const where = {};
  let operation = Op.lt;

  if (pagination.after) {
    where.id = {
      [operation]: pagination.after,
    };
  }

  if (storeId) {
    where.storeId = storeId;
  }

  const coupons = await db.Coupon.findAll({
    where,
    include: [db.Store, db.Point],
    order: [['id', 'DESC']],
    limit: pagination.first ? pagination.first + 1 : undefined,
  });

  return Promise.all(
    coupons.map(async coupon => {
      coupon.url = await Coupon.generateCouponLink(coupon.couponCode, coupon.Store);
      coupon.validatedAt = coupon.Point ? coupon.Point.createdAt : null;
      return coupon;
    }),
  );
};

const totalCoupons = async storeId => {
  const where = {};
  if (storeId) {
    where.storeId = storeId;
  }

  return await db.Coupon.count({
    where,
  });
};

const storeStatistics = async storeId => {
  const customersOverTime = await db.CustomerProfile.findAll({
    attributes: [
      /* add other attributes you may need from your table */
      [db.sequelize.fn('DATE', db.sequelize.col('createdAt')), 'date'],
      [db.sequelize.fn('COUNT', 'id'), 'count'],
    ],
    where: {
      storeId,
    },
    group: [db.sequelize.fn('DATE', db.sequelize.col('createdAt')), 'date'],
    raw: true,
  });

  const pointTransactionsOverTime = await db.Point.findAll({
    attributes: [
      /* add other attributes you may need from your table */
      [db.sequelize.fn('DATE', db.sequelize.col('createdAt')), 'date'],
      [db.sequelize.fn('COUNT', 'id'), 'count'],
    ],
    where: {
      storeId,
    },
    group: [db.sequelize.fn('DATE', db.sequelize.col('createdAt')), 'date'],
    raw: true,
  });

  const rewardTransactionsOverTime = await db.sequelize.query(
    `SELECT 
      DATE(sru.createdAt) AS date,
      COUNT(sru.id) AS count 
    FROM StoreRewardUsages AS sru
      LEFT OUTER JOIN StoreRewards AS sr
        ON sru.storeRewardId = sr.id
      LEFT OUTER JOIN UsageFrequencies AS sruf
        ON sr.usageFrequencyId = sruf.id 
    WHERE sr.storeId = ${storeId}
    GROUP BY DATE(sru.createdAt), date;`,
    { type: QueryTypes.SELECT },
  );

  return {
    customersOverTime,
    pointTransactionsOverTime,
    rewardTransactionsOverTime,
  };
};

module.exports = {
  searchUser,
  storeStatistics,
  searchStore,
  listStores,
  getStore,
  getStoreLevels,
  getStatistics,
  totalCoupons,
  getCoupons,
};
