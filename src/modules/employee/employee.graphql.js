const { Customer, Employee, User, Store, StoreReward } = require('@models');
const { sendUserInvitationEmail } = require('@services/email.service');
const { validateIsDemoStore } = require('@utils/roles-validator');

const userEmployeeProfiles = async (_, args, context) => {
  const user = context.reqUser();
  const profiles = await user.getEmployeeProfiles();
  return profiles;
};

const employeeGetUser = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  await Employee.init(employeeUser.id, args.storeId);
  const existingUser = await User.findUserByEmailOrAlias(args.userField);

  return existingUser ? existingUser.raw() : null;
};

const employeeGetCustomer = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  await Employee.init(employeeUser.id, args.storeId);
  const customerUser = await User.initEmailOrAlias(args.userField);

  const existingCustomer = await Customer.initIfExists(customerUser.raw().id, args.storeId);
  return existingCustomer ? existingCustomer.raw() : null;
};

const employeeCreateCustomer = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);
  const customerUser = await User.initEmailOrAlias(args.userField);
  const customer = await employee.createCustomer({ userId: customerUser.raw().id, ...args });

  return customer.raw();
};

/** Function meant for users that arent part of the system. A db record will be
 * created with the corresponding CustomerProfile. Users will need to finish the registration.
 */

const employeeInviteNewUser = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);
  const customerUser = await User.createPendingUser(args);
  const rawUser = customerUser.raw();
  const customer = await employee.createCustomer({ userId: rawUser.id, ...args });

  const store = await employee.getStore();

  await sendUserInvitationEmail({
    FIRST_NAME: rawUser.firstName,
    LAST_NAME: rawUser.lastName,
    EMAIL_TO: rawUser.email,
    STORE_NAME: store.name,
    STORE_ALIAS: store.alias,
    STORE_PROFILE_IMAGE: store.profileImage,
  });

  return customer.raw();
};

const employeeLoadPoints = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);
  const customerUser = await User.initEmailOrAlias(args.userField);
  const point = employee.loadPoints(customerUser.raw().id, args.amount);

  return point;
};

const employeeLevelsAndRewardsByStoreId = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);

  const levels = await employee.getStoreLevels();
  return levels;
};

const employeeGetStatistics = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);

  const storeStatistics = await employee.getStoreStatistics();

  return storeStatistics;
};

const employeeGetStoreCustomers = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);

  const storeCustomers = await employee.getStoreCustomers();

  return storeCustomers;
};

const employeeCreateCoupon = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);
  const coupon = await employee.createCoupon(args.amount);

  return coupon.raw();
};

const employeeRegisterStoreRewardUsage = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  const employee = await Employee.init(employeeUser.id, args.storeId);
  const customerUser = await User.initEmailOrAlias(args.userField);

  return employee.registerStoreRewardUsage(customerUser.raw().id, args.storeRewardId, args.storeId);
};

const employeeGetCustomerAvailableRewards = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  await Employee.init(employeeUser.id, args.storeId);
  const customerUser = await User.initEmailOrAlias(args.userField);
  const customer = await Customer.initIfExists(customerUser.raw().id, args.storeId);

  return customer ? customer.getStoreLevelsWithRewards() : null;
};

const employeeAddStoreReward = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  await Employee.init(employeeUser.id, args.storeId);
  validateIsDemoStore(args.storeId);
  const storeReward = await StoreReward.create(args.storeReward, args.storeId);
  return storeReward.raw();
};

const employeeModifyStoreReward = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  await Employee.init(employeeUser.id, args.storeId);
  validateIsDemoStore(args.storeId);
  const storeReward = await StoreReward.initById(args.id);
  return storeReward.update(args.storeReward, args.storeId);
};

const employeeRemoveStoreReward = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  await Employee.init(employeeUser.id, args.storeId);
  validateIsDemoStore(args.storeId);
  return StoreReward.remove(args.rewardId, args.storeId);
};

const employeeModifyStore = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();
  await Employee.init(employeeUser.id, args.storeId);
  validateIsDemoStore(args.storeId);

  const store = await Store.init(args.storeId);

  return store.update(args.store);
};

const employeeGetCustomerHistory = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();

  await Employee.init(employeeUser.id, args.storeId);

  const customer = await Customer.initByPk(args.customerId);

  return customer.getHistory();
};

const employeeSetCustomerLevel = async (_, args, context) => {
  const employeeUser = context.reqUser().raw();

  await Employee.init(employeeUser.id, args.storeId);

  await Customer.setCustomerLevel(args);

  const customer = await Customer.initByPk(args.customerId);

  return customer.raw();
};

const typeDefs = `
  type EmployeeProfile {
    id: ID!
    storeId: ID!
    permissionId: Int!
    Store: Store!
    User: User
  }

  extend type Query {
    userEmployeeProfiles: [EmployeeProfile],
    employeeLevelsAndRewardsByStoreId(storeId: ID!): [StoreLevel]
    employeeGetUser(storeId: ID!, userField: String!): User
    employeeGetCustomer(storeId: ID!, userField: String!): CustomerProfile
    employeeGetStatistics(storeId: ID!): StoreStatistics
    employeeGetStoreCustomers(storeId: ID!): [CustomerProfile]
    employeeGetCustomerAvailableRewards(storeId: ID!, userField: String!): [StoreLevel]
    employeeGetCustomerHistory(storeId: ID!, customerId: ID!): [CustomerHistory]
  }

  extend type Mutation {
    employeeCreateCustomer (
      storeId: ID!
      userField: String!
      initialLevel: Int
      cardId: String
    ): CustomerProfile
    employeeInviteNewUser (
      firstName: String!
      lastName: String!
      email: Email!
      storeId: ID!
      initialLevel: Int
      cardId: String
    ): CustomerProfile
    employeeLoadPoints (
      amount: Int!
      userField: String!
      storeId: ID!
    ): Point
    employeeCreateCoupon(
      storeId: ID!
      amount: Int!
    ): Coupon!
    employeeRegisterStoreRewardUsage(
      storeId: ID!
      storeRewardId: ID!
      userField: String!
    ): StoreRewardUsage
    employeeAddStoreReward(
      storeId: ID!
      storeReward: CreateStoreRewardInput!
    ): StoreReward!
    employeeModifyStoreReward(
      id: ID!
      storeId: ID!
      storeReward: CreateStoreRewardInput!
    ): StoreReward!
    employeeRemoveStoreReward(storeId: ID!, rewardId: ID!): Boolean
    employeeModifyStore(storeId: ID!, store: CreateStoreInput!): Store!
    employeeSetCustomerLevel(customerId: ID!, storeId: ID!, level: Int!): CustomerProfile
  }
`;

const resolvers = {
  Query: {
    userEmployeeProfiles,
    employeeLevelsAndRewardsByStoreId,
    employeeGetUser,
    employeeGetCustomer,
    employeeGetStatistics,
    employeeGetStoreCustomers,
    employeeGetCustomerAvailableRewards,
    employeeGetCustomerHistory,
  },
  Mutation: {
    employeeCreateCustomer,
    employeeInviteNewUser,
    employeeLoadPoints: employeeLoadPoints,
    employeeCreateCoupon,
    employeeRegisterStoreRewardUsage,
    employeeAddStoreReward,
    employeeModifyStoreReward,
    employeeRemoveStoreReward,
    employeeModifyStore,
    employeeSetCustomerLevel,
  },
};

module.exports = { typeDefs, resolvers };
