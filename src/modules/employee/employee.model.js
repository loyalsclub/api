const { ApolloError } = require('apollo-server');
const sequelize = require('sequelize');

const db = require('@db');
const { USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');
const emailsConstants = require('@modules/core/constants/emails.constants');
const {
  API_AUTH_ERRORS,
  API_USER_ERRORS,
  API_CUSTOMER_ERRORS,
  API_EMPLOYEE_ERRORS,
} = require('@modules/core/constants/error-codes.constants');
const Coupon = require('@modules/coupon/coupon.model');
const Customer = require('@modules/customer/customer.model');
const Store = require('@modules/store/store.model');
const { sendCustomerPointsLoadedEmail } = require('@services/email.service');
const { usageValidatorService } = require('@services/usage-validator.service');

class Employee {
  constructor(dbEmployee) {
    this.dbEmployee = dbEmployee;
  }

  static async init(userId, storeId) {
    const employee = await db.EmployeeProfile.findOne({
      where: {
        userId: userId,
        storeId: storeId,
      },
    });

    if (!employee) {
      const err = API_AUTH_ERRORS['auth/user-is-not-authorized'];
      throw new ApolloError(err.msg, err.code);
    }

    return new Employee(employee);
  }

  async getCustomer(userId) {
    const profile = await db.CustomerProfile.findOne({
      where: {
        userId: userId,
        storeId: this.dbEmployee.storeId,
      },
    });
    return profile;
  }

  async createCustomer({ userId, ...data }) {
    const profile = await this.getCustomer(userId);
    if (profile) {
      const err = API_USER_ERRORS['api/customer-exists'];
      throw new ApolloError(err.msg, err.code);
    }

    return Customer.create({ userId, storeId: this.dbEmployee.storeId, ...data });
  }

  async loadPoints(userId, price) {
    const customer = await Customer.initOrCreate(userId, this.dbEmployee.storeId);

    if (!customer) {
      const err = API_CUSTOMER_ERRORS['api/user-is-not-a-customer'];
      throw new ApolloError(err.msg, err.code);
    }

    const store = await Store.init(this.dbEmployee.storeId);

    const pointsAmount = store.convertPriceToPoints(price);

    const [points, customerUser] = await Promise.all([
      db.Point.create({
        customerId: customer.dbCustomer.id,
        employeeId: this.dbEmployee.id,
        storeId: this.dbEmployee.storeId,
        amount: pointsAmount,
      }),
      customer.dbCustomer.getUser(),
    ]);

    sendCustomerPointsLoadedEmail({
      SENDER_NAME: store.dbStore.name,
      EMAIL_TO: customerUser.email,
      TOPIC_NAME: emailsConstants.TOPICS.CUSTOMER_POINTS_LOADED,
      DATA: {
        CUSTOMER_FIRST_NAME: customerUser.firstName,
        STORE_NAME: store.dbStore.name,
        POINTS_AMOUNT: pointsAmount,
        STORE_PROFILE_IMAGE: store.dbStore.profileImage,
        STORE_URL: `${process.env.USERS_BASE_URL}/${store.dbStore.alias}`,
      },
    });

    return points.get({ plain: true });
  }

  async createCoupon(price) {
    const couponCode = await Coupon.generateRandomCode(this.dbEmployee.storeId);
    const store = await Store.init(this.dbEmployee.storeId);
    const points = store.convertPriceToPoints(price);

    return Coupon.create({
      storeId: this.dbEmployee.storeId,
      couponCode,
      points,
      usageFrequency: {
        type: USAGE_FREQUENCIES.ONE_TIME,
      },
    });
  }

  async getStoreLevels() {
    const levels = await db.StoreLevel.findAll({
      where: { storeId: this.dbEmployee.storeId },
    });

    return levels;
  }

  async getStoreStatistics() {
    const StoreStatistics = {
      customersTotal: 0,
      level1: 0,
      level2: 0,
      level3: 0,
      rewards: {
        total: 0,
        level1: 0,
        level2: 0,
        level3: 0,
      },
    };

    await Promise.all([
      db.CustomerLevel.findAll({
        where: { storeId: this.dbEmployee.storeId },
        attributes: ['level', [sequelize.fn('count', sequelize.col('customerId')), 'total']],
        group: ['level'],
        raw: true,
      }).then(levels => {
        levels.forEach(value => {
          StoreStatistics.customersTotal += value.total;
          StoreStatistics[`level${value.level}`] = value.total;
        });
      }),

      db.StoreRewardUsage.findAll({
        include: {
          model: db.StoreReward.unscoped(),
          where: { storeId: this.dbEmployee.storeId },
          include: db.StoreLevel.unscoped(),
        },
        raw: true,
      }).then(rewards => {
        rewards.forEach(reward => {
          StoreStatistics.rewards.total += 1;
          StoreStatistics.rewards[`level${reward['StoreReward.StoreLevel.level']}`] += 1;
        });
      }),
    ]);

    return StoreStatistics;
  }

  async getStore() {
    const store = await this.dbEmployee.getStore();
    return store.get({ plain: true });
  }

  async getStoreCustomers() {
    const customers = await db.CustomerProfile.findAll({
      where: { storeId: this.dbEmployee.storeId },
      include: [db.CustomerLevel, db.User],
    });

    return customers;
  }

  async registerStoreRewardUsage(userId, storeRewardId, storeId) {
    const customer = await this.getCustomer(userId);

    if (!customer) {
      const err = API_CUSTOMER_ERRORS['api/user-is-not-a-customer'];
      throw new ApolloError(err.msg, err.code);
    }

    const storeReward = await db.StoreReward.findByPk(storeRewardId, {
      include: [
        {
          model: db.StoreRewardUsage,
          where: { customerId: customer.id },
          limit: 1,
          order: [['createdAt', 'DESC']],
        },
      ],
    });

    const doesRewardBelongToStore = storeReward && storeReward.storeId === Number(storeId);

    if (!storeReward || !doesRewardBelongToStore) {
      const err = API_EMPLOYEE_ERRORS['api/invalid-store-reward'];
      throw new ApolloError(err.msg, err.code);
    }

    const usageFrequency = storeReward.get('UsageFrequency');
    const storeRewardUsages = storeReward.get('StoreRewardUsages');
    const lastTimeUsed = storeRewardUsages[0];

    const isRewardAvailable = usageValidatorService.isValidToRequestNow({
      usageFrequency,
      lastTimeUsed,
    });

    if (!isRewardAvailable) {
      const err = API_EMPLOYEE_ERRORS['api/unavailable-store-reward'];
      throw new ApolloError(err.msg, err.code);
    }

    const storeRewardUsage = await db.StoreRewardUsage.create({
      customerId: customer.id,
      employeeId: this.dbEmployee.id,
      storeRewardId,
    });

    return storeRewardUsage.get({
      plain: true,
    });
  }
}

module.exports = Employee;
