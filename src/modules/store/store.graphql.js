const { Store } = require('@models');

const storeInfoByAlias = (_, args) => {
  return Store.storeInfoByAlias(args);
};

const storeLevelsAndRewardsById = (_, args) => {
  return Store.storeLevelsAndRewards(args);
};

const storeCategories = (_, args) => {
  return Store.getStoreCategories(args);
};

const storeCountries = (_, args) => {
  return Store.getStoreCountries(args);
};

const validateStoreAlias = (_, args) => {
  return Store.validateAlias(args.data);
};

const generateValidStoreAlias = (_, args) => {
  return Store.generateAlias(args.name);
};

const createStoreForUser = (_, args, context) => {
  const user = context.reqUser().raw();

  return Store.create(user.id, args.store);
};

const typeDefs = `
  type Store {
    id: ID!
    name: String!
    profileImage: String!
    description: String
    alias: String!
    category: String
    socialMedia: JSON
    countryCode: String
    countryAreaCode: String
    private: Boolean
    fixedLevels: Boolean
    cardModule: Boolean
    StoreLocations: [StoreLocation]
    EmployeeProfiles: [EmployeeProfile]
    CustomerProfiles: [CustomerProfile]
    StoreRewards: [StoreReward]
    StoreSetting: StoreSetting
    createdAt: DateTime!
  }

  input StoreLocationInput {
    id: ID
    website: String
    address: String
    geo: GeometryInput
    email: String
    phone: String
  }

  type StoreLocation {
    id: ID!
    website: String
    address: String
    geo: Geometry
    email: String
    phone: String
  }

  type StoreLevel {
    id: ID!
    level: Int!
    points: Int!
    createdAt: DateTime!
    updatedAt: DateTime!
    storeId: Int
    StoreRewards: [StoreReward]
  }

  type StoreReward {
    id: ID!
    storeId: Int!
    storeLevelId: ID!
    createdAt: DateTime!
    updatedAt: DateTime!
    title: String
    description: String
    isAvailable: Boolean
    lastTimeUsed: DateTime
    nextTimeAvailable: DateTime
    isConsideredUsed: Boolean
    UsageFrequency: UsageFrequency
    StoreRewardUsages: [StoreRewardUsage]
  }

  type StoreRewardUsage {
    id: ID!
    storeRewardId: ID!
    CustomerProfile: CustomerProfile
    employeeId: ID!
    createdAt: DateTime!
    updatedAt: DateTime!
  }

  type Statistics {
    total: Int
    level1: Int
    level2: Int
    level3: Int
  }

  type StoreStatistics {
    customersTotal: Int
    level1: Int
    level2: Int
    level3: Int
    rewards: Statistics
  }

  type StoreCategory {
    name: String!
    displayName: String!
  }

  type StoreCountry {
    name: String!
    displayName: String!
    conversionRate: Int!
    areaCode: String!
  }

  type StoreSetting {
    personalPhone: String
    spendingTimes: String
    spendingAmount: Int
  }

  input StoreSettingInput {
    personalPhone: String!
    spendingTimes: String!
    spendingAmount: Int!
  }

  input CreateStoreInput {
    profileImage: Upload
    name: String!
    category: String!
    alias: String
    description: String!
    countryCode: String
    StoreLocations: [StoreLocationInput]
    level2: String
    level3: String
    socialMedia: JSON
    StoreSetting: StoreSettingInput
  }

  input CreateStoreRewardInput {
    storeLevelId: ID!
    title: String!
    description: String
    usageFrequencyType: UsageFrequencyType!
    usageFrequencyValue: String
  }

  extend type Mutation {
    createStoreForUser(store: CreateStoreInput!): Store!
  }

  # the schema allows the following query:
  extend type Query {
    generateValidStoreAlias(name: String!): String
    validateStoreAlias(data: String!): Boolean
    storeInfoByAlias(alias: String!): Store
    storeLevelsAndRewardsById(storeId: ID!): [StoreLevel]
    stores: [Store]
    storeCategories: [StoreCategory]
    storeCountries: [StoreCountry]
  }
`;

const resolvers = {
  Mutation: {
    createStoreForUser,
  },
  Query: {
    generateValidStoreAlias,
    validateStoreAlias,
    storeLevelsAndRewardsById,
    storeInfoByAlias,
    stores: () => [],
    storeCategories,
    storeCountries,
  },
};

module.exports = { typeDefs, resolvers };
