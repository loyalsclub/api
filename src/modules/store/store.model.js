const { ApolloError } = require('apollo-server');
const Op = require('sequelize').Op;

const db = require('@db');
const { API_USER_ERRORS, API_STORE_ERRORS } = require('@modules/core/constants/error-codes.constants');
const {
  POINT_VALUE_BY_COUNTRY,
  STORE_CATEGORIES,
  STORE_COUNTRIES,
} = require('@modules/store/constants/store.constants');
const User = require('@modules/user/user.model');
const { sendPartnerWelcomeEmail, addPartnerToList } = require('@services/email.service');
const { uploadStoreProfileImage } = require('@services/images.service');
const { escapeSpecialChars } = require('@utils/string-utils');

class Store {
  constructor(dbStore) {
    this.dbStore = dbStore;
  }

  static async init(storeId) {
    const store = await db.Store.findOne({
      where: { id: storeId },
      include: [{ model: db.StoreLocation }],
    });

    if (!store) {
      const err = API_STORE_ERRORS['api/store-not-found'];
      throw new ApolloError(err.msg, err.code);
    }

    return new Store(store);
  }

  static async validateAlias(alias) {
    if (!alias || alias == '') return false;

    const store = await db.Store.findOne({
      where: { alias },
      attributes: ['id'],
    });

    return !store;
  }

  static async storeLevelsAndRewards({ storeId }) {
    const levels = await db.StoreLevel.findAll({
      where: { storeId },
    });

    const err = API_USER_ERRORS['api/store-not-found'];
    if (!levels) throw new ApolloError(err.msg, err.code);

    return levels;
  }

  static async storeInfoByAlias({ alias }) {
    const store = await db.Store.findOne({
      where: { alias },
      include: [{ model: db.StoreLocation }],
    });

    return store;
  }

  static async create(userId, store) {
    const profileImage = await store.profileImage;

    const { key: relativeProfileImageUrl } = await uploadStoreProfileImage({
      mimetype: profileImage.mimetype,
      stream: profileImage.createReadStream(),
    });

    const user = await User.initId(userId);

    if (!store.alias) {
      store.alias = await Store.generateAlias(store.name);
    }

    const createdStore = await user.createStore({
      ...store,
      profileImage: relativeProfileImageUrl,
    });

    const rawUser = user.raw();

    const partnerEmailData = {
      FIRST_NAME: rawUser.firstName,
      LAST_NAME: rawUser.lastName,
      EMAIL_TO: rawUser.email,
      STORE_NAME: createdStore.name,
      STORE_PROFILE_IMAGE: createdStore.profileImage,
    };

    sendPartnerWelcomeEmail({ ...partnerEmailData, STORE_PROFILE_IMAGE: createdStore.profileImage });

    addPartnerToList({ ...partnerEmailData });

    return createdStore;
  }

  async update(data) {
    let relativeProfileImageUrl;

    const hasNewProfileImage = Boolean(data.profileImage);

    if (hasNewProfileImage) {
      const profileImage = await data.profileImage;

      const { key } = await uploadStoreProfileImage({
        mimetype: profileImage.mimetype,
        stream: profileImage.createReadStream(),
      });

      relativeProfileImageUrl = key;
    }

    const hasAliasChanged = !data.alias || data.alias !== this.dbStore.alias;

    if (hasAliasChanged) {
      const isAliasValid = await Store.validateAlias(data.alias);

      if (!isAliasValid) {
        const error = API_STORE_ERRORS['api/invalid-store-alias'];
        throw new ApolloError(error.msg, error.code);
      }
    }

    const { name, category, alias, description, StoreLocations, socialMedia } = data;

    if (StoreLocations) {
      await this.updateStoreLocations(StoreLocations);
    }

    const updatedStore = await this.dbStore.update({
      name,
      category,
      alias,
      description,
      socialMedia,
      profileImage: relativeProfileImageUrl,
    });

    updatedStore.setDataValue('StoreLocations', await updatedStore.getStoreLocations());

    return updatedStore.get({ plain: true });
  }

  async updateStoreLocations(storeLocations) {
    return Promise.all(
      storeLocations.map(({ id, ...storeLocation }) => {
        const data = {
          storeId: this.dbStore.id,
          ...storeLocation,
        };

        if (id) {
          return db.StoreLocation.update(data, { where: { id } });
        } else {
          return db.StoreLocation.create(data);
        }
      }),
    );
  }

  static async generateAlias(name) {
    const trimmedName = name.trim();
    const escapedSpecialChars = escapeSpecialChars(trimmedName);
    const duplicatesRemoved = escapedSpecialChars.replace(/([-_\s])\1+/g, '$1');
    const hyphenWithSurroundingSpacesReplaced = duplicatesRemoved.replace(/\s-\s/g, '-');
    const escapedWords = hyphenWithSurroundingSpacesReplaced.split(' ');
    const lowerCasedWords = escapedWords.map(word => word.toLowerCase());
    const alias = lowerCasedWords.join('.');

    const isAliasValid = await Store.validateAlias(alias);

    if (!isAliasValid) {
      const existingAliases = await db.Store.count({
        where: { alias: { [Op.like]: `${alias}_` } },
      });

      return `${alias}${existingAliases + 1}`;
    }

    return alias;
  }

  static getStoreCategories() {
    return STORE_CATEGORIES;
  }

  static getStoreCountries() {
    return STORE_COUNTRIES;
  }

  convertPriceToPoints(price) {
    const pointValue = POINT_VALUE_BY_COUNTRY[this.dbStore.countryCode];
    return Math.round(price / pointValue);
  }

  remove() {
    return this.dbStore.destroy({ individualHooks: true });
  }
}

module.exports = Store;
