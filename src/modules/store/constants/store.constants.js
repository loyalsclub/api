const COUNTRY_CODES = {
  ARGENTINA: 'AR',
  MEXICO: 'MX',
  CHILE: 'CL',
  COLOMBIA: 'CO',
  SPAIN: 'ES',
};

const POINT_VALUE_BY_COUNTRY = {
  [COUNTRY_CODES.ARGENTINA]: 20,
  [COUNTRY_CODES.CHILE]: 1000,
  [COUNTRY_CODES.COLOMBIA]: 2000,
  [COUNTRY_CODES.MEXICO]: 10,
  [COUNTRY_CODES.SPAIN]: 1,
};

const STORE_COUNTRIES = [
  {
    name: COUNTRY_CODES.ARGENTINA,
    displayName: 'Argentina',
    conversionRate: POINT_VALUE_BY_COUNTRY[COUNTRY_CODES.ARGENTINA],
    areaCode: '54',
  },
  {
    name: COUNTRY_CODES.CHILE,
    displayName: 'Chile',
    conversionRate: POINT_VALUE_BY_COUNTRY[COUNTRY_CODES.CHILE],
    areaCode: '56',
  },
  {
    name: COUNTRY_CODES.COLOMBIA,
    displayName: 'Colombia',
    conversionRate: POINT_VALUE_BY_COUNTRY[COUNTRY_CODES.COLOMBIA],
    areaCode: '57',
  },
  {
    name: COUNTRY_CODES.SPAIN,
    displayName: 'España',
    conversionRate: POINT_VALUE_BY_COUNTRY[COUNTRY_CODES.SPAIN],
    areaCode: '34',
  },
  {
    name: COUNTRY_CODES.MEXICO,
    displayName: 'México',
    conversionRate: POINT_VALUE_BY_COUNTRY[COUNTRY_CODES.MEXICO],
    areaCode: '52',
  },
];

const STORE_CATEGORIES = [
  {
    name: 'dress',
    displayName: 'Indumentaria y Accesorios',
  },
  {
    name: 'services',
    displayName: 'Servicios',
  },
  {
    name: 'coffee',
    displayName: 'Café y Pastelería',
  },
  {
    name: 'gastronomy',
    displayName: 'Gastronomía',
  },
  {
    name: 'market',
    displayName: 'Supermercado',
  },
  {
    name: 'bar',
    displayName: 'Bar',
  },
  {
    name: 'other',
    displayName: 'Otros',
  },
];

const STORE_NOTIFICATIONS_TYPES = {
  NEW_CUSTOMER: 'NEW_CUSTOMER',
  CUSTOMER_ACTIVATED_POINTS: 'CUSTOMER_ACTIVATED_POINTS',
};

const STORE_NOTIFICATIONS = {
  [STORE_NOTIFICATIONS_TYPES.NEW_CUSTOMER]: {
    title: ({ storeName }) => `¡${storeName} está creciendo!`,
    body: ({ customerName }) => `${customerName} se sumó a tu Club de Clientes.`,
  },
  [STORE_NOTIFICATIONS_TYPES.CUSTOMER_ACTIVATED_POINTS]: {
    title: () => ' ¡Tus clientes están sumando puntos!',
    body: ({ customerName, pointsAmount }) => `${customerName} activó los ${pointsAmount} puntos que le enviaste.`,
  },
};

module.exports = {
  COUNTRY_CODES,
  POINT_VALUE_BY_COUNTRY,
  STORE_CATEGORIES,
  STORE_COUNTRIES,
  STORE_NOTIFICATIONS,
  STORE_NOTIFICATIONS_TYPES,
};
