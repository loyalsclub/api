const db = require('@db');
const { STORE_NOTIFICATIONS_TYPES, STORE_NOTIFICATIONS } = require('@modules/store/constants/store.constants');
const pushNotificationService = require('@services/push-notifications/push-notifications.service');
const { isDemoStore } = require('@utils/roles-validator');

class NotificationManager {
  static async sendPushToStore({ type, storeId, data }) {
    if (isDemoStore(storeId)) {
      return false;
    }

    const employee = await db.EmployeeProfile.findOne({
      where: { storeId },
      include: [{ model: db.User }, { model: db.Store }],
    });

    if (!employee || !employee.User || !employee.User.pushToken) {
      return false;
    }

    let response;

    switch (type) {
      case STORE_NOTIFICATIONS_TYPES.NEW_CUSTOMER:
        response = await pushNotificationService.sendOne({
          pushToken: employee.User.pushToken,
          title: STORE_NOTIFICATIONS.NEW_CUSTOMER.title({ storeName: employee.Store.name }),
          body: STORE_NOTIFICATIONS.NEW_CUSTOMER.body(data),
        });
        break;
      case STORE_NOTIFICATIONS_TYPES.CUSTOMER_ACTIVATED_POINTS:
        response = await pushNotificationService.sendOne({
          pushToken: employee.User.pushToken,
          title: STORE_NOTIFICATIONS.CUSTOMER_ACTIVATED_POINTS.title(),
          body: STORE_NOTIFICATIONS.CUSTOMER_ACTIVATED_POINTS.body(data),
        });
        break;
    }

    return response;
  }
}

module.exports = NotificationManager;
