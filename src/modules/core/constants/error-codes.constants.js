const FIREBASE_AUTH_ERRORS = {
  'auth/email-already-exists': 'auth/email-already-exists',
  'auth/invalid-token': 'auth/invalid-token',
};

const API_AUTH_ERRORS = {
  'auth/user-is-not-authorized': {
    msg: 'auth/user-is-not-authorized',
    code: 800,
  },
};

const API_USER_ERRORS = {
  'api/user-not-found': {
    msg: 'api/user-not-found',
    code: '3001',
  },
  'api/store-not-found': {
    msg: 'api/store-not-found',
    code: '3002',
  },
  'api/customer-exists': {
    msg: 'api/customer-exists',
    code: '3003',
  },
  'api/invalid-user-creation': {
    msg: 'api/invalid-user-creation',
    code: '3000',
  },
};

const API_CUSTOMER_ERRORS = {
  'api/user-is-not-a-customer': {
    msg: 'api/user-is-not-a-customer',
    code: '999',
  },
  'api/coupon-not-found': {
    msg: 'api/coupon-not-found',
    code: '1000',
  },
  'api/coupon-not-available': {
    msg: 'api/coupon-not-available',
    code: '1001',
  },
  'api/insufficient-points': {
    msg: 'api/insufficient-points',
    code: '1002',
  },
  'api/error-acquiring-coupon': {
    msg: 'api/error-acquiring-coupon',
    code: '1003',
  },
  'api/coupon-already-exists': {
    msg: 'api/coupon-already-exists',
    code: '1004',
  },
  'api/coupon-invalid-time-frame': {
    msg: 'api/coupon-invalid-time-frame',
    code: '1005',
  },
  'api/coupon-is-not-active': {
    msg: 'api/coupon-is-not-active',
    code: '1006',
  },
  'api/could-not-create-customer': {
    msg: 'api/could-not-create-customer',
    code: '1007',
  },
};

const API_EMPLOYEE_ERRORS = {
  'api/transaction-not-found': {
    msg: 'api/transaction-not-found',
    code: '2000',
  },
  'api/transaction-redeemed': {
    msg: 'api/transaction-redeemed',
    code: '2001',
  },
  'api/invalid-store-reward': {
    msg: 'api/invalid-store-reward',
    code: '2002',
  },
  'api/unavailable-store-reward': {
    msg: 'api/unavailable-store-reward',
    code: '2003',
  },
};

const API_STORE_ERRORS = {
  'api/store-not-found': {
    msg: 'api/store-not-found',
    code: '4000',
  },
  'api/store-modification-forbidden': {
    msg: 'api/store-modification-forbidden',
    code: '4001',
  },
  'api/invalid-store-alias': {
    msg: 'api/invalid-store-alias',
    code: '4002',
  },
};

const API_STORE_REWARD_ERRORS = {
  'api/store-reward-not-found': {
    msg: 'api/store-reward-not-found',
    code: '5000',
  },
  'api/store-level-not-found': {
    msg: 'api/store-level-not-found',
    code: '5001',
  },
  'api/store-level-invalid': {
    msg: 'api/store-level-invalid',
    code: '5002',
  },
};

const SERVER_ERROR = {
  'server/database-connection': {
    msg: 'server/database-connection',
    code: 'INTERNAL_SERVER_ERROR',
  },
  'server/database-error': {
    msg: 'server/database-error',
    code: 'INTERNAL_SERVER_ERROR',
  },
};

module.exports = {
  SERVER_ERROR,
  FIREBASE_AUTH_ERRORS,
  API_AUTH_ERRORS,
  API_USER_ERRORS,
  API_CUSTOMER_ERRORS,
  API_EMPLOYEE_ERRORS,
  API_STORE_ERRORS,
  API_STORE_REWARD_ERRORS,
};
