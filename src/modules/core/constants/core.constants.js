const CUSTOMER_STATUS = {
  ACTIVE: 'active',
  PENDING: 'pending',
  DELETED: 'deleted',
};

const COUPON_TYPES = {
  IN_APP: 'in-app',
  WELCOME: 'welcome',
};

const COUPON_STATUS = {
  ACTIVE: 'active',
  PAUSED: 'paused',
};

const FOREVER_FREQUENCY_MIN_VALID_HOURS = 24;

const TRANSACTION_STATUS = {
  PENDING: 'pending',
  REDEEMED: 'redeemed',
};

const USER_ROLES = {
  ADMIN: 'admin',
  USER: 'user',
};

const USAGE_FREQUENCIES = {
  FOREVER: 'forever',
  ONE_TIME: 'oneTime',
  EVERY_CERTAIN_DAYS: 'everyCertainDays',
  EVERY_CERTAIN_HOURS: 'everyCertainHours',
  CERTAIN_WEEK_DAYS: 'certainWeekDays',
};

module.exports = {
  USER_ROLES,
  COUPON_TYPES,
  CUSTOMER_STATUS,
  COUPON_STATUS,
  TRANSACTION_STATUS,
  FOREVER_FREQUENCY_MIN_VALID_HOURS,
  USAGE_FREQUENCIES,
};
