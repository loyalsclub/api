const checkStatus = () => true;

const typeDefs = `
  extend type Query {
    checkStatus: Boolean
  }

  type PageInfo {
    startCursor: String
    endCursor: String
    hasNextPage: Boolean
    hasPreviousPage: Boolean
  }

  type CountStat {
    date: DateTime
    count: Int
  }

  enum Order {
    ASC
    DESC
  }

  input SortBy {
    field: String!
    order: Order!
  }
`;

const resolvers = {
  Query: {
    checkStatus,
  },
};

module.exports = { typeDefs, resolvers };
