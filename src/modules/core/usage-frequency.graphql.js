const { USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');

const typeDefs = `
  enum UsageFrequencyType {
    ${USAGE_FREQUENCIES.FOREVER}
    ${USAGE_FREQUENCIES.ONE_TIME}
    ${USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS}
    ${USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS}
    ${USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS}
  }

  type UsageFrequency {
    id: ID!
    type: UsageFrequencyType!
    value: String
    createdAt: DateTime!
    updatedAt: DateTime!
  }
`;

const resolvers = {};

module.exports = { typeDefs, resolvers };
