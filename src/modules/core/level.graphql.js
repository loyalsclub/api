const typeDefs = `
  type Point {
    id: ID! 
    storeId: Int!
    createdAt: DateTime!  
    updatedAt: DateTime!  
    customerId: Int!
    Store: Store
    amount: Int!
  }
`;

const resolvers = {};

module.exports = { typeDefs, resolvers };
