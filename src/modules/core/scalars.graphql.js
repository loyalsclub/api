const { GraphQLScalarType } = require('graphql');
const { EmailAddressResolver } = require('graphql-scalars');
const { Kind } = require('graphql/language');

const typeDefs = `
  scalar DateTime
  scalar JSON
  scalar Email

  input GeometryInput {
    type: String
    coordinates: [Float]
  }

  type Geometry {
    type: String
    coordinates: [Float]
  }
`;

const resolvers = {
  Email: EmailAddressResolver,
  DateTime: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value) {
      return new Date(value); // value from the client
    },
    serialize(value) {
      return value; // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10); // ast value is always in string format
      }
      if (ast.kind == Kind.STRING) {
        return ast.value;
      }
    },
  }),
};

module.exports = { typeDefs, resolvers };
