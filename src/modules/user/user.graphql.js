const R = require('ramda');

const { User } = require('@models');
const { sendUserWelcomeEmail, addUserToList } = require('@services/email.service');

const createUser = async (_, args) => {
  const user = await User.createUser(args);
  const loginToken = await user.createLoginToken();
  const rawUser = user.raw();

  await sendUserWelcomeEmail({
    FIRST_NAME: rawUser.firstName,
    LAST_NAME: rawUser.lastName,
    EMAIL_TO: rawUser.email,
    ALIAS: rawUser.alias,
  });

  await addUserToList({
    FIRST_NAME: rawUser.firstName,
    LAST_NAME: rawUser.lastName,
    EMAIL: rawUser.email,
  });

  return R.assoc('loginToken', loginToken, rawUser);
};

const completePendingUser = async (_, args) => {
  const user = await User.completePendingUser(args);
  const loginToken = await user.createLoginToken();
  const rawUser = user.raw();

  await sendUserWelcomeEmail({
    FIRST_NAME: rawUser.firstName,
    LAST_NAME: rawUser.lastName,
    EMAIL_TO: rawUser.email,
    ALIAS: rawUser.alias,
  });

  await addUserToList({
    FIRST_NAME: rawUser.firstName,
    LAST_NAME: rawUser.lastName,
    EMAIL: rawUser.email,
  });

  return R.assoc('loginToken', loginToken, rawUser);
};

const createPartnerUser = async (_, args) => {
  const user = await User.createUser(args);
  const loginToken = await user.createLoginToken();
  const rawUser = user.raw();

  return R.assoc('loginToken', loginToken, rawUser);
};

const completeUserSocialLogin = async (_, args, context) => {
  const user = context.reqUser();
  await user.completeUserSocialLogin(args);
  const { firstName, lastName, email, alias } = user.raw();

  await sendUserWelcomeEmail({
    FIRST_NAME: firstName,
    LAST_NAME: lastName,
    EMAIL_TO: email,
    ALIAS: alias,
  });

  await addUserToList({
    FIRST_NAME: firstName,
    LAST_NAME: lastName,
    EMAIL: email,
  });
  return user.raw();
};

const userInfo = async (_, args, context) => {
  const user = context.reqUser();
  return user.raw();
};

const findPendingUser = async (_, args) => {
  const { email } = args;
  return User.findPendingUser(email);
};

const isValidUser = async (_, args, context) => {
  const pendingUserSL = context.reqUser();
  return pendingUserSL.isValidUser();
};

const extractUserAndUpdate = async (_, args, context) => {
  const user = context.reqUser();

  await user.updateUser(args);

  return user.raw();
};

const updateUser = (...args) =>
  extractUserAndUpdate(...args).then(rawUser => {
    const { firstName, lastName, email } = rawUser;

    addUserToList({
      FIRST_NAME: firstName,
      LAST_NAME: lastName,
      EMAIL: email,
    });

    return rawUser;
  });

const registerUserPushToken = extractUserAndUpdate;

const updateUserWebPushSubscription = extractUserAndUpdate;

const typeDefs = `
  type User {
    id: ID!
    firstName: String
    lastName: String
    email: Email!
    alias: String!
    birthDate: DateTime
    loginToken: String
    pushToken: String
    webPushSubscription: JSON
  }

  type PendingUser {
    isPending: Boolean
    firstName: String
    lastName: String
    email:  Email
  }

  type Query {
    userInfo: User
    findPendingUser(email: String!): PendingUser
    isValidUser: Boolean
  }

  type Mutation {
    createUser (
      firstName: String!
      lastName: String!
      email: Email!
      birthDate: DateTime!
      password: String!
    ): User
    completePendingUser (
      firstName: String!
      lastName: String!
      email: Email!
      birthDate: DateTime!
      password: String!
    ): User
    completeUserSocialLogin(
      isPending: Boolean
      firstName: String!
      lastName: String!
      email: Email!
      birthDate: DateTime!
    ): User
    createPartnerUser (
      firstName: String!
      lastName: String!
      email: Email!
      birthDate: DateTime
      password: String!
    ): User
    updateUser (
      firstName: String
      lastName: String
    ): User
    registerUserPushToken (
      pushToken: String
    ): User
    updateUserWebPushSubscription (
      webPushSubscription: JSON
    ): User
  }
`;

const resolvers = {
  Mutation: {
    createUser,
    createPartnerUser,
    completePendingUser,
    completeUserSocialLogin,
    updateUser,
    registerUserPushToken,
    updateUserWebPushSubscription,
  },
  Query: {
    userInfo,
    findPendingUser,
    isValidUser,
  },
};

module.exports = { typeDefs, resolvers };
