const { ApolloError } = require('apollo-server');
const Op = require('sequelize').Op;

const db = require('@db');
const { API_USER_ERRORS } = require('@modules/core/constants/error-codes.constants');
const Customer = require('@modules/customer/customer.model');
const admin = require('@services/authentication/authentication.service');

const PERMISSIONS = {
  admin: 1,
  employee: 2,
};

class User {
  constructor(fbUser, dbUser) {
    this.fbUser = fbUser;
    this.dbUser = dbUser;
  }

  static async createUser(userData) {
    let dbUser, fbUser;

    // Create user in the database, handle errors in case of failure.
    dbUser = await db.User.create(userData);

    // Create user in firebase, handle errors in case of failure.
    try {
      userData.displayName = `${userData.firstName} ${userData.lastName}`;
      fbUser = await admin.auth().createUser(userData);

      await dbUser.update({ uid: fbUser.uid });
    } catch (error) {
      // If there is an error we destroy the registry on the database.
      if (fbUser) {
        admin.auth().deleteUser(fbUser.uid);
      }
      await dbUser.destroy();
      throw error;
    }

    return new User(fbUser, dbUser);
  }

  static async createPendingUser(userData) {
    // This method is meant to create the user Only in the database.
    // NOT AUTH DATA PROVIDED.

    // Create user in the database, handle errors in case of failure.
    const dbUser = await db.User.create(userData);

    return new User({}, dbUser);
  }

  static async completePendingUser(userData) {
    const { firstName, lastName, birthDate, email } = userData;
    let dbUser, fbUser;

    dbUser = await db.User.findOne({
      where: { email },
    });

    if (!dbUser || dbUser.uid) {
      const err = API_USER_ERRORS['api/invalid-user-creation'];
      throw new ApolloError(err.msg, err.code);
    }

    // Create user in firebase, handle errors in case of failure.
    try {
      userData.displayName = `${userData.firstName} ${userData.lastName}`;
      fbUser = await admin.auth().createUser(userData);

      await dbUser.update({ uid: fbUser.uid, lastName: lastName, firstName: firstName, birthDate: birthDate });
    } catch (error) {
      if (fbUser) await admin.auth().deleteUser(fbUser.uid);
      if (dbUser) await dbUser.destroy();
      throw error;
    }

    return new User(fbUser, dbUser);
  }

  static async findPendingUser(email) {
    const pendingUser = await db.User.findOne({
      where: { email },
      raw: true,
    });

    if (!pendingUser) {
      return null;
    }

    if (pendingUser.uid) {
      return { isPending: false };
    }

    return {
      isPending: true,
      ...pendingUser,
    };
  }

  static async initId(id) {
    const dbUser = await db.User.findOne({ where: { id } });

    const err = API_USER_ERRORS['api/user-not-found'];

    if (!dbUser) {
      throw new ApolloError(err.msg, err.code);
    }

    return new User({}, dbUser);
  }

  static async initEmailOrAlias(val) {
    const dbUser = await db.User.findOne({
      where: {
        [Op.or]: [{ alias: val }, { email: val }],
      },
    });

    const err = API_USER_ERRORS['api/user-not-found'];

    if (!dbUser) {
      throw new ApolloError(err.msg, err.code);
    }

    return new User({}, dbUser);
  }

  static async findUserByEmailOrAlias(val) {
    const dbUser = await db.User.findOne({
      where: {
        [Op.or]: [{ alias: val }, { email: val }],
      },
    });

    return dbUser ? new User({}, dbUser) : null;
  }

  isValidUser() {
    if (this.dbUser !== null && typeof this.dbUser !== undefined) {
      return true;
    }

    return false;
  }

  updateUser(userData) {
    return this.dbUser.update(userData);
  }

  async completeUserSocialLogin(userData) {
    const { email } = userData;
    let dbUser;
    userData.uid = this.fbUser.uid;

    if (userData.isPending) {
      dbUser = await db.User.findOne({
        where: {
          email,
          uid: {
            [Op.or]: [{ [Op.in]: ['NULL', ''] }, { [Op.is]: null }],
          },
        },
      });

      await dbUser.update(userData);
    } else {
      dbUser = await db.User.create(userData);
    }

    this.dbUser = dbUser;
  }

  createLoginToken() {
    return admin.auth().createCustomToken(this.fbUser.uid);
  }

  async createStore(storeData) {
    storeData.EmployeeProfiles = {
      userId: this.dbUser.id,
      permissionId: PERMISSIONS.admin,
    };

    storeData.StoreLevels = [
      { level: 1, points: 0 },
      { level: 2, points: storeData.level2 },
      { level: 3, points: storeData.level3 },
    ];

    const include = [db.EmployeeProfile, db.StoreLevel];

    if (storeData.StoreLocations) {
      include.push(db.StoreLocation);
    }

    if (storeData.StoreSetting) {
      include.push(db.StoreSetting);
    }

    const store = await db.Store.create(storeData, { include });

    return store.get({ plain: true });
  }

  async getEmployeeProfiles() {
    const profiles = await db.EmployeeProfile.findAll({
      where: { userId: this.dbUser.id },
      include: [{ model: db.Store }, { model: db.User }],
    });

    const rawProfiles = profiles.map(profile => profile.get({ plain: true }));

    return rawProfiles;
  }

  async getCustomerProfiles(status) {
    const where = { userId: this.dbUser.id };

    if (status) {
      where.status = status;
    }

    const profiles = await db.CustomerProfile.findAll({
      where,
      include: [db.Store, db.CustomerLevel],
    });

    const customers = profiles.map(profile => new Customer(profile));

    return Promise.all(customers.map(customer => customer.raw()));
  }

  async getCustomerProfileByStoreId(storeId) {
    const profile = await db.CustomerProfile.findOne({
      where: {
        userId: this.dbUser.id,
        storeId: storeId,
      },
      include: [
        {
          model: db.Store,
        },
      ],
    });

    return new Customer(profile).raw();
  }

  raw() {
    return this.dbUser.get({
      plain: true,
    });
  }
}
module.exports = User;
