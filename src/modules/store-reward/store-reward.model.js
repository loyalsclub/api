const { ApolloError } = require('apollo-server');

const db = require('@db');
const { API_STORE_REWARD_ERRORS } = require('@modules/core/constants/error-codes.constants');

class StoreReward {
  constructor(dbStoreReward) {
    this.dbStoreReward = dbStoreReward;
  }

  static async initById(id) {
    const dbStoreReward = await db.StoreReward.findByPk(id);

    if (!dbStoreReward) {
      const error = API_STORE_REWARD_ERRORS['api/store-reward-not-found'];
      throw new ApolloError(error.msg, error.code);
    }

    return new StoreReward(dbStoreReward);
  }

  static async create(storeRewardData, storeId = null) {
    const { storeLevelId, title, description, usageFrequencyType, usageFrequencyValue } = storeRewardData;

    const level = await db.StoreLevel.findOne({ where: { id: storeLevelId } });

    await StoreReward.validateCrudOperationOnStoreReward(level, storeId);

    const storeRewardUsageFrequency = await db.UsageFrequency.create({
      type: usageFrequencyType,
      value: usageFrequencyValue,
    });

    const storeReward = await level.createStoreReward({
      title,
      description: description || null,
      storeId: level.storeId,
      usageFrequencyId: storeRewardUsageFrequency.id,
    });

    storeReward.setDataValue('UsageFrequency', await storeReward.getUsageFrequency());

    return new StoreReward(storeReward);
  }

  async update(storeRewardData, storeId = null) {
    const { storeLevelId, title, description, usageFrequencyType, usageFrequencyValue } = storeRewardData;

    const level = await db.StoreLevel.findOne({ where: { id: storeLevelId } });

    await StoreReward.validateCrudOperationOnStoreReward(level, storeId);

    const { usageFrequencyId } = this.dbStoreReward;

    await db.UsageFrequency.update(
      {
        type: usageFrequencyType,
        value: usageFrequencyValue,
      },
      { where: { id: usageFrequencyId } },
    );

    await this.dbStoreReward.update({
      title,
      description: description || null,
      usageFrequencyId,
      storeLevelId,
    });

    this.dbStoreReward.setDataValue('UsageFrequency', await this.dbStoreReward.getUsageFrequency());

    return this.dbStoreReward.get({
      plain: true,
    });
  }

  static async remove(storeRewardId, storeId = null) {
    const storeReward = await db.StoreReward.findByPk(storeRewardId);

    const level = await db.StoreLevel.findOne({ where: { id: storeReward.storeLevelId } });

    await StoreReward.validateCrudOperationOnStoreReward(level, storeId);

    return db.StoreReward.destroy({
      where: { id: storeRewardId },
      individualHooks: true,
    });
  }

  static validateCrudOperationOnStoreReward(level, storeId = null) {
    if (!level) {
      const error = API_STORE_REWARD_ERRORS['api/store-level-not-found'];
      throw new ApolloError(error.msg, error.code);
    }

    const doesLevelBelongToStore = !storeId || Number(level.storeId) === Number(storeId);

    if (!doesLevelBelongToStore) {
      const error = API_STORE_REWARD_ERRORS['api/store-level-invalid'];
      throw new ApolloError(error.msg, error.code);
    }

    return true;
  }

  raw() {
    return this.dbStoreReward.get({
      plain: true,
    });
  }
}

module.exports = StoreReward;
