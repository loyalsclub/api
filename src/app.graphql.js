const { gql } = require('apollo-server');

const adminGraphql = require('@modules/admin/admin.graphql');
const levelGraphql = require('@modules/core/level.graphql');
const scalarsGraphql = require('@modules/core/scalars.graphql');
const systemGraphql = require('@modules/core/system.graphql');
const usageFrequencyGraphql = require('@modules/core/usage-frequency.graphql');
const couponGraphql = require('@modules/coupon/coupon.graphql');
const customerGraphql = require('@modules/customer/customer.graphql');
const employeeGraphql = require('@modules/employee/employee.graphql');
const storeGraphql = require('@modules/store/store.graphql');
const userGraphql = require('@modules/user/user.graphql');

const rootTypeDefs = ''
  .concat(adminGraphql.typeDefs)
  .concat(levelGraphql.typeDefs)
  .concat(scalarsGraphql.typeDefs)
  .concat(systemGraphql.typeDefs)
  .concat(usageFrequencyGraphql.typeDefs)
  .concat(couponGraphql.typeDefs)
  .concat(customerGraphql.typeDefs)
  .concat(employeeGraphql.typeDefs)
  .concat(storeGraphql.typeDefs)
  .concat(userGraphql.typeDefs);

const rootResolvers = [
  adminGraphql.resolvers,
  levelGraphql.resolvers,
  scalarsGraphql.resolvers,
  systemGraphql.resolvers,
  usageFrequencyGraphql.resolvers,
  couponGraphql.resolvers,
  customerGraphql.resolvers,
  employeeGraphql.resolvers,
  storeGraphql.resolvers,
  userGraphql.resolvers,
];

module.exports = { typeDefs: gql(rootTypeDefs), resolvers: rootResolvers };
