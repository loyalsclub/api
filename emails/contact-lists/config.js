const emailsConstants = require('@modules/core/constants/emails.constants');

module.exports = {
  ContactListName: emailsConstants.CONTACT_LIST_NAME,
  // eslint-disable-next-line prettier/prettier
  Description: "Contains the Customers' and Stores' users",
  Topics: [
    {
      TopicName: emailsConstants.TOPICS.CUSTOMER_PROFILE_CREATED,
      DisplayName: 'Te uniste a un nuevo comercio',
      Description: 'Notificación enviada al unirse a un nuevo Club de Clientes',
      DefaultSubscriptionStatus: 'OPT_IN',
    },
    {
      TopicName: emailsConstants.TOPICS.CUSTOMER_POINTS_LOADED,
      DisplayName: 'Sumaste puntos con un comercio',
      Description: 'Notificación enviada al sumar puntos con un Club de Clientes',
      DefaultSubscriptionStatus: 'OPT_IN',
    },
  ],
};
