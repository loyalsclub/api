const emailsConstants = require('@modules/core/constants/emails.constants');

const { getTemplateContent } = require('../email.utils');

/**
 * Variables
 * CUSTOMER_FIRST_NAME
 * STORE_NAME
 * STORE_PROFILE_IMAGE
 * STORE_URL
 * STORE_REWARDS
 *  title
 *  usageFrequency
 *  description
 */
module.exports = {
  TemplateName: emailsConstants.TEMPLATES.WELCOME_CUSTOMER,
  HtmlPart: getTemplateContent(__dirname),
  SubjectPart: '¡{{CUSTOMER_FIRST_NAME}}, bienvenido a nuestro Club de Clientes!',
};
