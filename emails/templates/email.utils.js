const fs = require('fs');
const path = require('path');

module.exports = {
  getTemplateContent: function (templateDir) {
    return fs.readFileSync(path.resolve(templateDir, 'template.html'), 'utf8');
  },
};
