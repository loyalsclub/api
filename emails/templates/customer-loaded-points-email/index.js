const emailsConstants = require('@modules/core/constants/emails.constants');

const { getTemplateContent } = require('../email.utils');

/**
 * Variables
 * CUSTOMER_FIRST_NAME
 * STORE_NAME
 * POINTS_AMOUNT
 * STORE_PROFILE_IMAGE
 * STORE_URL
 */
module.exports = {
  TemplateName: emailsConstants.TEMPLATES.POINTS_LOADED,
  HtmlPart: getTemplateContent(__dirname),
  SubjectPart: '¡Sumaste puntos! Gracias por elegirnos una vez más',
};
