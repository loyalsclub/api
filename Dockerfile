FROM node:10.15.1-slim

# Set up environment
ENV TERM=xterm-256color
ENV PROJECT_NAME loyals-club-api
ENV PROJECT_ROOT /opt/loyals-club-api

# Install dependencies
RUN mkdir -p /tmp/$PROJECT_NAME
ADD package.json /tmp/$PROJECT_NAME/package.json
# RUN cd /tmp/$PROJECT_NAME/ && npm install
RUN mkdir -p $PROJECT_ROOT
# RUN cp -a /tmp/$PROJECT_NAME/node_modules $PROJECT_ROOT

# Copy code
WORKDIR $PROJECT_ROOT
COPY . $PROJECT_ROOT
COPY package.json .
RUN npm install --quiet
# Start API
CMD [ "./bin/start.sh" ]
