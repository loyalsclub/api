module.exports = {
  development: {
    username: 'root',
    password: 'rootPassword',
    database: 'loyalsclub',
    host: 'localhost',
    dialect: 'mysql',
  },
  production: {
    username: 'root',
    password: 'rootPassword',
    database: 'loyalsclub',
    host: 'dd10107ex8mzbt0.cfx3pyxnava5.sa-east-1.rds.amazonaws.com',
    dialect: 'mysql',
  },
};
