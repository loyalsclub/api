const expect = require('chai').expect;
const firebase = require('firebase');

const { COUNTRY_CODES } = require('@modules/store/constants/store.constants');

const firebaseConfig = require('../config/firebase.config.json');

const firebaseAppConfig = firebaseConfig[process.env.NODE_ENV] || firebaseConfig.development;

const isFirebaseAppInitialized = firebase.apps.length > 0;

if (!isFirebaseAppInitialized) {
  firebase.initializeApp(firebaseAppConfig);
}

const apiUrl = 'http://localhost:3001';

const notCustomer = {
  id: 3,
  uid: 'rkU8O4xFJCel1VmaO0w21OihIoj1',
  firstName: 'Nicolas',
  lastName: 'Okroglic',
  email: 'nicolasokroglic@gmail.com',
  alias: '100002',
};

const customer = {
  id: 8,
  uid: 'aiJtzt8E1JVufRPVKddIPPDQkhK2',
  firstName: 'Tomas',
  lastName: 'Nocetti Customer',
  email: 'nocetti.tomas+customer@gmail.com',
  alias: '100007',
  customer: {
    id: 1,
    userId: 8,
    storeId: 1,
    cardId: '101',
  },
};

const employee = {
  id: 4,
  uid: '2jzNEnSunLhrHHGcKtF2qGUyuiv1',
  firstName: 'Pizzeria',
  lastName: 'Gomez',
  email: 'nocetti.tomas+pizzeriagomez@gmail.com',
  alias: '100003',
  pushToken: 'ExponentPushToken[y9wqbAIxZwP9RjZdqm7OHw]',
  employee: {
    id: 1,
    userId: 4,
    storeId: 1,
    permissionId: 1,
  },
};

const demoStoreId = 1;
const modifiableStoreId = 7;

const store = {
  id: demoStoreId,
  name: 'Loyals Club',
  alias: 'loyals.club',
  validCoupon: {
    id: 1,
  },
  storeRewards: [
    {
      id: 1,
      storeLevelId: 1,
    },
  ],
  countryCode: COUNTRY_CODES.ARGENTINA,
};

const invalidStore = {
  id: 3,
  name: 'No Users Store',
  alias: 'no.users.store',
};

const coupon = {
  couponCode: 'avaso7',
  store: { id: 1 },
  points: 200,
  customer: { id: 1 },
};

const inactiveCoupon = {
  couponCode: 'pamon9',
  store: { id: 1 },
  points: 500,
};

const adminToken = () => getUserToken('graziano.ramiro@gmail.com', 'Qwe12345');

const employeeToken = () => getUserToken('nocetti.tomas+pizzeriagomez@gmail.com', 'Qwe12345');

const customerToken = () => getUserToken('nocetti.tomas+customer@gmail.com', 'Qwe12345');

const notCustomerToken = () => getUserToken('nicolasokroglic@gmail.com', 'Qwe12345');

const getUserToken = async (email, password) => {
  try {
    await firebase.auth().signInWithEmailAndPassword(email, password);
    const authToken = await firebase.auth().currentUser.getIdToken();
    return authToken;
  } catch (error) {
    console.log(error);
  }
};

const validRequest = (err, res) => {
  expect(err).to.be.null;
  if (res.body.errors) console.log(res.body.errors);
  expect(res.body.errors).to.be.undefined;
  expect(res).to.have.status(200);
};

const invalidRequest = (err, res, errObj) => {
  expect(res.body.errors).not.to.be.undefined;
  expect(res).to.have.status(200);
  if (!errObj) return;

  const error = res.body.errors[0];
  expect(error).to.have.property('message').equals(errObj.msg);
  expect(error).to.have.property('extensions').to.have.property('code').equals(errObj.code);
};

const testPushToken = 'ExponentPushToken[y9wqbAIxZwP9RjZdqm7OHw]';

module.exports = {
  validRequest,
  firebase,
  apiUrl,
  employeeToken,
  customerToken,
  notCustomerToken,
  getUserToken,
  employee,
  notCustomer,
  customer,
  store,
  invalidStore,
  invalidRequest,
  coupon,
  inactiveCoupon,
  adminToken,
  demoStoreId,
  modifiableStoreId,
  testPushToken,
};
