const { expect } = require('chai');
const proxyquire = require('proxyquire');
const SequelizeMock = require('sequelize-mock');
const sinon = require('sinon');

const { NotificationManager } = require('@models');
const { STORE_NOTIFICATIONS, STORE_NOTIFICATIONS_TYPES } = require('@modules/store/constants/store.constants');

const { employee, testPushToken } = require('../utils/test.utils');

describe('Notification Manager Test Cases', () => {
  let sequelizeMock, db, notificationManagerMock;

  const sendOne = sinon.spy(() => 'mock');

  before(() => {
    sequelizeMock = new SequelizeMock();

    db = {
      EmployeeProfile: sequelizeMock.define('EmployeeProfile', {}),
    };

    notificationManagerMock = proxyquire('../../src/modules/core/notification-manager.model', {
      '../../../database/models': db,
      '../../services/push-notifications/push-notifications.service': {
        sendOne: sendOne,
      },
    });
  });

  describe('Static function sendPushToStore should', () => {
    beforeEach(() => {
      sendOne.resetHistory();
    });

    it('exist', () => {
      expect(NotificationManager.sendPushToStore).to.not.be.undefined;
    });

    it('return false if the store has not push token associated', async () => {
      db.EmployeeProfile.$queueResult({ User: { pushToken: null } });

      const storeId = 7;
      const data = {};

      const response = await notificationManagerMock.sendPushToStore({
        type: STORE_NOTIFICATIONS_TYPES.NEW_CUSTOMER,
        storeId,
        data,
      });

      expect(response).to.be.false;
    });

    it('send the notification NEW_CUSTOMER', async () => {
      const storeName = 'Pizzería Gómez';
      const storeId = 7;
      const customerName = 'Juan Gomez';

      db.EmployeeProfile.$queueResult({
        Store: {
          id: storeId,
          name: storeName,
        },
        User: { pushToken: testPushToken },
      });

      const response = await notificationManagerMock.sendPushToStore({
        type: STORE_NOTIFICATIONS_TYPES.NEW_CUSTOMER,
        storeId,
        data: {
          customerName,
        },
      });

      expect(Boolean(response)).to.be.true;
      expect(sendOne.called).to.be.true;
      expect(
        sendOne.calledWith({
          pushToken: testPushToken,
          title: STORE_NOTIFICATIONS.NEW_CUSTOMER.title({ storeName }),
          body: STORE_NOTIFICATIONS.NEW_CUSTOMER.body({ customerName }),
        }),
      ).to.be.true;
    });

    it('send the notification CUSTOMER_ACTIVATED_POINTS', async () => {
      const storeName = 'Pizzería Gómez';
      const storeId = 7;
      const customerName = 'Juan Gomez';
      const pointsAmount = 200;

      db.EmployeeProfile.$queueResult({
        Store: {
          id: storeId,
          name: storeName,
        },
        User: { pushToken: testPushToken },
      });

      const response = await notificationManagerMock.sendPushToStore({
        type: STORE_NOTIFICATIONS_TYPES.CUSTOMER_ACTIVATED_POINTS,
        storeId,
        data: {
          customerName,
          pointsAmount,
        },
      });

      expect(Boolean(response)).to.be.true;
      expect(sendOne.called).to.be.true;
      expect(
        sendOne.calledWith({
          pushToken: employee.pushToken,
          title: STORE_NOTIFICATIONS.CUSTOMER_ACTIVATED_POINTS.title({ storeName }),
          body: STORE_NOTIFICATIONS.CUSTOMER_ACTIVATED_POINTS.body({ customerName, pointsAmount }),
        }),
      ).to.be.true;
    });
  });
});
