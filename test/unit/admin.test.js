const { expect } = require('chai');

const db = require('@db');
const { Admin } = require('@models');

after(function () {
  return db.sequelize
    .authenticate()
    .then(() => db.sequelize.close())
    .catch(() => {});
});

describe('Admin Unit Tests', () => {
  describe('Module Admin should', () => {
    it('exist', () => {
      expect(Admin).to.not.be.undefined;
    });

    it('have exported members', () => {
      expect(Object.keys(Admin).length).to.be.greaterThan(0);
    });

    describe('Function getCoupons should', () => {
      it('exist', () => {
        expect(Admin.getCoupons).to.not.be.undefined;
      });

      it('return with no store id param an array of coupons including the store object', async () => {
        const coupons = await Admin.getCoupons();
        expect(coupons).to.be.an('array');
        for (let coupon of coupons) {
          expect(coupon.Store).to.not.be.undefined;
        }
      });

      it('return with store id param an array of coupons and they should all belong to the same store including the store object', async () => {
        const storeId = 1;
        const coupons = await Admin.getCoupons({}, storeId);
        expect(coupons).to.be.an('array');
        for (let coupon of coupons) {
          expect(coupon.Store).to.not.be.undefined;
        }
        expect(coupons.every(({ Store }) => Store.id === storeId)).to.be.true;
      });
    });

    describe('Function searchStore should', () => {
      it('exist', () => {
        expect(Admin.searchStore).to.not.be.undefined;
      });

      it('return an empty array with empty data', async () => {
        const stores = await Admin.searchStore({});
        expect(stores).to.be.an('array');
        expect(stores.length).to.be.equal(0);
      });

      it('return an array with store database instances', async () => {
        const stores = await Admin.searchStore({ data: 'bien' });

        expect(stores).to.be.an('array');

        for (let store of stores) {
          expect(store.dataValues).to.have.all.keys([
            'id',
            'name',
            'alias',
            'profileImage',
            'category',
            'description',
            'socialMedia',
            'createdAt',
            'updatedAt',
          ]);
        }
      });
    });
    describe('Function storeStatistics should', () => {
      it('exist', () => {
        expect(Admin.storeStatistics).to.not.be.undefined;
      });
      it('return an oject with properties', async () => {
        const statistics = await Admin.storeStatistics(1);
        expect(statistics).to.haveOwnProperty('customersOverTime');
        expect(statistics).to.haveOwnProperty('pointTransactionsOverTime');
        expect(statistics).to.haveOwnProperty('rewardTransactionsOverTime');
      });
      it('each property to have given format, {date: .., count: ..}', async () => {
        const statistics = await Admin.storeStatistics(1);
        for (let el of statistics.customersOverTime) {
          expect(el).to.haveOwnProperty('date');
          expect(el).to.haveOwnProperty('count');
        }
        for (let el of statistics.pointTransactionsOverTime) {
          expect(el).to.haveOwnProperty('date');
          expect(el).to.haveOwnProperty('count');
        }
        for (let el of statistics.rewardTransactionsOverTime) {
          expect(el).to.haveOwnProperty('date');
          expect(el).to.haveOwnProperty('count');
        }
      });
    });
  });
});
