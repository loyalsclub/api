const { ApolloError } = require('apollo-server');
const { expect } = require('chai');

const db = require('@db');
const { Coupon } = require('@models');
const { COUPON_STATUS } = require('@modules/core/constants/core.constants');
const { API_CUSTOMER_ERRORS } = require('@modules/core/constants/error-codes.constants');

const { coupon: utilsCoupon } = require('../utils/test.utils');

const couponData = {
  couponCode: 'asdf123',
  storeId: utilsCoupon.store.id,
  points: utilsCoupon.points,
};

after(function () {
  return db.sequelize
    .authenticate()
    .then(() => db.sequelize.close())
    .catch(() => {});
});

describe('Coupon Unit Tests', () => {
  describe('Class Coupon should', () => {
    it('exist', () => {
      expect(Coupon.prototype).to.not.be.undefined;
    });

    describe('Static function create should', () => {
      let coupon;

      it('exist', () => {
        expect(Coupon.create).to.not.be.undefined;
      });

      it('return a newly created coupon', async function () {
        coupon = await Coupon.create(couponData);

        expect(coupon).to.not.be.undefined;

        this.coupon = coupon;
      });

      it('and should be an instance of Coupon class', () => {
        expect(coupon instanceof Coupon).to.be.true;
      });

      it('and the instance should have all the information sent', () => {
        expect(coupon.dbCoupon).to.not.be.undefined;
        expect(coupon.dbCoupon).to.include(couponData);
        expect(coupon.dbCoupon).to.include(couponData);
      });

      it('and the coupon should created with ACTIVE status', () => {
        expect(coupon.dbCoupon.status).to.be.equals(COUPON_STATUS.ACTIVE);
      });

      it('and should have loaded the UsageFrequency instance', () => {
        expect(coupon.dbCoupon.get('UsageFrequency')).to.not.be.undefined;
      });

      it('return an error if the coupon is already created of that store', async () => {
        try {
          coupon = await Coupon.create(couponData);
          expect(coupon).to.be.undefined;
        } catch (err) {
          const { msg, code } = API_CUSTOMER_ERRORS['api/coupon-already-exists'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });

      after(async function () {
        await db.sequelize.query(`DELETE from UsageFrequencies where id=${this.coupon.dbCoupon.usageFrequencyId}`);
        await db.sequelize.query(
          `DELETE from Coupons where couponCode='${couponData.couponCode}' and storeId=${couponData.storeId}`,
        );
      });
    });

    describe('Static function init should', () => {
      let coupon;

      it('exist', () => {
        expect(Coupon.init).to.not.be.undefined;
      });

      before(async function () {
        this.coupon = await Coupon.create(couponData);
      });

      it('return a coupon by coupon and store id', async () => {
        coupon = await Coupon.init(couponData.couponCode, couponData.storeId);
        expect(coupon).to.not.be.undefined;
      });

      it('and should be an instance of Coupon class', () => {
        expect(coupon instanceof Coupon).to.be.true;
      });

      it('and should have loaded the UsageFrequency instance', () => {
        expect(coupon.dbCoupon.UsageFrequency).to.not.be.undefined;
      });

      it('return an error with coupon or store invalid id', async () => {
        try {
          coupon = await Coupon.init('sarasa', couponData.storeId);
          expect(coupon.dbCoupon).to.not.be.null;
        } catch (err) {
          const { msg, code } = API_CUSTOMER_ERRORS['api/coupon-not-found'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });

      after(async function () {
        await db.sequelize.query(`DELETE from UsageFrequencies where id=${this.coupon.dbCoupon.usageFrequencyId}`);
        await db.sequelize.query(
          `DELETE from Coupons where couponCode='${couponData.couponCode}' and storeId=${couponData.storeId}`,
        );
      });
    });

    describe('Static function initById should', () => {
      let coupon;
      let couponCode;

      it('exist', () => {
        expect(Coupon.initById).to.not.be.undefined;
      });

      before(async function () {
        const newCoupon = await Coupon.create(couponData);
        couponCode = newCoupon.dbCoupon.id;
        this.coupon = newCoupon;
      });

      it('return a coupon by id', async () => {
        coupon = await Coupon.initById(couponCode);
        expect(coupon).to.not.be.undefined;
      });

      it('and should be an instance of Coupon class', () => {
        expect(coupon instanceof Coupon).to.be.true;
      });

      it('return an error with invalid id', async () => {
        try {
          coupon = await Coupon.initById(-1);
          expect(coupon.dbCoupon).to.not.be.null;
        } catch (err) {
          const { msg, code } = API_CUSTOMER_ERRORS['api/coupon-not-found'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });

      after(async function () {
        await db.sequelize.query(`DELETE from UsageFrequencies where id=${this.coupon.dbCoupon.usageFrequencyId}`);
        await db.sequelize.query(
          `DELETE from Coupons where couponCode='${couponData.couponCode}' and storeId=${couponData.storeId}`,
        );
      });
    });

    describe('Static function validateCouponCode should', () => {
      it('exist', () => {
        expect(Coupon.validateCouponCode).to.not.be.undefined;
      });

      it('return true if the coupon has not been created yet', async () => {
        const isValid = await Coupon.validateCouponCode(couponData);

        expect(isValid).to.be.a('boolean');
        expect(isValid).to.be.equal(true);
      });

      it('return false if the coupon has already been created', async function () {
        this.coupon = await Coupon.create(couponData);

        const isValid = await Coupon.validateCouponCode(couponData);

        expect(isValid).to.be.a('boolean');
        expect(isValid).to.be.equal(false);
      });

      it('return false if no coupon code is provided', async () => {
        const isValid = await Coupon.validateCouponCode({ ...couponData, couponCode: '' });

        expect(isValid).to.be.a('boolean');
        expect(isValid).to.be.equal(false);
      });

      it('return false if no store id is provided', async () => {
        const isValid = await Coupon.validateCouponCode({ ...couponData, storeId: '' });

        expect(isValid).to.be.a('boolean');
        expect(isValid).to.be.equal(false);
      });

      after(async function () {
        await db.sequelize.query(`DELETE from UsageFrequencies where id=${this.coupon.dbCoupon.usageFrequencyId}`);
        await db.sequelize.query(
          `DELETE from Coupons where couponCode='${couponData.couponCode}' and storeId=${couponData.storeId}`,
        );
      });
    });

    describe('Class function modifyStatus should', () => {
      let coupon;

      before(async function () {
        coupon = await Coupon.create(couponData);
        this.coupon = coupon;
      });

      it('exist', async () => {
        expect(coupon.modifyStatus).to.not.be.undefined;
      });

      it('update the status in the database and return the instance', async () => {
        expect(coupon.dbCoupon.status).to.be.equal(COUPON_STATUS.ACTIVE);

        coupon = await coupon.modifyStatus(COUPON_STATUS.PAUSED);

        expect(coupon).to.not.be.undefined;
        expect(coupon.status).to.be.equal(COUPON_STATUS.PAUSED);
      });

      after(async function () {
        await db.sequelize.query(`DELETE from UsageFrequencies where id=${this.coupon.dbCoupon.usageFrequencyId}`);
        await db.sequelize.query(
          `DELETE from Coupons where couponCode='${couponData.couponCode}' and storeId=${couponData.storeId}`,
        );
      });
    });

    describe('Class function raw should', () => {
      let coupon;

      before(async function () {
        coupon = await Coupon.create(couponData);

        this.coupon = coupon;
      });

      after(async function () {
        await db.sequelize.query(`DELETE from UsageFrequencies where id=${this.coupon.dbCoupon.usageFrequencyId}`);
        await db.sequelize.query(
          `DELETE from Coupons where id=${this.coupon.dbCoupon.id} and storeId=${couponData.storeId}`,
        );
      });

      it('exist', () => {
        expect(coupon.raw).to.be.exist;
        expect(coupon.raw).to.be.a('function');
      });

      it('return the raw model of the coupon', async () => {
        expect(coupon).to.have.property('dbCoupon');
        const rawCoupon = await coupon.raw();
        expect(rawCoupon).to.not.have.property('dbCoupon');
      });
    });
  });
});
