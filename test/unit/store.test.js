const { ApolloError } = require('apollo-server');
const { expect } = require('chai');

const db = require('@db');
const { Store } = require('@models');
const { API_STORE_ERRORS } = require('@modules/core/constants/error-codes.constants');
const { COUNTRY_CODES } = require('@modules/store/constants/store.constants');

const { store: utilsStore } = require('../utils/test.utils');

after(function () {
  return db.sequelize
    .authenticate()
    .then(() => db.sequelize.close())
    .catch(() => {});
});

describe('Store Unit Tests', () => {
  describe('Class Store should', () => {
    it('exist', () => {
      expect(Store.prototype).to.not.be.undefined;
    });

    describe('Static function init should', () => {
      let store;

      it('exist', () => {
        expect(Store.init).to.not.be.undefined;
      });

      it('return a store by its id', async () => {
        store = await Store.init(utilsStore.id);
        expect(store).to.not.be.undefined;
      });

      it('and should be an instance of Store class', () => {
        expect(store instanceof Store).to.be.true;
      });

      it('return an error with invalid id', async () => {
        try {
          store = await Store.init('sarasa');
          expect(store.dbStore).to.not.be.null;
        } catch (err) {
          const { msg, code } = API_STORE_ERRORS['api/store-not-found'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });
    });

    describe('Static function generateAlias should', () => {
      it('exist', () => {
        expect(Store.generateAlias).to.not.be.undefined;
      });

      it('remove trailing and leading white spaces', async () => {
        const escaped = await Store.generateAlias('   john    ');
        expect(escaped).to.be.equal('john');
      });

      it('escape the special chars of a given name minus letters, numbers, -, _ and white spaces', async () => {
        const escaped = await Store.generateAlias('~`!@#$%^&*()+={}[]|\\;:\'",.<>/?-_a1');
        expect(escaped).to.be.equal('-_a1');
      });

      it('remove consecutive duplicates of -, _ and white spaces (that are replaced by dots)', async () => {
        const escaped = await Store.generateAlias('my--------name________is     john');
        expect(escaped).to.be.equal('my-name_is.john');
      });

      it('remove surrounding white spaces around a hyphen', async () => {
        const escaped = await Store.generateAlias('john - b');
        expect(escaped).to.be.equal('john-b');
      });

      it('lower case all the letters', async () => {
        const escaped = await Store.generateAlias('AaBbCc');
        expect(escaped).to.be.equal('aabbcc');
      });

      it('escape accents', async () => {
        const escaped = await Store.generateAlias('AáBbéc');
        expect(escaped).to.be.equal('aabbec');
      });

      it('return a single string separating the words with a .', async () => {
        const escaped = await Store.generateAlias('My @Name !Is^@!*$@)$(#) John+++#!@)#@!');
        expect(escaped).to.be.equal('my.name.is.john');
      });

      it('return a valid alias if duplicated', async () => {
        const escaped = await Store.generateAlias('pizzeria gomez');
        expect(escaped).to.be.equal('pizzeria.gomez1');
      });
    });

    describe('Class function convertPriceToPoints should', () => {
      let store;

      before(async function () {
        store = await Store.init(utilsStore.id);
        this.store = store;
      });

      it('exist', () => {
        expect(store.convertPriceToPoints).to.not.be.undefined;
      });

      it('convert the given price into points based on the country points value', async () => {
        expect(store.dbStore.countryCode).to.be.equal(COUNTRY_CODES.ARGENTINA);
        expect(store.convertPriceToPoints(200)).to.be.equal(10);
        expect(store.convertPriceToPoints(400)).to.be.equal(20);
      });
    });
  });
});
