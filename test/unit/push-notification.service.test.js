const { expect } = require('chai');

const ExpoPushNotificationsService = require('@services/push-notifications/expo/expo-push-notifications.service');

const { testPushToken } = require('../utils/test.utils');

describe.skip('Push Notification Service', () => {
  let expoPushNotificationsService = new ExpoPushNotificationsService();

  describe('Send push notification with', () => {
    it('Expo Provider', async () => {
      const messages = [
        {
          pushToken: testPushToken,
          title: 'Hello',
          body: 'World!',
          data: { hi: 'there' },
        },
      ];

      const notifications = await expoPushNotificationsService.parseNotifications(messages);

      const [{ pushToken, ...message }] = notifications;

      expect(notifications[0]).to.deep.equal({
        to: pushToken,
        sound: 'default',
        'content-available': 1,
        ...message,
      });

      const [tickets, errors] = await expoPushNotificationsService.sendNotifications(notifications);

      expect(errors.length).to.be.eq(0);

      const responses = await expoPushNotificationsService.checkNotificationsReceipt(tickets);

      for (const response of responses) {
        expect(response).to.be.true;
      }
    });
  });
});
