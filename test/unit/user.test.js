const { expect, AssertionError } = require('chai');
const faker = require('faker');
const proxyquire = require('proxyquire');
const SequelizeMock = require('sequelize-mock');
const sinon = require('sinon');

const { User } = require('@models');

describe('User Unit Test', () => {
  let sequelizeMock, db, UserModuleMock;

  const createUser = data =>
    Promise.resolve({
      ...data,
      uid: faker.datatype.uuid(),
    });

  const auth = {
    createUser,
    deleteUser: () => Promise.resolve(1),
    createCustomToken: () => Promise.resolve(''),
  };

  const destroy = sinon.spy(() => 'deleted');

  before(() => {
    sequelizeMock = new SequelizeMock();

    db = {
      User: sequelizeMock.define(
        'User',
        {
          firstName: '',
          lastName: '',
          email: '',
          uid: '',
          role: 'user',
          birthDate: null,
          alias: '',
          pushToken: null,
        },
        {
          instanceMethods: { destroy },
        },
      ),
    };

    UserModuleMock = proxyquire('../../src/modules/user/user.model', {
      '../../../database/models': db,
      '../../services/authentication/authentication.service': {
        auth: () => auth,
      },
    });
  });

  describe('Class User should', () => {
    it('exist', () => {
      expect(User.prototype).to.not.be.undefined;
    });
  });

  describe('Static function createUser should', () => {
    it('exist', () => {
      expect(User.createUser).to.not.be.undefined;
    });

    it('Create a new user in the database and firebase with the given data', async () => {
      const userObj = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        birthDate: new Date().toISOString(),
      };

      const createdUser = await UserModuleMock.createUser(userObj);

      expect(createdUser).to.not.be.undefined;
      expect(createdUser.dbUser).to.not.be.undefined;
      expect(createdUser.fbUser).to.not.be.undefined;
      expect(createdUser.dbUser.role).to.be.eq('user');
      expect(createdUser.fbUser.uid).to.not.be.undefined;
      expect(createdUser.dbUser.uid).to.not.be.undefined;
      expect(createdUser.dbUser.uid).to.be.eq(createdUser.fbUser.uid);
      expect(createdUser.fbUser.displayName).to.be.eq(`${userObj.firstName} ${userObj.lastName}`);
    });

    it('Destroy the created user if an error occurred', async () => {
      destroy.resetHistory();

      const originalFunction = auth.createUser;

      auth.createUser = () => Promise.reject('error');

      const userObj = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        birthDate: new Date().toISOString(),
      };

      try {
        const user = await UserModuleMock.createUser(userObj);
        expect(user).to.be.undefined;
      } catch (err) {
        expect(err).to.not.be.instanceof(AssertionError);
        expect(destroy.called).to.be.true;
        expect(err).to.not.be.undefined;
      }

      auth.createUser = originalFunction;

      destroy.resetHistory();
    });
  });

  describe('Class function updateUser should', () => {
    let user;

    before(async () => {
      const userObj = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        birthDate: new Date().toISOString(),
      };

      user = await UserModuleMock.createUser(userObj);
    });

    it('exist', () => {
      expect(user.updateUser).to.not.be.undefined;
    });

    it('Update user data in the database', async () => {
      const data = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        pushToken: faker.git.commitSha(),
      };

      const updatedUser = await user.updateUser(data);

      expect(updatedUser).to.not.be.undefined;
      expect(updatedUser.firstName).to.be.eq(data.firstName);
      expect(updatedUser.lastName).to.be.eq(data.lastName);
      expect(updatedUser.pushToken).to.be.eq(data.pushToken);
    });
  });
});
