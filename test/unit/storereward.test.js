const { ApolloError } = require('apollo-server');
const { expect } = require('chai');

const db = require('@db');
const { StoreReward } = require('@models');
const { USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');
const { API_STORE_REWARD_ERRORS } = require('@modules/core/constants/error-codes.constants');

after(function () {
  return db.sequelize
    .authenticate()
    .then(() => db.sequelize.close())
    .catch(() => {});
});

const storeRewardData = {
  storeLevelId: 16,
  title: 'Testing Store Reward',
  usageFrequencyType: USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS,
  usageFrequencyValue: '2',
};

describe('StoreReward Unit Tests', () => {
  describe('Class StoreReward should', () => {
    it('exist', () => {
      expect(StoreReward).to.not.be.undefined;
    });

    describe('Static function create should', () => {
      let storeReward;

      after(async function () {
        await db.sequelize.query(`DELETE from UsageFrequencies where id=${storeReward.dbStoreReward.usageFrequencyId}`);
        await db.sequelize.query(`DELETE from StoreRewards where id=${storeReward.dbStoreReward.id}`);
      });

      it('exist', () => {
        expect(StoreReward.create).to.not.be.undefined;
      });

      it('create a new store reward with usage frequency', async () => {
        storeReward = await StoreReward.create(storeRewardData);

        expect(storeReward).to.not.be.undefined;
        expect(storeReward.dbStoreReward).to.not.be.undefined;
        expect(storeReward.dbStoreReward.title).to.equals('Testing Store Reward');

        const usageFrequency = storeReward.dbStoreReward.get('UsageFrequency');
        expect(usageFrequency).to.not.be.null;
        expect(usageFrequency.type).to.equals(USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS);
        expect(usageFrequency.value).to.equals('2');
      });

      it('return an error with non existing store level id', async () => {
        try {
          storeReward = await StoreReward.create({ ...storeRewardData, storeLevelId: 0 });
          expect(storeReward).to.be.undefined;
        } catch (err) {
          const { msg, code } = API_STORE_REWARD_ERRORS['api/store-level-not-found'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });

      it('return an error with store level id that does not belong to storeId', async () => {
        try {
          storeReward = await StoreReward.create({ ...storeRewardData, storeLevelId: 4 }, 1);
          expect(storeReward).to.be.undefined;
        } catch (err) {
          const { msg, code } = API_STORE_REWARD_ERRORS['api/store-level-invalid'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });
    });

    describe('Static function initById should', () => {
      let storeReward;

      it('exist', () => {
        expect(StoreReward.initById).to.not.be.undefined;
      });

      before(async function () {
        this.storeReward = await StoreReward.create(storeRewardData);
      });

      it('return a storeReward by storeReward and store id', async function () {
        storeReward = await StoreReward.initById(this.storeReward.dbStoreReward.id);
        expect(storeReward).to.not.be.undefined;
        expect(storeReward.dbStoreReward).to.not.be.undefined;
      });

      it('and should be an instance of Coupon class', () => {
        expect(storeReward instanceof StoreReward).to.be.true;
      });

      it('and should have loaded the UsageFrequency instance', () => {
        expect(storeReward.dbStoreReward.UsageFrequency).to.not.be.undefined;
      });

      it('return an error with coupon or store invalid id', async () => {
        try {
          storeReward = await StoreReward.initById('sarasa');
          expect(storeReward.dbStoreReward).to.not.be.null;
        } catch (err) {
          const { msg, code } = API_STORE_REWARD_ERRORS['api/store-reward-not-found'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });

      after(async function () {
        await db.sequelize.query(
          `DELETE from UsageFrequencies where id=${this.storeReward.dbStoreReward.usageFrequencyId}`,
        );
        await db.sequelize.query(`DELETE from StoreRewards where id='${this.storeReward.dbStoreReward.id}'`);
      });
    });

    describe('Function update should', () => {
      let storeReward;

      before(async function () {
        this.storeReward = await StoreReward.create({
          storeLevelId: 16,
          title: 'Testing Store Reward',
          usageFrequencyType: USAGE_FREQUENCIES.ONE_TIME,
        });
      });

      after(async function () {
        await db.sequelize.query(
          `DELETE from UsageFrequencies where id=${this.storeReward.dbStoreReward.usageFrequencyId}`,
        );
        await db.sequelize.query(`DELETE from StoreRewards where id=${this.storeReward.dbStoreReward.id}`);
      });

      it('exist', function () {
        expect(this.storeReward.update).to.not.be.undefined;
      });

      it('edit a store reward and its usage frequency', async function () {
        storeReward = await this.storeReward.update({
          storeLevelId: 16,
          title: 'Testing Store Reward modification',
          usageFrequencyType: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
          usageFrequencyValue: '2,4',
        });

        expect(storeReward).to.not.be.undefined;
        expect(storeReward.title).to.equals('Testing Store Reward modification');

        expect(storeReward.storeLevelId).to.equals(16);

        const usageFrequency = storeReward.UsageFrequency;
        expect(usageFrequency).to.not.be.null;
        expect(usageFrequency.type).to.equals(USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS);
        expect(usageFrequency.value).to.equals('2,4');
      });

      it('edit a store reward and its usage frequency', async function () {
        storeReward = await this.storeReward.update({
          storeLevelId: 16,
          title: 'Testing Store Reward modification',
          usageFrequencyType: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
          usageFrequencyValue: '2,4',
        });

        expect(storeReward).to.not.be.undefined;
        expect(storeReward.title).to.equals('Testing Store Reward modification');

        expect(storeReward.storeLevelId).to.equals(16);

        const usageFrequency = storeReward.UsageFrequency;
        expect(usageFrequency).to.not.be.null;
        expect(usageFrequency.type).to.equals(USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS);
        expect(usageFrequency.value).to.equals('2,4');
      });

      it('return an error with non existing store level id', async function () {
        try {
          storeReward = await this.storeReward.update({ ...storeRewardData, storeLevelId: 0 });
          expect(storeReward).to.be.undefined;
        } catch (err) {
          const { msg, code } = API_STORE_REWARD_ERRORS['api/store-level-not-found'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });

      it('return an error with store level id that does not belong to storeId', async function () {
        try {
          storeReward = await this.storeReward.update({ ...storeRewardData, storeLevelId: 4 }, 1);
          expect(storeReward).to.be.undefined;
        } catch (err) {
          const { msg, code } = API_STORE_REWARD_ERRORS['api/store-level-invalid'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });
    });

    describe('Function remove should', () => {
      let storeReward;

      before(async function () {
        storeReward = await StoreReward.create({
          storeLevelId: 16,
          title: 'Testing Store Reward',
          usageFrequencyType: USAGE_FREQUENCIES.ONE_TIME,
        });
      });

      it('exist', () => {
        expect(StoreReward.remove).to.not.be.undefined;
      });

      it('throw an error if the store reward does not belong to given store', async () => {
        try {
          await StoreReward.remove(storeReward.dbStoreReward.id, 1);
          expect(true).to.be.false;
        } catch (err) {
          const { msg, code } = API_STORE_REWARD_ERRORS['api/store-level-invalid'];

          expect(err instanceof ApolloError).to.be.true;
          expect(err.message).to.be.equal(msg);
          expect(err.extensions.code).to.be.equal(code);
        }
      });

      it('remove a store reward and its associated usage frequency', async () => {
        await StoreReward.remove(storeReward.dbStoreReward.id);

        const usageFrequency = await db.UsageFrequency.findByPk(storeReward.dbStoreReward.usageFrequencyId);

        expect(usageFrequency).to.be.null;

        storeReward = await db.StoreReward.findByPk(storeReward.dbStoreReward.id);

        expect(storeReward).to.be.null;
      });
    });

    describe('Class function raw should', () => {
      let storeReward;

      before(async function () {
        storeReward = await StoreReward.create(storeRewardData);

        this.storeReward = storeReward;
      });

      after(async function () {
        await db.sequelize.query(
          `DELETE from UsageFrequencies where id=${this.storeReward.dbStoreReward.usageFrequencyId}`,
        );
        await db.sequelize.query(`DELETE from StoreRewards where id=${this.storeReward.dbStoreReward.id}`);
      });

      it('exist', () => {
        expect(storeReward.raw).to.be.exist;
        expect(storeReward.raw).to.be.a('function');
      });

      it('return the raw model of the storeReward', async () => {
        expect(storeReward).to.have.property('dbStoreReward');
        const rawStoreReward = await storeReward.raw();
        expect(rawStoreReward).to.not.have.property('dbStoreReward');
      });
    });
  });
});
