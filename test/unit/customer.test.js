const { ApolloError } = require('apollo-server');
const { expect } = require('chai');
const faker = require('faker');
const moment = require('moment');
const { QueryTypes } = require('sequelize');

const db = require('@db');
const { Coupon, Customer, User } = require('@models');
const {
  CUSTOMER_STATUS,
  COUPON_STATUS,
  FOREVER_FREQUENCY_MIN_VALID_HOURS,
} = require('@modules/core/constants/core.constants');
const { API_CUSTOMER_ERRORS } = require('@modules/core/constants/error-codes.constants');

const { customer, coupon: utilsCoupon } = require('../utils/test.utils');

const couponData = {
  couponCode: 'asdf123',
  storeId: utilsCoupon.store.id,
  points: utilsCoupon.points,
};

after(function () {
  return db.sequelize
    .authenticate()
    .then(() => db.sequelize.close())
    .catch(() => {});
});

describe('Customer Unit Tests', () => {
  let createdCustomer;

  after(function () {
    return db.sequelize.query(`DELETE from CustomerProfiles where id=${createdCustomer.dbCustomer.id}`).then(() => {
      return db.sequelize.query(`DELETE from Users where id=${createdCustomer.dbCustomer.userId}`);
    });
  });

  describe('Class Customer should', () => {
    it('exist', () => {
      expect(Customer.prototype).to.not.be.undefined;
    });
  });

  describe('Static function initById should', () => {
    it('exist', () => {
      expect(Customer.initById).to.be.exist;
      expect(Customer.initById).to.be.a('function');
    });

    it('return a customer with existing user and customer id', async () => {
      const customerInstance = await Customer.initIfExists(customer.id, customer.customer.id);
      expect(customerInstance instanceof Customer).to.be.true;
    });

    it('return an error with non-existing customer id', async () => {
      try {
        await Customer.initById(100000, customer.customer.storeId);
      } catch (err) {
        const { msg, code } = API_CUSTOMER_ERRORS['api/user-is-not-a-customer'];
        expect(err instanceof ApolloError).to.be.true;
        expect(err.message).to.be.equal(msg);
        expect(err.extensions.code).to.be.equal(code);
      }
    });
  });

  describe('Static function initByCardId should', () => {
    it('exist', () => {
      expect(Customer.initByCardId).to.be.exist;
      expect(Customer.initByCardId).to.be.a('function');
    });

    it('return a customer with existing user and customer id', async () => {
      const customerInstance = await Customer.initByCardId(customer.customer.cardId);
      expect(customerInstance instanceof Customer).to.be.true;
    });
  });

  describe('Static function initIfExists should', () => {
    it('exist', () => {
      expect(Customer.initIfExists).to.be.exist;
      expect(Customer.initIfExists).to.be.a('function');
    });

    it('return a customer with existing user and customer id', async () => {
      const customerInstance = await Customer.initIfExists(customer.id, customer.customer.storeId);
      expect(customerInstance instanceof Customer).to.be.true;
    });

    it('return null with non-existing customer id', async () => {
      const customerInstance = await Customer.initIfExists(1001, customer.customer.storeId);
      expect(customerInstance).to.be.null;
    });
  });

  describe('Static function initOrCreate should', () => {
    let customerInstance;

    after(function () {
      return db.sequelize.query(`DELETE from CustomerProfiles where id=${customerInstance.dbCustomer.id}`).then(() => {
        return db.sequelize.query(`DELETE from Users where id=${customerInstance.dbCustomer.userId}`);
      });
    });

    it('exist', () => {
      expect(Customer.initOrCreate).to.be.exist;
      expect(Customer.initOrCreate).to.be.a('function');
    });

    it('return a customer with existing user and customer id', async () => {
      const { userId, storeId } = await db.sequelize.query('SELECT userId, storeId FROM `CustomerProfiles` limit 1', {
        type: QueryTypes.SELECT,
        plain: true,
      });
      const customerInstance = await Customer.initOrCreate(userId, storeId);
      expect(customerInstance instanceof Customer).to.be.true;
    });

    it('return a new customer with non-existing customer id', async () => {
      const userObj = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        birthDate: new Date().toISOString(),
        nickname: Math.random().toString(36).substring(7),
      };
      const userInstance = await User.createPendingUser(userObj);
      customerInstance = await Customer.initOrCreate(userInstance.dbUser.id, customer.customer.storeId);
      expect(customerInstance instanceof Customer).to.be.true;
    });
  });

  describe('Static function create should', () => {
    it('exist', () => {
      expect(Customer.create).to.be.exist;
      expect(Customer.create).to.be.a('function');
    });

    it('return a new customer', async () => {
      const userObj = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        birthDate: new Date().toISOString(),
        nickname: Math.random().toString(36).substring(7),
      };
      const userInstance = await User.createPendingUser(userObj);
      const customerInstance = await Customer.create({
        userId: userInstance.dbUser.id,
        storeId: customer.customer.storeId,
        cardId: customer.customer.cardId,
      });
      expect(customerInstance instanceof Customer).to.be.true;
      expect(customerInstance.dbCustomer.cardId).to.not.be.undefined;
      createdCustomer = customerInstance;
    });

    it('init the customer in the corresponding initialLevel', async () => {
      const userObj = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        birthDate: new Date().toISOString(),
        nickname: Math.random().toString(36).substring(7),
      };
      const userInstance = await User.createPendingUser(userObj);
      const customerInstance = await Customer.create({
        userId: userInstance.dbUser.id,
        storeId: customer.customer.storeId,
        initialLevel: 2,
      });

      expect(customerInstance instanceof Customer).to.be.true;
      expect(customerInstance.dbCustomer.get('CustomerLevel')).to.not.be.undefined;
      expect(customerInstance.dbCustomer.get('CustomerLevel').level).to.be.equal(2);

      await db.sequelize.query(`DELETE from CustomerProfiles where id=${customerInstance.dbCustomer.id}`);
      await db.sequelize.query(`DELETE from Users where id=${customerInstance.dbCustomer.userId}`);
    });
  });

  describe('Static function setCustomerLevel should', () => {
    let customerId, storeId;

    before(() => {
      customerId = createdCustomer.dbCustomer.id;
      storeId = createdCustomer.dbCustomer.storeId;
    });

    it('exist', () => {
      expect(Customer.setCustomerLevel).to.be.exist;
      expect(Customer.setCustomerLevel).to.be.a('function');
    });

    it('upgrade to sent level if greater than current', async () => {
      const level = 3;

      expect(createdCustomer.dbCustomer.get('CustomerLevel').level).to.be.equal(1);

      await Customer.setCustomerLevel({ customerId, storeId, level });

      const customerLevel = await db.CustomerLevel.findOne({ where: { customerId, storeId } });

      expect(customerLevel).to.not.be.undefined;
      expect(customerLevel.level).to.be.equal(level);
    });

    it('downgrade to sent level if lower than current', async () => {
      const level = 2;

      await Customer.setCustomerLevel({ customerId, storeId, level });

      const customerLevel = await db.CustomerLevel.findOne({ where: { customerId, storeId } });

      expect(customerLevel).to.not.be.undefined;
      expect(customerLevel.level).to.be.equal(level);
    });

    after(() => Customer.setCustomerLevel({ customerId, storeId, level: 1 }));
  });

  describe('Class function modifyStatus should', () => {
    it('exist', () => {
      expect(createdCustomer.modifyStatus).to.be.exist;
      expect(createdCustomer.modifyStatus).to.be.a('function');
    });

    it('modify the status of an already created customer instance', async () => {
      expect(createdCustomer.dbCustomer.status).to.be.equal(CUSTOMER_STATUS.PENDING);
      const updatedCustomer = await createdCustomer.modifyStatus(CUSTOMER_STATUS.ACTIVE);
      expect(updatedCustomer.status).to.be.equal(CUSTOMER_STATUS.ACTIVE);
    });
  });

  describe('Class function getStore should', () => {
    it('exist', () => {
      expect(createdCustomer.getStore).to.be.exist;
      expect(createdCustomer.getStore).to.be.a('function');
    });

    it('return a the store model of an already created customer instance', async () => {
      const store = await createdCustomer.getStore();
      expect(store instanceof db.Store).to.be.true;
    });
  });

  describe('Class function getStoreWithLocations should', () => {
    it('exist', () => {
      expect(createdCustomer.getStoreWithLocations).to.be.exist;
      expect(createdCustomer.getStoreWithLocations).to.be.a('function');
    });

    it('return a the store model of an already created customer instance including the store location', async () => {
      const store = await createdCustomer.getStoreWithLocations();
      expect(store instanceof db.Store).to.be.true;
      expect(store).to.have.property('StoreLocations');
    });
  });

  describe('Class function getPoints should', () => {
    it('exist', () => {
      expect(createdCustomer.getPoints).to.be.exist;
      expect(createdCustomer.getPoints).to.be.a('function');
    });

    it('return the amount of points of an already created customer instance', async () => {
      const amount = await createdCustomer.getPoints();
      expect(amount).to.be.null;
    });
  });

  describe('Class function raw should', () => {
    it('exist', () => {
      expect(createdCustomer.raw).to.be.exist;
      expect(createdCustomer.raw).to.be.a('function');
    });

    it('return the raw model of the customer', async () => {
      expect(createdCustomer).to.have.property('dbCustomer');
      const rawCustomer = await createdCustomer.raw();
      expect(rawCustomer).to.not.have.property('dbCustomer');
    });
  });

  describe('Class function registerCouponPoints should', () => {
    let coupon;
    let point;
    let customerInstance;

    before(async function () {
      coupon = await Coupon.create(couponData);
      customerInstance = await Customer.initIfExists(customer.id, customer.customer.storeId);

      this.coupon = coupon;
    });

    after(async function () {
      await db.sequelize.query(`DELETE from UsageFrequencies where id=${this.coupon.dbCoupon.usageFrequencyId}`);
      await db.sequelize.query(
        `DELETE from Coupons where id=${this.coupon.dbCoupon.id} and storeId=${couponData.storeId}`,
      );
    });

    afterEach(function () {
      return db.sequelize.query(
        `DELETE from Points where couponId=${this.coupon.dbCoupon.id} and customerId=${customer.id}`,
      );
    });

    it('exist', () => {
      expect(customerInstance.registerCouponPoints).to.not.be.undefined;
    });

    it('return the newly created point entry', async () => {
      point = await customerInstance.registerCouponPoints(coupon);
      expect(point).to.not.be.undefined;
      expect(point.amount).to.not.be.undefined;
      expect(point.amount).to.be.equal(couponData.points);
    });

    it('return an error if the coupon is not valid to apply', async () => {
      const createdAt = moment
        .utc()
        .subtract(FOREVER_FREQUENCY_MIN_VALID_HOURS - 1, 'h')
        .format('YYYY-MM-DD HH:mm:ss');

      await db.sequelize.query(
        `INSERT into Points (storeId, customerId, couponId, amount, createdAt, updatedAt) values (${couponData.storeId},  ${customer.customer.id}, '${coupon.dbCoupon.id}', ${couponData.points}, '${createdAt}', '${createdAt}')`,
      );

      try {
        await customerInstance.registerCouponPoints(coupon);
        expect(false).to.be.true;
      } catch (err) {
        const { msg, code } = API_CUSTOMER_ERRORS['api/coupon-invalid-time-frame'];

        expect(err instanceof ApolloError).to.be.true;
        expect(err.message).to.be.equal(msg);
        expect(err.extensions.code).to.be.equal(code);
      }
    });

    it('return an error if the coupon is not in active state', async () => {
      await coupon.modifyStatus(COUPON_STATUS.PAUSED);
      coupon = await Coupon.initById(coupon.dbCoupon.id);

      try {
        await customerInstance.registerCouponPoints(coupon);
        expect(false).to.be.true;
      } catch (err) {
        const { msg, code } = API_CUSTOMER_ERRORS['api/coupon-is-not-active'];

        expect(err instanceof ApolloError).to.be.true;
        expect(err.message).to.be.equal(msg);
        expect(err.extensions.code).to.be.equal(code);
      }

      await coupon.modifyStatus(COUPON_STATUS.ACTIVE);
      coupon = await Coupon.initById(coupon.dbCoupon.id);
    });
  });

  describe('Class function getHistory should', () => {
    let customerInstance;

    before(async function () {
      customerInstance = await Customer.initIfExists(customer.id, customer.customer.storeId);
    });

    it('exist', () => {
      expect(customerInstance.getHistory).to.not.be.undefined;
    });

    it('return the corresponding history associated to the customer', async () => {
      const customerHistory = await customerInstance.getHistory();

      expect(customerHistory).to.be.a('array');

      for (const history of customerHistory) {
        expect(history).to.haveOwnProperty('id');
        expect(history).to.haveOwnProperty('type');
        expect(history).to.haveOwnProperty('value');
        expect(history).to.haveOwnProperty('createdAt');
      }
    });
  });
});
