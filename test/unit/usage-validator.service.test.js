const { expect } = require('chai');
const moment = require('moment');
const proxyquire = require('proxyquire');

const { USAGE_FREQUENCIES, FOREVER_FREQUENCY_MIN_VALID_HOURS } = require('@modules/core/constants/core.constants');
const { usageValidatorService } = require('@services/usage-validator.service');

const { FOREVER, ONE_TIME, EVERY_CERTAIN_DAYS, EVERY_CERTAIN_HOURS, CERTAIN_WEEK_DAYS } = USAGE_FREQUENCIES;

describe('Usage Validator Service Unit Tests', () => {
  describe('Service UsageValidator should', () => {
    it('exist', () => {
      expect(usageValidatorService).to.not.be.undefined;
    });
  });

  describe('Class function isValidToRequestNow should', () => {
    it('exist', () => {
      expect(usageValidatorService.isValidToRequestNow).to.not.be.undefined;
    });

    describe(`Given a ${FOREVER} Type`, () => {
      const usageFrequency = { type: FOREVER };

      it('return false if last time used is same day', () => {
        const lastTimeUsed = {
          createdAt: moment().endOf('date').subtract(1, 'hours'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.false;
      });

      it('return true if it has not been used yet', () => {
        const lastTimeUsed = { createdAt: '' };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });

      it(`return true if last time used is same than ${FOREVER_FREQUENCY_MIN_VALID_HOURS}`, () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(FOREVER_FREQUENCY_MIN_VALID_HOURS, 'hours'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });

      it(`return true if last time used is greater than ${FOREVER_FREQUENCY_MIN_VALID_HOURS}`, () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(FOREVER_FREQUENCY_MIN_VALID_HOURS + 1, 'hours'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });
    });

    describe(`Given a ${ONE_TIME} Type`, () => {
      const usageFrequency = { type: ONE_TIME };

      it('return false if it has already been used', () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(FOREVER_FREQUENCY_MIN_VALID_HOURS, 'hours'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.false;
      });

      it('return true if it has not been used yet', () => {
        const lastTimeUsed = { createdAt: '' };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });
    });

    describe(`Given a ${EVERY_CERTAIN_DAYS} Type`, () => {
      const usageFrequency = {
        type: EVERY_CERTAIN_DAYS,
        value: 2,
      };

      it('return false if last time used is lower than expected days', () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(usageFrequency.value - 1, 'd'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.false;
      });

      it('return true if it has not been used yet', () => {
        const lastTimeUsed = { createdAt: '' };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });

      it('return true if last time used is same than expected days', () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(usageFrequency.value, 'd'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });

      it('return true if last time used is greater than expected days', () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(usageFrequency.value + 1, 'd'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });
    });

    describe(`Given a ${EVERY_CERTAIN_HOURS} Type`, () => {
      const usageFrequency = {
        type: EVERY_CERTAIN_HOURS,
        value: 32,
      };

      it('return false if last time used is lower than expected hours', () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(usageFrequency.value - 1, 'h'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.false;
      });

      it('return true if it has not been used yet', () => {
        const lastTimeUsed = { createdAt: '' };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });

      it('return true if last time used is same than expected hours', () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(usageFrequency.value, 'h'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });

      it('return true if last time used is greater than expected hours', () => {
        const lastTimeUsed = {
          createdAt: moment().subtract(usageFrequency.value + 1, 'h'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });
    });

    describe(`Given a ${CERTAIN_WEEK_DAYS} Type`, () => {
      const yesterdayWeekday = moment().subtract(1, 'd').isoWeekday();
      const todayWeekday = moment().isoWeekday();
      const tomorrowWeekday = moment().add(1, 'd').isoWeekday();

      it('return false if requested on different weekday', () => {
        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: `${yesterdayWeekday},${tomorrowWeekday}`,
        };

        const lastTimeUsed = {
          createdAt: moment(),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.false;
      });

      it('return false if requested on correct weekday but twice same day', () => {
        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: `${todayWeekday},${tomorrowWeekday}`,
        };

        const lastTimeUsed = {
          createdAt: moment().subtract(1, 'h'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.false;
      });

      it('return false if it has not been used yet but requested on incorrect weekday', () => {
        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: `${yesterdayWeekday},${tomorrowWeekday}`,
        };

        const lastTimeUsed = { createdAt: '' };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency })).to.be.false;
        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.false;
      });

      it('return true if requested on correct weekday by first time', () => {
        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: `${todayWeekday},${tomorrowWeekday}`,
        };

        const lastTimeUsed = {
          createdAt: moment().subtract(1, 'd'),
        };

        expect(usageValidatorService.isValidToRequestNow({ usageFrequency, lastTimeUsed })).to.be.true;
      });
    });
  });

  describe('Class function getNextTimeAvailable should', () => {
    it('exist', () => {
      expect(usageValidatorService.getNextTimeAvailable).to.not.be.undefined;
    });

    describe(`Given a ${FOREVER} usageFrequency`, () => {
      it('if has never been used it should be today start of date', () => {
        const usageFrequency = { type: FOREVER };

        const todayStart = moment().startOf('date').toISOString();

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency);

        expect(date).to.be.equal(todayStart);
      });

      it('return the next date based on the diff with FOREVER_FREQUENCY_MIN_VALID_HOURS', () => {
        const usageFrequency = { type: FOREVER };
        const date = usageValidatorService.getNextTimeAvailable(usageFrequency, moment('2021-08-24T00:00:00'));

        const expected = moment('2021-08-24T00:00:00')
          .add(FOREVER_FREQUENCY_MIN_VALID_HOURS, 'h')
          .startOf('date')
          .toISOString();

        expect(moment(date).isSame(expected, 'year')).to.be.true;
        expect(moment(date).isSame(expected, 'month')).to.be.true;
        expect(moment(date).isSame(expected, 'day')).to.be.true;
        expect(moment(date).isSame(expected, 'hour')).to.be.true;
        expect(moment(date).isSame(expected, 'minute')).to.be.true;
      });
    });

    describe(`Given a ${ONE_TIME} usageFrequency`, () => {
      it('return an empty value', () => {
        const usageFrequency = { type: ONE_TIME };
        const date = usageValidatorService.getNextTimeAvailable(usageFrequency, moment());

        expect(date).to.be.equal('');
      });

      it('if has never been used it should be today start of date', () => {
        const usageFrequency = { type: ONE_TIME };

        const todayStart = moment().startOf('date').toISOString();

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency);

        expect(date).to.be.equal(todayStart);
      });
    });

    describe(`Given a ${CERTAIN_WEEK_DAYS} usageFrequency`, () => {
      it('if has never been used and requested on not valid weekday it should be the next possible day', () => {
        const wednesday = '2021-08-18T08:00:00.000Z';

        const usageValidatorMock = proxyquire('../../src/services/usage-validator.service', {
          moment: () => moment(wednesday),
        });

        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: '2,4',
        };

        const date = usageValidatorMock.usageValidatorService.getNextTimeAvailable(usageFrequency);

        expect(moment(date).isSame(moment(wednesday).add(1, 'd').startOf('date'))).to.be.true;
      });

      it('if has never been used and requested on valid weekday it should be the same day', () => {
        const thursday = '2021-08-19T08:00:00.000Z';

        const usageValidatorMock = proxyquire('../../src/services/usage-validator.service', {
          moment: () => moment(thursday),
        });

        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: '2,4',
        };

        const date = usageValidatorMock.usageValidatorService.getNextTimeAvailable(usageFrequency);

        expect(moment(date).isSame(moment(thursday).startOf('date'))).to.be.true;
      });

      it('return a date of the same week when there is a next day available', () => {
        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: '2,4',
        };

        const tuesday = moment('2021-08-10T15:30:00');
        const thursdaySameWeek = moment('2021-08-12T00:00:00');

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency, tuesday);

        expect(moment(date).isSame(thursdaySameWeek)).to.be.true;
      });

      it('return a date of the next week when there is no next day available in the same week', () => {
        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: '2,4',
        };

        const thursday = moment('2021-08-12T15:30:00');
        const tuesdayNextWeek = moment('2021-08-17T00:00:00');

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency, thursday);

        expect(moment(date).isSame(tuesdayNextWeek)).to.be.true;
      });

      it('return a date of the next week when there is only one day available', () => {
        const usageFrequency = {
          type: CERTAIN_WEEK_DAYS,
          value: '2',
        };

        const tuesday = moment('2021-08-10T15:30:00');
        const tuesdayNextWeek = moment('2021-08-17T00:00:00');

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency, tuesday);

        expect(moment(date).isSame(tuesdayNextWeek)).to.be.true;
      });
    });

    describe(`Given a ${EVERY_CERTAIN_DAYS} usageFrequency`, () => {
      it('return a date of the next week when there is only one day available', () => {
        const usageFrequency = {
          type: EVERY_CERTAIN_DAYS,
          value: '2',
        };

        const tuesday = moment('2021-08-10T15:30:00');
        const thursday = moment('2021-08-12T00:00:00');

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency, tuesday);

        expect(moment(date).isSame(thursday)).to.be.true;
      });

      it('if has never been used it should be today start of date', () => {
        const usageFrequency = { type: EVERY_CERTAIN_DAYS };

        const todayStart = moment().startOf('date').toISOString();

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency);

        expect(date).to.be.equal(todayStart);
      });
    });

    describe(`Given a ${EVERY_CERTAIN_HOURS} usageFrequency`, () => {
      it('return a date of the next week when there is only one day available', () => {
        const usageFrequency = {
          type: EVERY_CERTAIN_HOURS,
          value: '2',
        };

        const lastUsed = moment('2021-08-10T15:30:00');
        const expected = moment('2021-08-10T17:30:00');

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency, lastUsed);

        expect(moment(date).isSame(expected)).to.be.true;
      });

      it('if has never been used it should be today start of date', () => {
        const usageFrequency = { type: EVERY_CERTAIN_DAYS };

        const todayStart = moment().startOf('date').toISOString();

        const date = usageValidatorService.getNextTimeAvailable(usageFrequency);

        expect(date).to.be.equal(todayStart);
      });
    });
  });
});
