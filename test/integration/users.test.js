const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const faker = require('faker');

const { validRequest, firebase, apiUrl, customerToken } = require('../utils/test.utils');

const usersQueries = require('./users.queries');

chai.use(chaiHttp);

describe('UserCreation', () => {
  let userObj;
  let partnerUserObj;
  let loginToken;
  let authToken;
  let userAuthToken;

  before(async () => {
    userObj = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
      birthDate: new Date().toISOString(),
      nickname: Math.random().toString(36).substring(7),
    };
    partnerUserObj = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
      birthDate: new Date().toISOString(),
      nickname: Math.random().toString(36).substring(7),
    };

    userAuthToken = await customerToken();
  });

  it('Creates User correctly', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .send({ query: usersQueries.createUser(userObj) })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data).to.have.property('createUser');
        expect(res.body.data.createUser).to.have.property('loginToken');
        loginToken = res.body.data.createUser.loginToken;
        done();
      });
  });

  it('Creates Partner User correctly', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .send({ query: usersQueries.createPartnerUser(partnerUserObj) })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data).to.have.property('createPartnerUser');
        expect(res.body.data.createPartnerUser).to.have.property('loginToken');
        loginToken = res.body.data.createPartnerUser.loginToken;
        done();
      });
  });

  it('Gets a valid login token and generate auth token', async () => {
    const user = await firebase.auth().signInWithCustomToken(loginToken);
    expect(user).to.not.be.null;
    authToken = await firebase.auth().currentUser.getIdToken();
    expect(authToken).to.not.be.null;
  });

  it('Finds invalid pendingUser', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .send({ query: usersQueries.findPendingUser('asd@asd.com') })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data).to.have.property('findPendingUser');
        expect(res.body.data.findPendingUser).to.be.null;
        done();
      });
  });

  it('Checks if user has valid account for the created Auth Token', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({ query: usersQueries.isValidUser() })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data).to.have.property('isValidUser');
        expect(res.body.data.isValidUser).to.be.a('boolean');
        done();
      });
  });

  it('Updates userInfo', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${userAuthToken}`)
      .send({
        query: usersQueries.updateUser({
          firstName: 'Jorge',
          lastName: 'Salpietro',
          email: 'nocetti.tomas@gmail.com',
        }),
      })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data).to.have.property('updateUser');
        expect(res.body.data.updateUser).to.have.property('id');
        done();
      });
  });

  it('Updates userInfo', done => {
    const token = faker.git.commitSha();

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${userAuthToken}`)
      .send({
        query: usersQueries.registerUserPushToken(token),
      })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data).to.have.property('registerUserPushToken');
        expect(res.body.data.registerUserPushToken).to.have.property('id');
        expect(res.body.data.registerUserPushToken).to.have.property('pushToken');
        expect(res.body.data.registerUserPushToken.pushToken).to.be.equal(token);
        done();
      });
  });
});
