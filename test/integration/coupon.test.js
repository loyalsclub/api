const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');

const { apiUrl, customerToken, coupon } = require('../utils/test.utils');

const couponQueries = require('./coupon.queries');

chai.use(chaiHttp);

describe('Coupon Usual Requests', () => {
  let authToken;

  before(async () => {
    authToken = await customerToken();
  });

  it('Validate coupon code should return false if id is already in use', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: couponQueries.validateCouponCode({ couponCode: coupon.couponCode, storeId: coupon.store.id }),
      })
      .end((err, res) => {
        expect(res.body.data.validateCouponCode).to.be.a('boolean');
        expect(res.body.data.validateCouponCode).to.be.equal(false);
        done();
      });
  });

  it('Validate coupon code should return true if id is not in use', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: couponQueries.validateCouponCode({ couponCode: '253inmaculadaconcepcion', storeId: coupon.store.id }),
      })
      .end((err, res) => {
        expect(res.body.data.validateCouponCode).to.be.a('boolean');
        expect(res.body.data.validateCouponCode).to.be.equal(true);
        done();
      });
  });
});
