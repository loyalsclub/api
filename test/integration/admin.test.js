const fs = require('fs');
const path = require('path');

const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const faker = require('faker');
const FormData = require('form-data');
const fetch = require('node-fetch');

const { COUPON_STATUS, USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');
const { COUNTRY_CODES } = require('@modules/store/constants/store.constants');

const { validRequest, apiUrl, adminToken, store } = require('../utils/test.utils');

const adminQueries = require('./admin.queries');

chai.use(chaiHttp);

const PREFERENCES = {
  auth: true,
};

describe('AdminQueries', () => {
  let authToken;

  before(async () => {
    if (!PREFERENCES.auth) return;
    authToken = await adminToken();
  });

  it('Get admin statistics', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: adminQueries.adminGetStatistics(),
      })
      .end((err, res) => {
        validRequest(err, res);
        done();
      });
  });

  describe('Stores Related Requests', () => {
    let createdStore;

    it('Admin get Store', done => {
      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminGetStore(store.id),
        })
        .end((err, res) => {
          validRequest(err, res);
          expect(res.body.data.adminGetStore).to.be.not.undefined;

          const store = res.body.data.adminGetStore;

          expect(store).to.have.property('id');
          expect(store).to.have.property('EmployeeProfiles');
          expect(store).to.have.property('CustomerProfiles');
          expect(store).to.have.property('StoreLocations');
          expect(store).to.have.property('StoreRewards');

          if (store.StoreRewards.length) {
            expect(store.StoreRewards[0]).to.have.property('StoreRewardUsages');
          }

          done();
        });
    });

    it('Admin list stores', done => {
      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminListStores(),
        })
        .end((err, res) => {
          validRequest(err, res);
          expect(res.body.data.adminListStores).to.be.an('array');

          for (const store of res.body.data.adminListStores) {
            expect(store).to.have.property('id');
            expect(store).to.have.property('EmployeeProfiles');
            expect(store).to.have.property('CustomerProfiles');
            expect(store).to.have.property('StoreLocations');
          }

          done();
        });
    });

    it('Create a store for a user', done => {
      const body = new FormData();

      const storePayload = {
        name: faker.company.companyName(),
        category: 'dress',
        alias: Math.random().toString(36).substring(7),
        description: faker.company.catchPhraseDescriptor(),
        level2: '100',
        level3: '200',
        countryCode: COUNTRY_CODES.MEXICO,
      };

      body.append(
        'operations',
        JSON.stringify({
          query: adminQueries.adminCreateStoreForUser(1),
          variables: {
            store: {
              ...storePayload,
              profileImage: null,
              StoreLocations: [
                {
                  address: '1100 Princess Rd, Manchester M15 5AS, UK',
                  geo: { type: 'Point', coordinates: [53.4614267, -2.246362099999942] },
                },
              ],
            },
          },
        }),
      );

      body.append('map', JSON.stringify({ 1: ['variables.store.profileImage'] }));

      body.append('1', fs.createReadStream(path.resolve(__dirname, '../utils/store-placeholder.png')));

      fetch(apiUrl + '/graphql', { method: 'POST', headers: { Authorization: `Bearer ${authToken}` }, body })
        .then(async res => ({ body: await res.json(), status: 200 }))
        .then(res => {
          validRequest(null, res);
          expect(res.body.data.adminCreateStoreForUser).to.be.not.empty;
          createdStore = res.body.data.adminCreateStoreForUser;
          expect(createdStore.countryCode).to.be.equal(COUNTRY_CODES.MEXICO);
          done();
        });
    });

    it('Update a store for a user', done => {
      const body = new FormData();

      const storePayload = {
        name: faker.company.companyName(),
        category: 'bar',
        alias: Math.random().toString(36).substring(7),
        description: faker.company.catchPhraseDescriptor(),
        socialMedia: {
          facebook: 'https://www.facebook.com/fake/',
          whatsapp: 'https://wa.me/5491112341234',
          instagram: 'https://www.instagram.com/fake/',
        },
      };

      body.append(
        'operations',
        JSON.stringify({
          query: adminQueries.adminModifyStore(createdStore.id),
          variables: {
            store: {
              ...storePayload,
              profileImage: null,
              StoreLocations: [
                {
                  id: createdStore.StoreLocations[0].id,
                  address: '2200 Princess Rd, Manchester M15 5AS, UK',
                  geo: { type: 'Point', coordinates: [53.4614267, -2.246362099999942] },
                  website: faker.internet.url(),
                  phone: faker.phone.phoneNumber(),
                  email: faker.internet.email(),
                },
                {
                  address: '3300 Princess Rd, Manchester M15 5AS, UK',
                  geo: { type: 'Point', coordinates: [53.4614267, -2.246362099999942] },
                },
              ],
            },
          },
        }),
      );

      body.append('map', JSON.stringify({ 1: ['variables.store.profileImage'] }));

      body.append('1', fs.createReadStream(path.resolve(__dirname, '../utils/store-placeholder.png')));

      fetch(apiUrl + '/graphql', { method: 'POST', headers: { Authorization: `Bearer ${authToken}` }, body })
        .then(async res => ({ body: await res.json(), status: 200 }))
        .then(res => {
          validRequest(null, res);
          expect(res.body.data.adminModifyStore).to.be.not.empty;
          createdStore = res.body.data.adminModifyStore;
          expect(createdStore.name).to.be.equal(storePayload.name);
          expect(createdStore.category).to.be.equal(storePayload.category);
          expect(createdStore.alias).to.be.equal(storePayload.alias);
          expect(createdStore.description).to.be.equal(storePayload.description);
          expect(createdStore.socialMedia).to.be.deep.equal(storePayload.socialMedia);
          expect(createdStore.StoreLocations.length).to.be.equal(2);
          done();
        });
    });

    it('Remove created store', done => {
      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminRemoveStore(createdStore.id),
        })
        .end((err, res) => {
          validRequest(err, res);
          done();
        });
    });
  });

  describe('Store Rewards Related Requests', () => {
    let createdStoreReward;

    it('Create StoreReward', done => {
      const storeRewardPayload = {
        storeLevelId: 1,
        title: 'Testing Store Reward',
        description: 'Testing description',
        usageFrequencyType: USAGE_FREQUENCIES.ONE_TIME,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminAddStoreReward(),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          validRequest(err, res);
          createdStoreReward = { ...res.body.data.adminAddStoreReward };
          done();
        });
    });

    it('Modify StoreReward', done => {
      const storeRewardPayload = {
        storeLevelId: 1,
        title: 'Testing Store Reward Modification',
        description: 'Testing description Modified',
        usageFrequencyType: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
        usageFrequencyValue: '2,4,6',
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminModifyStoreReward(createdStoreReward.id),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          validRequest(err, res);
          createdStoreReward = { ...res.body.data.adminModifyStoreReward };
          done();
        });
    });

    it('Remove StoreReward', done => {
      const storeRewardPayload = {
        rewardId: createdStoreReward.id,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminRemoveStoreReward(storeRewardPayload),
        })
        .end((err, res) => {
          validRequest(err, res);
          done();
        });
    });
  });

  describe('Coupons Related Requests', () => {
    let createdCoupon;

    it('Create coupon for store', done => {
      const couponPayload = {
        couponCode: faker.random.alphaNumeric(6),
        storeId: store.id,
        points: 200,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminCreateCouponForStore(couponPayload),
        })
        .end((err, res) => {
          validRequest(err, res);
          createdCoupon = { ...res.body.data.adminCreateCouponForStore };
          done();
        });
    });

    it('Get coupons', done => {
      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminGetCoupons(),
        })
        .end((err, res) => {
          validRequest(err, res);
          expect(res.body.data.adminGetCoupons.edges).to.be.an('array');
          for (let coupon of res.body.data.adminGetCoupons.edges) {
            expect(coupon).to.have.property('cursor');
            expect(coupon).to.have.property('node');
            expect(coupon.node).to.have.property('id');
          }
          done();
        });
    });

    it('Get coupons by store id', done => {
      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminGetCoupons(store.id),
        })
        .end((err, res) => {
          validRequest(err, res);
          expect(res.body.data.adminGetCoupons.edges).to.be.an('array');
          for (let coupon of res.body.data.adminGetCoupons.edges) {
            expect(coupon).to.have.property('cursor');
            expect(coupon).to.have.property('node');
            expect(coupon.node).to.have.property('id');
          }
          done();
        });
    });

    it('Modify coupon status', async () => {
      expect(createdCoupon.status).to.be.equal(COUPON_STATUS.ACTIVE);

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: adminQueries.adminModifyCoupon({
            id: createdCoupon.id,
            status: COUPON_STATUS.PAUSED,
          }),
        })
        .end((err, res) => {
          validRequest(err, res);
          expect(res.body.data.adminModifyCoupon.status).to.be.equal(COUPON_STATUS.PAUSED);
        });
    });
  });
});
