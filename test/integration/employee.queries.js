const employeeLoadPoints = payload => {
  return `mutation{
    employeeLoadPoints(
      amount: ${payload.amount},
      userField: "${payload.userField}",
      storeId: ${payload.storeId}
    ){
      id
    }
  }`;
};

const employeeLevelsAndRewardsByStoreId = payload => {
  return `query{
    employeeLevelsAndRewardsByStoreId(storeId: ${payload.storeId}){
      id,
      storeId,
      StoreRewards{
        id,
        title
      },
      level,
      points
    }
  }`;
};

const employeeInviteNewUser = payload => {
  return `mutation{
    employeeInviteNewUser(
      firstName: "${payload.firstName}",
      lastName: "${payload.lastName}",
      email: "${payload.email}",
      storeId: ${payload.storeId}
    ){
      id
    }
  }`;
};

const employeeInviteNewUserInitialLevel = payload => {
  return `mutation{
    employeeInviteNewUser(
      firstName: "${payload.firstName}",
      lastName: "${payload.lastName}",
      email: "${payload.email}",
      storeId: ${payload.storeId}
      initialLevel: ${payload.initialLevel}
      cardId: "${payload.cardId}"
    ){
      id
      cardId
      CustomerLevel {
        level
      }
    }
  }`;
};

const employeeGetUser = payload => {
  return `query{
    employeeGetUser(
      userField: "${payload.userField}",
      storeId: ${payload.storeId}
    ){
      id
    }
  }`;
};

const employeeGetCustomer = payload => {
  return `query{
    employeeGetCustomer(
      userField: "${payload.userField}",
      storeId: ${payload.storeId}
    ){
      id
      CustomerLevel {
        level
        total
      }
    }
  }`;
};

const employeeCreateCustomer = payload => {
  return `mutation{
    employeeCreateCustomer(
      userField: "${payload.userField}",
      storeId: ${payload.storeId}
    ){
      id
    }
  }`;
};

const employeeGetStatistics = payload => {
  return `query{
    employeeGetStatistics(
      storeId: ${payload.storeId}
    ){
      customersTotal,
      level1,
      level2,
      level3
      rewards {
        total
        level1
        level2
        level3
      }
    }
  }`;
};

const employeeGetStoreCustomers = storeId => {
  return `query{
    employeeGetStoreCustomers(storeId: ${storeId}){
      id
      User {
        id
      }
      CustomerLevel {
        total
      }
    }
  }`;
};

const employeeSetCustomerLevel = (storeId, customerId, level) => {
  return `mutation {
    employeeSetCustomerLevel(storeId: ${storeId}, customerId: ${customerId}, level: ${level}) {
      id
      CustomerLevel {
        level
      }
    }
  }`;
};

const employeeGetCustomerHistory = (storeId, customerId) => {
  return `query{
    employeeGetCustomerHistory(storeId: ${storeId}, customerId: ${customerId}){
      id
      type
      value
      createdAt
    }
  }`;
};

const employeeCreateCoupon = payload => {
  return `mutation{
    employeeCreateCoupon(
      storeId: "${payload.storeId}"
      amount: ${payload.amount}
      ) {
        id
        couponCode
        url
      }
    }`;
};

const employeeRegisterStoreRewardUsage = payload => {
  return `mutation{
    employeeRegisterStoreRewardUsage(
      storeId: "${payload.storeId}"
      storeRewardId: "${payload.storeRewardId}"
      userField: "${payload.userField}"
      ) {
        id
      }
    }`;
};

const employeeGetCustomerAvailableRewards = payload => {
  return `query{
    employeeGetCustomerAvailableRewards(
      storeId: "${payload.storeId}"
      userField: "${payload.userField}"
      ) {
        id
        level
        StoreRewards {
          id
          title
          isAvailable
          lastTimeUsed
          nextTimeAvailable
          UsageFrequency {
            type
            value
          }
        }
      }
    }`;
};

const employeeAddStoreReward = storeId => {
  return `mutation($storeReward: CreateStoreRewardInput!){
    employeeAddStoreReward(
      storeId: "${storeId}"
      storeReward: $storeReward
    ) {
      id
      title
      description
      UsageFrequency {
        id
      }
    }
  }`;
};

const employeeModifyStoreReward = (storeRewardId, storeId) => {
  return `mutation($storeReward: CreateStoreRewardInput!){
    employeeModifyStoreReward(
      id: "${storeRewardId}"
      storeId: "${storeId}"
      storeReward: $storeReward
    ) {
      id
    }
  }`;
};

const employeeRemoveStoreReward = ({ rewardId, storeId }) => {
  return `mutation{
    employeeRemoveStoreReward(
      storeId: "${storeId}"
      rewardId: "${rewardId}"
    )
  }`;
};

const employeeModifyStore = storeId => {
  return `mutation($store: CreateStoreInput!){
    employeeModifyStore(storeId: "${storeId}", store: $store) {
      id
      name
      alias
      category
      description
      socialMedia
      profileImage
      StoreLocations {
        id
      }
    }
  }`;
};

module.exports = {
  employeeLevelsAndRewardsByStoreId,
  employeeCreateCustomer,
  employeeGetCustomer,
  employeeGetUser,
  employeeInviteNewUser,
  employeeInviteNewUserInitialLevel,
  employeeGetStatistics,
  employeeLoadPoints,
  employeeCreateCoupon,
  employeeRegisterStoreRewardUsage,
  employeeGetCustomerAvailableRewards,
  employeeAddStoreReward,
  employeeModifyStoreReward,
  employeeRemoveStoreReward,
  employeeModifyStore,
  employeeGetStoreCustomers,
  employeeGetCustomerHistory,
  employeeSetCustomerLevel,
};
