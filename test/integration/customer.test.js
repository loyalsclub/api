const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const faker = require('faker');
const { QueryTypes } = require('sequelize');

const db = require('@db');
const { Coupon, Customer, User } = require('@models');
const { CUSTOMER_STATUS } = require('@modules/core/constants/core.constants');
const { API_CUSTOMER_ERRORS } = require('@modules/core/constants/error-codes.constants');
const firebase = require('@services/authentication/authentication.service');

const {
  validRequest,
  apiUrl,
  customerToken,
  notCustomerToken,
  employeeToken,
  getUserToken,
  customer,
  invalidRequest,
  coupon,
  inactiveCoupon,
  employee,
} = require('../utils/test.utils');

const customerQueries = require('./customer.queries');
const employeeQueries = require('./employee.queries');

chai.use(chaiHttp);

after(function () {
  return Promise.all([
    firebase.app().delete(),
    db.sequelize
      .authenticate()
      .then(() => db.sequelize.close())
      .catch(() => {}),
  ]);
});

describe('Customer Usual Requests', () => {
  let authToken;
  let employeeAuthToken;
  let notCustomerAuthToken;

  before(async () => {
    const response = await Promise.all([customerToken(), notCustomerToken(), employeeToken()]);
    authToken = response[0];
    notCustomerAuthToken = response[1];
    employeeAuthToken = response[2];
  });

  it('Get customer profile by store id', done => {
    let getStoreProfilePayload = {
      storeId: customer.customer.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.userCustomerProfileByStoreId(getStoreProfilePayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data.userCustomerProfileByStoreId).to.have.property('id');
        done();
      });
  });

  it('Get customer profile by card id', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.getCustomerByCardId({
          cardId: customer.customer.cardId,
        }),
      })
      .end((err, res) => {
        validRequest(err, res);

        const { Customer, StoreLevels } = res.body.data.getCustomerByCardId;

        expect(Customer).to.have.property('id');
        expect(Customer).to.have.property('CustomerLevel');
        expect(Customer.CustomerLevel).to.have.property('level');

        for (const storeLevel of StoreLevels) {
          expect(storeLevel).to.have.property('id');
          expect(storeLevel).to.have.property('level');
          expect(storeLevel).to.have.property('StoreRewards');
          for (const storeReward of storeLevel.StoreRewards) {
            expect(storeReward).to.have.property('id');
            expect(storeReward).to.have.property('title');
            expect(storeReward).to.have.property('isAvailable');
            expect(storeReward).to.have.property('lastTimeUsed');
            expect(storeReward).to.have.property('nextTimeAvailable');
            expect(storeReward).to.have.property('UsageFrequency');
            expect(storeReward.UsageFrequency).to.not.be.null;
            expect(storeReward).to.have.property('StoreRewardUsages');
          }
        }

        done();
      });
  });

  it('Get not customer profile by store id', done => {
    let getStoreProfilePayload = {
      storeId: customer.customer.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${notCustomerAuthToken}`)
      .send({
        query: customerQueries.userCustomerProfileByStoreId(getStoreProfilePayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data.userCustomerProfileByStoreId).to.have.property('id');
        done();
      });
  });

  it('Get all active customer profiles for a single user', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.getCustomerProfiles(CUSTOMER_STATUS.ACTIVE),
      })
      .end((err, res) => {
        validRequest(err, res);
        const profilesArray = res.body.data.userCustomerProfiles;
        expect(res.body.data.userCustomerProfiles).to.be.an('array');
        for (let profile of profilesArray) {
          expect(profile.status).to.equal(CUSTOMER_STATUS.ACTIVE);
        }
        done();
      });
  });

  it('Get all pending customer profiles for a single user', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.getCustomerProfiles(CUSTOMER_STATUS.PENDING),
      })
      .end((err, res) => {
        validRequest(err, res);
        const profilesArray = res.body.data.userCustomerProfiles;
        expect(res.body.data.userCustomerProfiles).to.be.an('array');
        for (let profile of profilesArray) {
          expect(profile.status).to.equal(CUSTOMER_STATUS.PENDING);
        }
        done();
      });
  });

  it('Accept stores-request modifying profile', done => {
    const modifyCustomerProfilePayload = {
      id: 2,
      status: CUSTOMER_STATUS.ACTIVE,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.modifyCustomerProfile(modifyCustomerProfilePayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        const response = res.body.data.modifyCustomerProfile;
        expect(response).to.have.property('status').to.equal(CUSTOMER_STATUS.ACTIVE);
        expect(response).to.have.property('id');
        done();
      });
  });

  it('Invalid request for modifying profile', done => {
    const modifyCustomerProfilePayload = {
      id: 3,
      status: CUSTOMER_STATUS.DELETED,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.modifyCustomerProfile(modifyCustomerProfilePayload),
      })
      .end((err, res) => {
        invalidRequest(err, res, API_CUSTOMER_ERRORS['api/user-is-not-a-customer']);
        done();
      });
  });

  it('Delete stores-request modifying profile', done => {
    const modifyCustomerProfilePayload = {
      id: 2,
      status: CUSTOMER_STATUS.DELETED,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.modifyCustomerProfile(modifyCustomerProfilePayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        const response = res.body.data.modifyCustomerProfile;
        expect(response).to.have.property('status').to.equal(CUSTOMER_STATUS.DELETED);
        expect(response).to.have.property('id');
        done();
      });
  });

  it('Register points with active coupon or return error if hours has not passed', async () => {
    const payload = {
      storeId: coupon.store.id,
      couponCode: coupon.couponCode,
    };

    const couponToRegister = await Coupon.init(payload.couponCode, payload.storeId);
    const customerInstance = await Customer.initOrCreate(customer.id, customer.customer.storeId);
    const isValid = await customerInstance.validateCouponUsage(couponToRegister);

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.registerCouponPoints(payload),
      })
      .end((err, res) => {
        const validator = isValid ? validRequest : invalidRequest;
        validator(err, res);
      });
  });

  it('Register points with inactive coupon should return an error', done => {
    const payload = {
      storeId: inactiveCoupon.store.id,
      couponCode: inactiveCoupon.couponCode,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.registerCouponPoints(payload),
      })
      .end((err, res) => {
        invalidRequest(err, res);
        done();
      });
  });

  it('Get customer available store rewards', done => {
    const getCustomerAvailableRewards = {
      storeId: customer.customer.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: customerQueries.customerProfileGetStoreLevelsWithRewards(getCustomerAvailableRewards),
      })
      .end((err, res) => {
        validRequest(err, res);

        for (const storeLevel of res.body.data.customerProfileGetStoreLevelsWithRewards) {
          expect(storeLevel).to.have.property('id');
          expect(storeLevel).to.have.property('level');
          expect(storeLevel).to.have.property('StoreRewards');
          for (const storeReward of storeLevel.StoreRewards) {
            expect(storeReward).to.have.property('id');
            expect(storeReward).to.have.property('title');
            expect(storeReward).to.have.property('isAvailable');
            expect(storeReward).to.have.property('lastTimeUsed');
            expect(storeReward).to.have.property('nextTimeAvailable');
            expect(storeReward).to.have.property('UsageFrequency');
            expect(storeReward.UsageFrequency).to.not.be.null;
            expect(storeReward).to.have.property('StoreRewardUsages');
          }
        }

        done();
      });
  });

  describe('One time coupon', () => {
    let userInstance;
    let token;
    let couponId;

    before(async () => {
      const userObj = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        birthDate: new Date().toISOString(),
        nickname: Math.random().toString(36).substring(7),
      };

      userInstance = await User.createUser(userObj);

      token = await getUserToken(userObj.email, userObj.password);
    });

    after(async function () {
      await db.sequelize.query(`DELETE from Points where couponId=${couponId}`);
      await db.sequelize.query(`DELETE from Coupons where id=${couponId}`);
      await db.sequelize.query(`DELETE from CustomerProfiles where userId=${userInstance.dbUser.id}`);
      await db.sequelize.query(`DELETE from Users where id=${userInstance.dbUser.id}`);
    });

    it('Should return an error if two different customers try to use it', done => {
      const couponPayload = {
        storeId: employee.employee.storeId,
        amount: 2000,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${employeeAuthToken}`)
        .send({
          query: employeeQueries.employeeCreateCoupon(couponPayload),
        })
        .end((err, res) => {
          couponId = res.body.data.employeeCreateCoupon.id;

          const payload = {
            storeId: coupon.store.id,
            couponCode: res.body.data.employeeCreateCoupon.couponCode,
          };

          chai
            .request(apiUrl)
            .post('/graphql')
            .set('Authorization', `Bearer ${authToken}`)
            .send({
              query: customerQueries.registerCouponPoints(payload),
            })
            .end((err, res) => {
              validRequest(err, res);

              chai
                .request(apiUrl)
                .post('/graphql')
                .set('Authorization', `Bearer ${token}`)
                .send({
                  query: customerQueries.registerCouponPoints(payload),
                })
                .end((err, res) => {
                  invalidRequest(err, res);
                  done();
                });
            });
        });
    });
  });
});

describe('Customer Race Conditions', () => {
  let userInstance;
  let token;

  after(function () {
    return db.sequelize.query(`DELETE from CustomerProfiles where userId=${userInstance.dbUser.id}`).then(() => {
      return db.sequelize.query(`DELETE from Users where id=${userInstance.dbUser.id}`);
    });
  });

  before(async () => {
    const userObj = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
      birthDate: new Date().toISOString(),
      nickname: Math.random().toString(36).substring(7),
    };

    userInstance = await User.createUser(userObj);

    token = await getUserToken(userObj.email, userObj.password);
  });

  it('Call two endpoints with Customer.initOrCreate and only one customer should be created', done => {
    let isTestFinished = false;

    const setTestCaseAsFinished = err => {
      if (isTestFinished) {
        done(err);
      }

      isTestFinished = true;
    };

    const validateOnlyOneCustomerCreated = async response => {
      expect(response).status(200);

      const { quantityOfCustomers } = await db.sequelize.query(
        `SELECT count(id) as quantityOfCustomers FROM CustomerProfiles where userId=${userInstance.dbUser.id}`,
        {
          type: QueryTypes.SELECT,
          plain: true,
        },
      );

      expect(quantityOfCustomers).to.be.equal(1);

      setTestCaseAsFinished();
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: customerQueries.registerCouponPoints({
          storeId: coupon.store.id,
          couponCode: coupon.couponCode,
        }),
      })
      .then(validateOnlyOneCustomerCreated)
      .catch(setTestCaseAsFinished);

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${token}`)
      .send({
        query: customerQueries.userCustomerProfileByStoreId({
          storeId: customer.customer.storeId,
        }),
      })
      .then(validateOnlyOneCustomerCreated)
      .catch(setTestCaseAsFinished);
  });
});
