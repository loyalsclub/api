const validateCouponCode = coupon => {
  return `query{
        validateCouponCode(couponCode: "${coupon.couponCode}", storeId: ${coupon.storeId})
    }`;
};

module.exports = { validateCouponCode };
