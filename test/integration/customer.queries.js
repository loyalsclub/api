const userCustomerProfileByStoreId = payload => {
  return `query{
    userCustomerProfileByStoreId(storeId: ${payload.storeId}){
      id,
      CustomerLevel {
        currentLevelPoints
      }
    }
  }`;
};

const getCustomerByCardId = payload => {
  return `query {
    getCustomerByCardId(cardId: "${payload.cardId}"){
      Customer {
        id
        CustomerLevel {
          level
        }
      }
      StoreLevels {
        id
        level
        StoreRewards {
          id
          title
          isAvailable
          lastTimeUsed
          nextTimeAvailable
          UsageFrequency {
            type
            value
          }
          StoreRewardUsages {
            id
          }
        }
      }
    }
  }`;
};

const getCustomerProfiles = status => {
  return `query{
    userCustomerProfiles(status: ${status}){
      id,
      status,
      CustomerLevel {
        currentLevelPoints
      }
    }
  }`;
};

const modifyCustomerProfile = payload => {
  return `mutation{
    modifyCustomerProfile(
      id: ${payload.id} 
      status: ${payload.status} 
    ){
      id,
      status
    }
  }`;
};

const registerCouponPoints = ({ couponCode, storeId }) => {
  return `mutation {
    registerCouponPoints(
        couponCode: "${couponCode}"
        storeId: ${storeId}
    ){
      id
    }
  }`;
};

const customerProfileGetStoreLevelsWithRewards = payload => {
  return `query{
    customerProfileGetStoreLevelsWithRewards(
      storeId: "${payload.storeId}"
    ) {
        id
        level
        StoreRewards {
          id
          title
          isAvailable
          lastTimeUsed
          nextTimeAvailable
          UsageFrequency {
            type
            value
          }
          StoreRewardUsages {
            id
          }
        }
      }
    }`;
};

module.exports = {
  userCustomerProfileByStoreId,
  getCustomerProfiles,
  modifyCustomerProfile,
  registerCouponPoints,
  customerProfileGetStoreLevelsWithRewards,
  getCustomerByCardId,
};
