const getEmployeeProfiles = () => {
  return `query{
    userEmployeeProfiles{
      id
      permissionId
      Store {
        id
        name
        alias
      }
      User {
        id
      }
    }
  }`;
};

const storeLevelsAndRewardsById = storeId => {
  return `query{
    storeLevelsAndRewardsById(storeId: "${storeId}"){
      id
      level
      StoreRewards {
        id
        title
        isAvailable
        lastTimeUsed
        nextTimeAvailable
        UsageFrequency {
          type
          value
        }
      }
    }
  }`;
};

const createStoreForUser = () => {
  return `mutation($store: CreateStoreInput!){
    createStoreForUser(store: $store) {
      id
      alias
      countryCode
    }
  }`;
};

const generateValidStoreAlias = name => {
  return `query{
    generateValidStoreAlias(name: "${name}")
  }`;
};

const storeCategories = () => {
  return `query{
    storeCategories{
      name
      displayName
    }
  }`;
};

const storeCountries = () => {
  return `query{
    storeCountries{
      name
      displayName
      conversionRate
      areaCode
    }
  }`;
};

module.exports = {
  getEmployeeProfiles,
  storeLevelsAndRewardsById,
  createStoreForUser,
  generateValidStoreAlias,
  storeCategories,
  storeCountries,
};
