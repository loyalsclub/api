const fs = require('fs');
const path = require('path');

const chai = require('chai');
const chaiHttp = require('chai-http');
const faker = require('faker');
const FormData = require('form-data');
const fetch = require('node-fetch');

const db = require('@db');
const { Store } = require('@models');
const { USAGE_FREQUENCIES } = require('@modules/core/constants/core.constants');
const {
  API_EMPLOYEE_ERRORS,
  API_STORE_ERRORS,
  API_STORE_REWARD_ERRORS,
} = require('@modules/core/constants/error-codes.constants');
const { COUNTRY_CODES } = require('@modules/store/constants/store.constants');

const {
  validRequest,
  invalidRequest,
  apiUrl,
  employeeToken,
  notCustomer,
  customer,
  employee,
  store,
  demoStoreId,
  modifiableStoreId,
} = require('../utils/test.utils');

const employeeQueries = require('./employee.queries');

const expect = chai.expect;

chai.use(chaiHttp);

describe('Employee Usual Requests', () => {
  let authToken;
  let userObj;

  before(async () => {
    userObj = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      birthDate: new Date().toISOString(),
    };
    authToken = await employeeToken();
  });

  it('Add points to a valid customer', done => {
    let loadPointsPayload = {
      userField: customer.alias,
      amount: 200,
      storeId: customer.customer.storeId,
    };
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeLoadPoints(loadPointsPayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data.employeeLoadPoints).to.have.property('id');
        done();
      });
  });

  it('Add points to an invalid customer throws error', done => {
    let invalidLoadPointsPayload = {
      userField: 'ASDFG',
      amount: 200,
      storeId: customer.customer.storeId,
    };
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeLoadPoints(invalidLoadPointsPayload),
      })
      .end((err, res) => {
        expect(res.body.errors).not.to.be.undefined;
        expect(res).to.have.status(200);
        done();
      });
  });

  it('Add points to an valid customer with invalid store throws error', done => {
    let invalidLoadPointsPayload = {
      userField: customer.alias,
      amount: 200,
      storeId: -1,
    };
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeLoadPoints(invalidLoadPointsPayload),
      })
      .end((err, res) => {
        expect(res.body.errors).not.to.be.undefined;
        expect(res).to.have.status(200);
        done();
      });
  });

  it('Retrieve all levels with their rewards related to a store', done => {
    let employeeLevelsAndRewardsByStoreIdPayload = {
      storeId: employee.employee.storeId,
    };
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeLevelsAndRewardsByStoreId(employeeLevelsAndRewardsByStoreIdPayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        const levelsArray = res.body.data.employeeLevelsAndRewardsByStoreId;
        expect(levelsArray).to.be.an('array');
        for (let level of levelsArray) {
          expect(level.storeId).to.equal(employeeLevelsAndRewardsByStoreIdPayload.storeId);
        }
        expect(res).to.have.status(200);
        done();
      });
  });

  it('Invite New User as Customer', done => {
    let employeeInviteNewUserPayload = {
      ...userObj,
      storeId: employee.employee.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeInviteNewUser(employeeInviteNewUserPayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        expect(res.body.data.employeeInviteNewUser).to.have.property('id');
        done();
      });
  });

  it('Invite New User as Customer with initialLevel and cardId', done => {
    let employeeInviteNewUserPayload = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      storeId: employee.employee.storeId,
      cardId: '101',
      initialLevel: 2,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeInviteNewUserInitialLevel(employeeInviteNewUserPayload),
      })
      .end((err, res) => {
        validRequest(err, res);

        const customer = res.body.data.employeeInviteNewUser;

        expect(customer).to.have.property('id');
        expect(customer).to.have.property('cardId');
        expect(customer.cardId).to.be.equal('101');
        expect(customer).to.have.property('CustomerLevel');
        expect(customer.CustomerLevel.level).to.be.equal(2);
        done();
      });
  });

  it('Get User if exists returns null', done => {
    let employeeGetUserPayload = {
      userField: userObj.email,
      storeId: employee.employee.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeGetUser(employeeGetUserPayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        done();
      });
  });

  it('Get User if exists', done => {
    let employeeGetUserPayload = {
      userField: customer.email,
      storeId: employee.employee.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeGetUser(employeeGetUserPayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        done();
      });
  });

  it('Get Customer if exists', done => {
    let employeeGetUserPayload = {
      userField: notCustomer.email,
      storeId: employee.employee.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeGetCustomer(employeeGetUserPayload),
      })
      .end((err, res) => {
        validRequest(err, res);
        done();
      });
  });

  it('Add existing user as a Customer or return error if already existing', done => {
    const employeeCreateCustomerPayload = {
      userField: notCustomer.email,
      storeId: employee.employee.storeId,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeGetCustomer(employeeCreateCustomerPayload),
      })
      .end((_, res) => {
        const validatorFunction = res.body.data.employeeGetCustomer ? invalidRequest : validRequest;

        return chai
          .request(apiUrl)
          .post('/graphql')
          .set('Authorization', `Bearer ${authToken}`)
          .send({
            query: employeeQueries.employeeCreateCustomer(employeeCreateCustomerPayload),
          })
          .end((err, res) => {
            validatorFunction(err, res);
            done();
          });
      });
  });

  it('upgrade to next level', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeSetCustomerLevel(customer.customer.storeId, customer.customer.id, 2),
      })
      .end((err, res) => {
        validRequest(err, res);

        expect(res.body.data.employeeSetCustomerLevel).to.have.property('CustomerLevel');
        expect(res.body.data.employeeSetCustomerLevel.CustomerLevel).to.have.property('level');
        expect(res.body.data.employeeSetCustomerLevel.CustomerLevel.level).to.be.eq(2);

        done();
      });
  });

  it('Get Store Statistics', done => {
    let employeeGetStatisticsPayload = {
      storeId: employee.employee.storeId,
    };
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeGetStatistics(employeeGetStatisticsPayload),
      })
      .end((err, res) => {
        expect(res.body.data.employeeGetStatistics).to.have.property('customersTotal');
        expect(res.body.data.employeeGetStatistics).to.have.property('level1');
        expect(res.body.data.employeeGetStatistics).to.have.property('level2');
        expect(res.body.data.employeeGetStatistics).to.have.property('level3');
        expect(res.body.data.employeeGetStatistics).to.have.property('rewards');
        validRequest(err, res);
        done();
      });
  });

  it('Get Store Customers', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeGetStoreCustomers(employee.employee.storeId),
      })
      .end((err, res) => {
        expect(res.body.data.employeeGetStoreCustomers).to.be.an('array');

        for (const customer of res.body.data.employeeGetStoreCustomers) {
          expect(customer).to.have.property('id');
          expect(customer).to.have.property('User');
          expect(customer.User).to.have.property('id');
          expect(customer).to.have.property('CustomerLevel');
          expect(customer.CustomerLevel).to.have.property('total');
        }

        validRequest(err, res);
        done();
      });
  });

  it('Get Customer History', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeGetCustomerHistory(customer.customer.storeId, customer.id),
      })
      .end((err, res) => {
        expect(res.body.data.employeeGetCustomerHistory).to.be.an('array');

        for (const history of res.body.data.employeeGetCustomerHistory) {
          expect(history).to.have.property('id');
          expect(history).to.have.property('type');
          expect(history).to.have.property('value');
          expect(history).to.have.property('createdAt');
        }

        validRequest(err, res);
        done();
      });
  });

  it('Create coupon', done => {
    const couponPayload = {
      storeId: employee.employee.storeId,
      amount: 2000,
    };

    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: employeeQueries.employeeCreateCoupon(couponPayload),
      })
      .end((err, res) => {
        expect(res.body.data.employeeCreateCoupon).to.have.property('url');
        validRequest(err, res);
        done();
      });
  });

  describe('Register Store Rewards Usage Test Cases', () => {
    before(async function () {
      await db.sequelize.query(`DELETE from StoreRewardUsages where storeRewardId=${store.storeRewards[0].id}`);
    });

    it('Register invalid store reward usage should return an error', done => {
      const registerStoreRewardUsage = {
        userField: customer.email,
        storeId: employee.employee.storeId,
        storeRewardId: 0,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeRegisterStoreRewardUsage(registerStoreRewardUsage),
        })
        .end((err, res) => {
          invalidRequest(err, res, API_EMPLOYEE_ERRORS['api/invalid-store-reward']);
          done();
        });
    });

    it('Register store reward usage valid', done => {
      const registerStoreRewardUsage = {
        userField: customer.email,
        storeId: employee.employee.storeId,
        storeRewardId: store.storeRewards[0].id,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeRegisterStoreRewardUsage(registerStoreRewardUsage),
        })
        .end((err, res) => {
          validRequest(err, res);
          expect(res.body.data.employeeRegisterStoreRewardUsage).to.have.property('id');
          done();
        });
    });

    it('Register store reward usage invalid', done => {
      const registerStoreRewardUsage = {
        userField: customer.email,
        storeId: employee.employee.storeId,
        storeRewardId: store.storeRewards[0].id,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeRegisterStoreRewardUsage(registerStoreRewardUsage),
        })
        .end((err, res) => {
          invalidRequest(err, res, API_EMPLOYEE_ERRORS['api/unavailable-store-reward']);
          done();
        });
    });
  });

  describe('Store Rewards CRUD Related Requests', () => {
    let createdStoreReward;

    it('Create StoreReward', done => {
      const storeRewardPayload = {
        storeLevelId: 16,
        title: 'Testing Store Reward',
        description: 'Testing description',
        usageFrequencyType: USAGE_FREQUENCIES.ONE_TIME,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeAddStoreReward(modifiableStoreId),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          validRequest(err, res);
          createdStoreReward = { ...res.body.data.employeeAddStoreReward };
          done();
        });
    });

    it('Create StoreReward should throw error with non exiting store level id', done => {
      const storeRewardPayload = {
        storeLevelId: 0,
        title: 'Testing Store Reward',
        description: 'Testing description',
        usageFrequencyType: USAGE_FREQUENCIES.ONE_TIME,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeAddStoreReward(modifiableStoreId),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          invalidRequest(err, res, API_STORE_REWARD_ERRORS['api/store-level-not-found']);
          done();
        });
    });

    it('Create StoreReward should throw error with invalid store level id', done => {
      const storeRewardPayload = {
        storeLevelId: 4,
        title: 'Testing Store Reward',
        description: 'Testing description',
        usageFrequencyType: USAGE_FREQUENCIES.ONE_TIME,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeAddStoreReward(modifiableStoreId),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          invalidRequest(err, res, API_STORE_REWARD_ERRORS['api/store-level-invalid']);
          done();
        });
    });

    it('Modify StoreReward', done => {
      const storeRewardPayload = {
        storeLevelId: 16,
        title: 'Testing Store Reward Modification',
        description: 'Testing description Modified',
        usageFrequencyType: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
        usageFrequencyValue: '2,4,6',
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeModifyStoreReward(createdStoreReward.id, modifiableStoreId),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          validRequest(err, res);
          createdStoreReward = { ...res.body.data.employeeModifyStoreReward };
          done();
        });
    });

    it('Modify StoreReward should throw error with non existing store level id', done => {
      const storeRewardPayload = {
        storeLevelId: 0,
        title: 'Testing Store Reward Modification',
        description: 'Testing description Modified',
        usageFrequencyType: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
        usageFrequencyValue: '2,4,6',
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeModifyStoreReward(createdStoreReward.id, modifiableStoreId),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          invalidRequest(err, res, API_STORE_REWARD_ERRORS['api/store-level-not-found']);
          done();
        });
    });

    it('Modify StoreReward should throw error with invalid store level id', done => {
      const storeRewardPayload = {
        storeLevelId: 4,
        title: 'Testing Store Reward Modification',
        description: 'Testing description Modified',
        usageFrequencyType: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
        usageFrequencyValue: '2,4,6',
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeModifyStoreReward(createdStoreReward.id, modifiableStoreId),
          variables: {
            storeReward: storeRewardPayload,
          },
        })
        .end((err, res) => {
          invalidRequest(err, res, API_STORE_REWARD_ERRORS['api/store-level-invalid']);
          done();
        });
    });

    it('Remove StoreReward should throw an error with invalid store id', done => {
      const storeRewardPayload = {
        storeId: demoStoreId,
        rewardId: createdStoreReward.id,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeRemoveStoreReward(storeRewardPayload),
        })
        .end((err, res) => {
          invalidRequest(err, res, API_STORE_ERRORS['api/store-modification-forbidden']);
          done();
        });
    });

    it('Remove StoreReward', done => {
      const storeRewardPayload = {
        storeId: modifiableStoreId,
        rewardId: createdStoreReward.id,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeRemoveStoreReward(storeRewardPayload),
        })
        .end((err, res) => {
          validRequest(err, res);
          done();
        });
    });

    it('Get customer available store rewards', done => {
      const getCustomerAvailableRewards = {
        userField: customer.email,
        storeId: employee.employee.storeId,
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeGetCustomerAvailableRewards(getCustomerAvailableRewards),
        })
        .end((err, res) => {
          validRequest(err, res);

          for (const storeLevel of res.body.data.employeeGetCustomerAvailableRewards) {
            expect(storeLevel).to.have.property('id');
            expect(storeLevel).to.have.property('level');
            expect(storeLevel).to.have.property('StoreRewards');

            for (const storeReward of storeLevel.StoreRewards) {
              expect(storeReward).to.have.property('title');
              expect(storeReward).to.have.property('isAvailable');
              expect(storeReward).to.have.property('nextTimeAvailable');
              expect(storeReward).to.have.property('UsageFrequency');
              expect(storeReward.UsageFrequency).to.not.be.null;
            }
          }

          done();
        });
    });
  });

  describe('Stores Related Requests', function () {
    before(async function () {
      const storePayload = {
        name: faker.company.companyName(),
        alias: Math.random().toString(36).substring(7),
        category: 'dress',
        description: faker.company.catchPhraseDescriptor(),
        countryCode: COUNTRY_CODES.MEXICO,
        createdAt: '2020-10-24 21:04:25',
        updatedAt: '2020-10-24 21:04:25',
      };

      const { lastStoreId } = await db.sequelize.query('SELECT max(id) as lastStoreId FROM Stores', {
        type: db.sequelize.QueryTypes.SELECT,
        plain: true,
      });

      const storeId = lastStoreId + 1;

      await db.sequelize.query(
        `INSERT INTO
        Stores (
          id,
          name,
          alias,
          category,
          description,
          countryCode,
          createdAt,
          updatedAt
        )
        values (
          '${storeId}',
          '${storePayload.name}',
          '${storePayload.alias}',
          '${storePayload.category}',
          '${storePayload.description}',
          '${storePayload.countryCode}',
          '${storePayload.createdAt}',
          '${storePayload.updatedAt}'
        )`,
      );

      await db.sequelize.query(
        `INSERT INTO
        EmployeeProfiles (
          userId,
          storeId,
          permissionId,
          createdAt,
          updatedAt
        )
        values (
          '${employee.employee.userId}',
          '${storeId}',
          '${1}',
          '${storePayload.createdAt}',
          '${storePayload.updatedAt}'
        )`,
      );

      this.store = await Store.init(storeId);
    });

    after(async function () {
      await db.sequelize.query(`DELETE from StoreLocations where storeId='${this.store.dbStore.id}'`);
      await db.sequelize.query(`DELETE from Stores where id='${this.store.dbStore.id}'`);
      await db.sequelize.query(`DELETE from EmployeeProfiles where storeId='${this.store.dbStore.id}'`);
    });

    it('Update a store for a user', function (done) {
      const body = new FormData();

      const storePayload = {
        name: faker.company.companyName(),
        category: 'bar',
        alias: Math.random().toString(36).substring(7),
        description: faker.company.catchPhraseDescriptor(),
        socialMedia: {
          facebook: 'https://www.facebook.com/fake/',
          whatsapp: 'https://wa.me/5491112341234',
          instagram: 'https://www.instagram.com/fake/',
        },
      };

      body.append(
        'operations',
        JSON.stringify({
          query: employeeQueries.employeeModifyStore(this.store.dbStore.id),
          variables: {
            store: {
              ...storePayload,
              profileImage: null,
              StoreLocations: [
                {
                  address: '2200 Princess Rd, Manchester M15 5AS, UK',
                  geo: { type: 'Point', coordinates: [53.4614267, -2.246362099999942] },
                  website: faker.internet.url(),
                  phone: faker.phone.phoneNumber(),
                  email: faker.internet.email(),
                },
              ],
            },
          },
        }),
      );

      body.append('map', JSON.stringify({ 1: ['variables.store.profileImage'] }));

      body.append('1', fs.createReadStream(path.resolve(__dirname, '../utils/store-placeholder.png')));

      fetch(apiUrl + '/graphql', { method: 'POST', headers: { Authorization: `Bearer ${authToken}` }, body })
        .then(async res => ({ body: await res.json(), status: 200 }))
        .then(res => {
          validRequest(null, res);
          expect(res.body.data.employeeModifyStore).to.be.not.empty;

          const updatedStore = res.body.data.employeeModifyStore;
          expect(updatedStore.name).to.be.equal(storePayload.name);
          expect(updatedStore.category).to.be.equal(storePayload.category);
          expect(updatedStore.alias).to.be.equal(storePayload.alias);
          expect(updatedStore.description).to.be.equal(storePayload.description);
          expect(updatedStore.socialMedia).to.be.deep.equal(storePayload.socialMedia);
          expect(updatedStore.profileImage).to.be.not.null;
          expect(updatedStore.StoreLocations.length).to.be.equal(1);
          done();
        });
    });

    it('Update a store for a user without image should keep it', function (done) {
      const storePayload = {
        name: faker.company.companyName(),
        category: 'bar',
        alias: Math.random().toString(36).substring(7),
        description: faker.company.catchPhraseDescriptor(),
        socialMedia: {
          facebook: 'https://www.facebook.com/fake/',
          whatsapp: 'https://wa.me/5491112341234',
          instagram: 'https://www.instagram.com/fake/',
        },
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeModifyStore(this.store.dbStore.id),
          variables: {
            store: { ...storePayload },
          },
        })
        .end((err, res) => {
          validRequest(err, res);
          expect(res.body.data.employeeModifyStore).to.be.not.empty;

          const updatedStore = res.body.data.employeeModifyStore;
          expect(updatedStore.name).to.be.equal(storePayload.name);
          expect(updatedStore.category).to.be.equal(storePayload.category);
          expect(updatedStore.alias).to.be.equal(storePayload.alias);
          expect(updatedStore.description).to.be.equal(storePayload.description);
          expect(updatedStore.socialMedia).to.be.deep.equal(storePayload.socialMedia);
          expect(updatedStore.profileImage).to.be.not.null;
          expect(updatedStore.StoreLocations.length).to.be.equal(1);
          done();
        });
    });

    it('Update a store for a user without alias should throw error', function (done) {
      const storePayload = {
        name: faker.company.companyName(),
        category: 'bar',
        description: faker.company.catchPhraseDescriptor(),
        socialMedia: {
          facebook: 'https://www.facebook.com/fake/',
          whatsapp: 'https://wa.me/5491112341234',
          instagram: 'https://www.instagram.com/fake/',
        },
      };

      chai
        .request(apiUrl)
        .post('/graphql')
        .set('Authorization', `Bearer ${authToken}`)
        .send({
          query: employeeQueries.employeeModifyStore(this.store.dbStore.id),
          variables: {
            store: { ...storePayload },
          },
        })
        .end((err, res) => {
          invalidRequest(err, res, API_STORE_ERRORS['api/invalid-store-alias']);
          done();
        });
    });
  });
});
