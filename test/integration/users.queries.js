const createUser = user => {
  return `mutation{
    createUser(
      firstName: "${user.firstName}",
      lastName: "${user.lastName}",
      email: "${user.email}",
      birthDate: "${user.birthDate}",
      password: "${user.password}"
    ){
      loginToken
    }
  }`;
};

const createPartnerUser = user => {
  return `mutation{
    createPartnerUser(
      firstName: "${user.firstName}",
      lastName: "${user.lastName}",
      email: "${user.email}",
      birthDate: "${user.birthDate}",
      password: "${user.password}"
    ){
      loginToken
    }
  }`;
};

const findPendingUser = email => {
  return `query{
    findPendingUser(
      email: "${email}"
    ){
      isPending,
      firstName,
      lastName
    }
  }`;
};

const userInfo = () => {
  return `query{
    userInfo{
      id,
      email,
      alias
    }
  }`;
};

const isValidUser = () => {
  return `query{
    isValidUser
  }`;
};

const updateUser = user => {
  return `mutation{
    updateUser(
      firstName: "${user.firstName}",
      lastName: "${user.lastName}"
    ){
      id
    }
  }`;
};

const registerUserPushToken = token => {
  return `mutation{
    registerUserPushToken(
      pushToken: "${token}",
    ){
      id
      pushToken
    }
  }`;
};

module.exports = {
  createUser,
  createPartnerUser,
  userInfo,
  findPendingUser,
  isValidUser,
  updateUser,
  registerUserPushToken,
};
