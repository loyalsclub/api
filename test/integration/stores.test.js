const fs = require('fs');
const path = require('path');

const chai = require('chai');
const chaiHttp = require('chai-http');
const faker = require('faker');
const FormData = require('form-data');
const fetch = require('node-fetch');

const { COUNTRY_CODES, STORE_CATEGORIES, STORE_COUNTRIES } = require('@modules/store/constants/store.constants');

const { apiUrl, employeeToken, validRequest } = require('../utils/test.utils');

const storesQueries = require('./stores.queries');

const expect = chai.expect;
chai.use(chaiHttp);

const PREFERENCES = {
  auth: true,
};

describe('StoreAndCustomerCreation', () => {
  let authToken;

  before(async () => {
    if (!PREFERENCES.auth) return;
    authToken = await employeeToken();
  });

  it('Get user employeeprofiles', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: storesQueries.getEmployeeProfiles(),
      })
      .end((err, res) => {
        expect(res.body.data.userEmployeeProfiles).to.be.an('array');
        expect(res.body.data.userEmployeeProfiles[0].User).to.not.be.undefined;
        expect(res.body.data.userEmployeeProfiles[0].User.id).to.not.be.undefined;
        done();
      });
  });

  it('Get storeLevelsAndRewardsById', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: storesQueries.storeLevelsAndRewardsById(1),
      })
      .end((err, res) => {
        validRequest(err, res);

        for (const storeLevel of res.body.data.storeLevelsAndRewardsById) {
          expect(storeLevel).to.have.property('id');
          expect(storeLevel).to.have.property('level');
          expect(storeLevel).to.have.property('StoreRewards');

          for (const storeReward of storeLevel.StoreRewards) {
            expect(storeReward).to.have.property('id');
            expect(storeReward).to.have.property('title');
            expect(storeReward).to.have.property('isAvailable');
            expect(storeReward).to.have.property('lastTimeUsed');
            expect(storeReward).to.have.property('nextTimeAvailable');
            expect(storeReward).to.have.property('UsageFrequency');
            expect(storeReward.UsageFrequency).to.not.be.null;
          }
        }
        done();
      });
  });

  it('Create a store for a user', done => {
    const body = new FormData();

    const storePayload = {
      name: faker.company.companyName(),
      category: 'dress',
      description: faker.company.catchPhraseDescriptor(),
      level2: '100',
      level3: '200',
      countryCode: COUNTRY_CODES.MEXICO,
      StoreSetting: {
        personalPhone: '541167678888',
        spendingTimes: '9',
        spendingAmount: 300,
      },
    };

    body.append(
      'operations',
      JSON.stringify({
        query: storesQueries.createStoreForUser(),
        variables: {
          store: {
            ...storePayload,
            profileImage: null,
            StoreLocations: [
              {
                address: '1100 Princess Rd, Manchester M15 5AS, UK',
                geo: { type: 'Point', coordinates: [53.4614267, -2.246362099999942] },
              },
            ],
          },
        },
      }),
    );

    body.append('map', JSON.stringify({ 1: ['variables.store.profileImage'] }));

    body.append('1', fs.createReadStream(path.resolve(__dirname, '../utils/store-placeholder.png')));

    fetch(apiUrl + '/graphql', { method: 'POST', headers: { Authorization: `Bearer ${authToken}` }, body })
      .then(async res => ({ body: await res.json(), status: 200 }))
      .then(res => {
        validRequest(null, res);
        expect(res.body.data.createStoreForUser).to.be.not.empty;
        expect(res.body.data.createStoreForUser.alias).to.not.be.null;
        expect(res.body.data.createStoreForUser.alias).to.not.be.undefined;
        expect(res.body.data.createStoreForUser.countryCode).to.be.equal(COUNTRY_CODES.MEXICO);
        done();
      });
  });

  it('Generate valid store alias', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: storesQueries.generateValidStoreAlias('pizzeria gomez'),
      })
      .end((err, res) => {
        expect(res.body.data.generateValidStoreAlias).to.be.an('string');
        expect(res.body.data.generateValidStoreAlias).to.be.equal('pizzeria.gomez1');
        done();
      });
  });

  it('Get store categories', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: storesQueries.storeCategories(),
      })
      .end((err, res) => {
        expect(res.body.data.storeCategories).to.be.an('array');
        expect(res.body.data.storeCategories).to.be.deep.equal(STORE_CATEGORIES);
        done();
      });
  });

  it('Get store countries', done => {
    chai
      .request(apiUrl)
      .post('/graphql')
      .set('Authorization', `Bearer ${authToken}`)
      .send({
        query: storesQueries.storeCountries(),
      })
      .end((err, res) => {
        expect(res.body.data.storeCountries).to.be.an('array');
        expect(res.body.data.storeCountries).to.be.deep.equal(STORE_COUNTRIES);
        done();
      });
  });
});
