const adminCreateStoreForUser = userId => {
  return `mutation($store: CreateStoreInput!){
    adminCreateStoreForUser(userId: "${userId}", store: $store) {
      id
      countryCode
      StoreLocations {
        id
      }
    }
  }`;
};

const adminModifyStore = storeId => {
  return `mutation($store: CreateStoreInput!){
    adminModifyStore(storeId: "${storeId}", store: $store) {
      id
      name
      alias
      category
      description
      socialMedia
      StoreLocations {
        id
      }
    }
  }`;
};

const adminRemoveStore = storeId => {
  return `mutation {
    adminRemoveStore(storeId: "${storeId}") {
      id
    }
  }`;
};

const adminListStores = () => {
  return `query{
    adminListStores{
      id
      name
      alias
      CustomerProfiles {
        id
      }
      EmployeeProfiles {
        id
      }
      StoreLocations {
        id
        website
        address
        email
        phone
      }
    }
  }`;
};

const adminGetStore = storeId => {
  return `query{
    adminGetStore(storeId: "${storeId}") {
      id
      name
      alias
      CustomerProfiles {
        id
        User {
          id
        }
      }
      EmployeeProfiles {
        id
        User {
          id
        }
      }
      StoreLocations {
        id
        website
        address
        email
        phone
      }
      StoreRewards {
        id
        StoreRewardUsages {
          id
        }
      }
    }
  }`;
};

const adminAddStoreReward = () => {
  return `mutation($storeReward: CreateStoreRewardInput!){
    adminAddStoreReward(storeReward: $storeReward) {
      id
      title
      description
      UsageFrequency {
        id
      }
    }
  }`;
};

const adminModifyStoreReward = storeRewardId => {
  return `mutation($storeReward: CreateStoreRewardInput!){
    adminModifyStoreReward(
      id: "${storeRewardId}"
      storeReward: $storeReward
    ) {
      id
    }
  }`;
};

const adminRemoveStoreReward = ({ rewardId }) => {
  return `mutation{
    adminRemoveStoreReward(rewardId: "${rewardId}")
  }`;
};

const adminGetStatistics = () => {
  return `query{
    adminGetStatistics{
      totalUsers
      totalStores
      totalCustomers
      totalLevel1
      totalLevel2
      totalLevel3
      totalLoadPointsOperations
    }
  }`;
};

const adminCreateCouponForStore = payload => {
  return `mutation{
    adminCreateCouponForStore(
      couponCode: "${payload.couponCode}"
      storeId: "${payload.storeId}"
      points: ${payload.points}
      ) {
        id
        status
      }
    }`;
};

const adminModifyCoupon = ({ id, status }) => {
  return `mutation{
    adminModifyCoupon(
      id: ${id}
      status: ${status}
    ){
      id
      status
    }
  }`;
};

const adminGetCoupons = (storeId = null) => {
  return `query{
    adminGetCoupons(storeId: ${storeId}){
      storeId,
      totalCount,
      pageInfo {
        endCursor,
        startCursor,
        hasNextPage,
      },
      edges {
        cursor,
        node {
          id
        },
      }
    }
  }`;
};

module.exports = {
  adminListStores,
  adminGetStore,
  adminCreateStoreForUser,
  adminModifyStore,
  adminRemoveStore,
  adminAddStoreReward,
  adminModifyStoreReward,
  adminRemoveStoreReward,
  adminGetStatistics,
  adminCreateCouponForStore,
  adminModifyCoupon,
  adminGetCoupons,
};
