const sleep = require('sleep');

const admin = require('@services/authentication/authentication.service');

const adminUsers = ['nocetti.tomas@gmail.com', 'graziano.ramiro@gmail.com', 'nicolasokroglic@gmail.com'];
const testingUsers = [
  'nocetti.tomas+pizzeriagomez@gmail.com',
  'nocetti.tomas+heladeriasabor@gmail.com',
  'nocetti.tomas+cafeloshermanos@gmail.com',
  'nocetti.tomas+decoracionluna@gmail.com',
  'nocetti.tomas+customer@gmail.com',
  'graziano.ramiro+customer@gmail.com',
  'nicolasokroglic+customer@gmail.com',
];

const clearUsersFirebase = async () => {
  const listUsersResult = await admin.auth().listUsers(1000);
  const promises = [];

  listUsersResult.users.forEach(({ email, uid }) => {
    const isAdminUser = adminUsers.includes(email);
    const isTestUser = testingUsers.includes(email);

    if (!isAdminUser && !isTestUser) {
      sleep.msleep(150); // Max
      promises.push(admin.auth().deleteUser(uid));
    }
  });
  await Promise.all(promises);
};

(async () => {
  await clearUsersFirebase();
  process.exit(0);
})();
