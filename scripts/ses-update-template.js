require('dotenv').config({ path: '../api.env' });

const { readdirSync } = require('fs');
const path = require('path');
const readline = require('readline');

const AWS = require('aws-sdk');

const getDirectories = source =>
  readdirSync(source, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name);

AWS.config.update({ region: 'sa-east-1' });

const ses = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const dirs = getDirectories(path.resolve(__dirname, '../emails/templates'));

dirs.forEach((value, index) => console.log(`${index}) ${value}`));

rl.question('\n Choose one of the options above: ', function (answer) {
  const templateName = dirs[answer];

  if (!templateName) {
    console.error('Invalid option');
    rl.close();
    return;
  }

  let email;

  try {
    email = require(path.resolve(__dirname, '../emails/templates', templateName));
  } catch (err) {
    console.error('Template not found');
    rl.close();
    return;
  }

  ses.getTemplate({ TemplateName: email.TemplateName }, function (err, response) {
    const operation = response ? ses.updateTemplate : ses.createTemplate;

    operation.call(ses, { Template: email }, function (err, res) {
      console.log({ err, res });
      rl.close();
    });
  });
});
