require('dotenv').config({ path: '../api.env' });

var AWS = require('aws-sdk');
AWS.config.update({ region: 'sa-east-1' });

const S3 = new AWS.S3();
const FOLDERS = ['store-profile-images/', 'qr-codes/'];

const clearFolderContent = async ({ params, prefix }) => {
  return new Promise((succ, error) => {
    S3.listObjects({ ...params, Prefix: prefix }, function (err, data) {
      if (err) return error(err); // an error occurred

      if (data.Contents.length == 0) return;

      params.Delete = { Objects: [] };

      data.Contents.forEach(function (content) {
        params.Delete.Objects.push({ Key: content.Key });
      });

      S3.deleteObjects(params, function (err) {
        if (err) return error(err, err.stack); // an error occurred
        // if(data.Contents.length == 1000) emptyBucket(bucketName,callback);
        succ();
      });
    });
  });
};

const clearImagesDatabase = async () => {
  var params = {
    Bucket: process.env.AWS_FILES_BUCKET,
  };

  const promises = [];
  FOLDERS.forEach(function (folderName) {
    promises.push(clearFolderContent({ params, prefix: folderName }));
  });

  await Promise.all(promises);
};

(async () => {
  await clearImagesDatabase();
  process.exit(0);
})();
