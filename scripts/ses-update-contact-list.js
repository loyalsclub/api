require('dotenv').config({ path: '../api.env' });

const path = require('path');
const readline = require('readline');

const AWS = require('aws-sdk');

AWS.config.update({ region: 'sa-east-1' });

const sesV2 = new AWS.SESV2({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let config;

try {
  config = require(path.resolve(__dirname, '../emails/contact-lists/config'));
} catch (err) {
  console.error('Contact List not found');
  throw err;
}

console.log(config);

rl.question('Are you sure you want to update the contact list with the config above (y/n):', function (answer) {
  if (answer.toLocaleLowerCase() === 'y') {
    console.log('Updating...');

    sesV2.getContactList({ ContactListName: config.ContactListName }, function (err, response) {
      const operation = response ? sesV2.updateContactList : sesV2.createContactList;

      operation.call(sesV2, config, function (err, res) {
        console.log({ err, res });
        rl.close();
      });
    });
  } else {
    console.log('Bye bye');
    rl.close();
  }
});
