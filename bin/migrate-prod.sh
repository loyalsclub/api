#!/bin/bash

npx sequelize db:migrate:status --env production

printf "\n"

read -p "Are you sure you want to migrate to production? (y/n): " SHOULD_MIGRATE

if [ "$SHOULD_MIGRATE" = "y" ]; then
  printf "Got it. Migrating.... \n\n"

  npx sequelize db:migrate --env production

  printf "Done! Good luck buddy."
else
  echo "Ok."
fi
