#!/bin/bash

read -p "Are you sure you want to seed production? (y/n): " SHOULD_SEED

if [ "$SHOULD_SEED" = "y" ]; then
  printf "Got it. Seeding.... \n\n"

  sequelize db:seed:all --env production --seeders-path database/seeders/production

  printf "Done! Good luck buddy."
else
  echo "Ok."
fi
