#!/bin/bash

COMMIT_HASH=$(git rev-parse --short HEAD)

echo "Appending commit $COMMIT_HASH..."

git status

printf "\n"

read -p "Are you sure you want to deploy to production? (y/n): " SHOULD_DEPLOY

if [ "$SHOULD_DEPLOY" = "y" ]; then
    read -p "Deploy message?: " MESSAGE

    if [ -n "$MESSAGE" ]; then
        printf "Got it. Deploying.... \n\n"

        eb deploy prod -m "$MESSAGE - $COMMIT_HASH"

        printf "Done! Good luck buddy."
    else
        printf "Please, message cannot be empty."
    fi
else
    echo "Ok."
fi
