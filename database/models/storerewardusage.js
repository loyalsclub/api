'use strict';
module.exports = (sequelize, DataTypes) => {
  const StoreRewardUsage = sequelize.define('StoreRewardUsage', {
    storeRewardId: DataTypes.INTEGER,
    customerId: DataTypes.INTEGER,
    employeeId: DataTypes.INTEGER,
  });

  StoreRewardUsage.associate = function (models) {
    StoreRewardUsage.belongsTo(models.StoreReward, { foreignKey: 'storeRewardId' });
    StoreRewardUsage.belongsTo(models.CustomerProfile, { foreignKey: 'customerId' });
    StoreRewardUsage.belongsTo(models.EmployeeProfile, { foreignKey: 'employeeId' });
  };

  return StoreRewardUsage;
};
