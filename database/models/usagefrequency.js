'use strict';
module.exports = (sequelize, DataTypes) => {
  const UsageFrequency = sequelize.define('UsageFrequency', {
    type: DataTypes.STRING,
    value: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  });

  UsageFrequency.associate = function (models) {
    UsageFrequency.hasOne(models.Coupon, { foreignKey: 'usageFrequencyId' });
  };

  return UsageFrequency;
};
