'use strict';
module.exports = (sequelize, DataTypes) => {
  const CustomerProfile = sequelize.define(
    'CustomerProfile',
    {
      userId: DataTypes.INTEGER,
      status: {
        type: DataTypes.ENUM('active', 'pending', 'deleted'),
        defaultValue: 'pending',
      },
      cardId: DataTypes.STRING,
      storeId: DataTypes.INTEGER,
      deletedAt: DataTypes.DATE,
    },
    {
      paranoid: true,
    },
  );

  CustomerProfile.associate = function (models) {
    CustomerProfile.belongsTo(models.User, { foreignKey: 'userId' });
    CustomerProfile.belongsTo(models.Store, { foreignKey: 'storeId' });
    CustomerProfile.hasMany(models.Point, { foreignKey: 'customerId' });
    CustomerProfile.hasOne(models.CustomerLevel, { foreignKey: 'customerId' });
  };

  return CustomerProfile;
};
