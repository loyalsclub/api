'use strict';
module.exports = (sequelize, DataTypes) => {
  const StoreReward = sequelize.define(
    'StoreReward',
    {
      storeId: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      storeLevelId: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      description: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      usageFrequencyId: DataTypes.INTEGER,
    },
    {},
  );

  StoreReward.addHook('beforeDestroy', 'destroyUsageFrequency', storeReward => {
    const removeUsageFrequency = sequelize.models.UsageFrequency.destroy({
      where: {
        id: storeReward.usageFrequencyId,
      },
    });

    const removeStoreRewardUsage = sequelize.models.StoreRewardUsage.destroy({
      where: {
        storeRewardId: storeReward.id,
      },
    });

    return Promise.all([removeUsageFrequency, removeStoreRewardUsage]);
  });

  StoreReward.associate = function (models) {
    StoreReward.belongsTo(models.Store, { foreignKey: 'storeId' });
    StoreReward.belongsTo(models.StoreLevel, { foreignKey: 'storeLevelId' });
    StoreReward.belongsTo(models.UsageFrequency, { foreignKey: 'usageFrequencyId' });
    StoreReward.hasMany(models.StoreRewardUsage, { foreignKey: 'storeRewardId' });
  };

  StoreReward.loadScopes = function (models) {
    StoreReward.addScope('defaultScope', {
      include: [{ model: models.UsageFrequency }],
    });
  };

  return StoreReward;
};
