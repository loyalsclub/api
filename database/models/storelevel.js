'use strict';

const { USAGE_FREQUENCIES } = require('../../src/modules/core/constants/core.constants');

const exampleStoreRewards = [
  {
    title: 'Llevate un producto B de Regalo',
    description: 'Comprando dos productos A',
    usageFrequency: {
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
    },
  },
  {
    title: '$100 de bonificación para tu próxima compra',
    description: 'En consumos mayores a $1250',
    usageFrequency: {
      type: USAGE_FREQUENCIES.ONE_TIME,
      value: null,
    },
  },
  {
    title: '10% de descuento',
    description: 'Todos los Martes y Jueves',
    usageFrequency: {
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2,4',
    },
  },
];

module.exports = (sequelize, DataTypes) => {
  var StoreLevel = sequelize.define(
    'StoreLevel',
    {
      storeId: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      level: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      points: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
    },
    {},
  );

  StoreLevel.associate = function (models) {
    StoreLevel.belongsTo(models.Store, { foreignKey: 'storeId' });
    StoreLevel.hasMany(models.StoreReward, { foreignKey: 'storeLevelId' });
  };

  StoreLevel.addHook('beforeDestroy', 'destroyStoreRewards', storeLevel => {
    return sequelize.models.StoreReward.destroy({
      where: { storeLevelId: storeLevel.id },
      individualHooks: true,
    });
  });

  StoreLevel.addHook('afterCreate', 'createExampleReward', storeLevel => {
    const exampleReward = exampleStoreRewards[storeLevel.level - 1];

    return sequelize.models.UsageFrequency.create(exampleReward.usageFrequency).then(usageFrequency =>
      sequelize.models.StoreReward.create({
        storeLevelId: storeLevel.id,
        title: `Ejemplo ${storeLevel.level}: ${exampleReward.title}`,
        description: exampleReward.description,
        storeId: storeLevel.storeId,
        usageFrequencyId: usageFrequency.id,
      }),
    );
  });

  StoreLevel.loadScopes = function (models) {
    StoreLevel.addScope('defaultScope', {
      order: [
        ['level', 'ASC'],
        [{ model: models.StoreReward }, 'title', 'ASC'],
      ],
      include: [{ model: models.StoreReward }],
    });
  };

  return StoreLevel;
};
