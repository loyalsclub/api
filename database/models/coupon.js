'use strict';
module.exports = (sequelize, DataTypes) => {
  const Coupon = sequelize.define(
    'Coupon',
    {
      couponCode: DataTypes.STRING,
      storeId: DataTypes.INTEGER,
      status: DataTypes.STRING,
      points: DataTypes.INTEGER,
      usageFrequencyId: DataTypes.INTEGER,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {},
  );

  Coupon.associate = function (models) {
    Coupon.belongsTo(models.Store, { foreignKey: 'storeId' });
    Coupon.belongsTo(models.UsageFrequency, { foreignKey: 'usageFrequencyId' });
    Coupon.hasOne(models.Point, { foreignKey: 'couponId' });
  };

  Coupon.loadScopes = function (models) {
    Coupon.addScope('defaultScope', {
      include: [{ model: models.UsageFrequency }],
    });
  };

  return Coupon;
};
