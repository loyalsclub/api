'use strict';
module.exports = (sequelize, DataTypes) => {
  const StoreSetting = sequelize.define(
    'StoreSetting',
    {
      storeId: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      personalPhone: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      spendingTimes: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      spendingAmount: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
    },
    {},
  );

  StoreSetting.associate = function (models) {
    StoreSetting.belongsTo(models.Store, { foreignKey: 'storeId' });
  };

  return StoreSetting;
};
