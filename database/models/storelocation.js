'use strict';
module.exports = (sequelize, DataTypes) => {
  const StoreLocation = sequelize.define(
    'StoreLocation',
    {
      storeId: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      website: DataTypes.STRING,
      phone: DataTypes.STRING,
      email: DataTypes.STRING,
      address: DataTypes.STRING,
      geo: DataTypes.GEOMETRY,
    },
    {},
  );

  StoreLocation.associate = function () {};

  return StoreLocation;
};
