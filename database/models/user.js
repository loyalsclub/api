'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      uid: { type: DataTypes.STRING, unique: true, defaultValue: null },
      role: { type: DataTypes.ENUM('user', 'admin'), defaultValue: 'user' },
      birthDate: { type: DataTypes.DATEONLY },
      email: { type: DataTypes.STRING, unique: true },
      alias: { type: DataTypes.STRING, unique: true },
      pushToken: DataTypes.STRING,
      webPushSubscription: DataTypes.JSON,
    },
    {
      hooks: {
        beforeValidate: async user => {
          user.alias = await User.getAvailableAlias();
        },
      },
    },
  );

  User.getAvailableAlias = async () => {
    let lastUser = await User.findOne({
      order: [['alias', 'DESC']],
      raw: true,
    });
    const alias = Number(lastUser.alias) + 1;
    return alias;
  };

  User.associate = function (models) {
    User.hasMany(models.CustomerProfile, { foreignKey: 'userId' });
  };

  User.addHook('beforeSave', 'lowerCaseEmail', user => {
    user.email = user.email.toLowerCase();
  });

  return User;
};
