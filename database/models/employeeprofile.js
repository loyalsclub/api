'use strict';
module.exports = (sequelize, DataTypes) => {
  const EmployeeProfile = sequelize.define(
    'EmployeeProfile',
    {
      userId: DataTypes.INTEGER,
      storeId: DataTypes.INTEGER,
      permissionId: DataTypes.INTEGER,
    },
    {},
  );

  EmployeeProfile.associate = function (models) {
    EmployeeProfile.belongsTo(models.User, { foreignKey: 'userId' });
    EmployeeProfile.belongsTo(models.Store, { foreignKey: 'storeId' });
  };

  return EmployeeProfile;
};
