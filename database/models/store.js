'use strict';

const { STORE_COUNTRIES } = require('../../src/modules/store/constants/store.constants');

module.exports = (sequelize, DataTypes) => {
  const Store = sequelize.define(
    'Store',
    {
      name: DataTypes.STRING,
      alias: { type: DataTypes.STRING, unique: true },
      category: DataTypes.STRING,
      profileImage: {
        type: DataTypes.STRING(2083),
        get() {
          const imageRequest = JSON.stringify({
            bucket: process.env.AWS_FILES_BUCKET,
            key: this.getDataValue('profileImage'),
            edits: {
              resize: {
                width: 288,
                height: 288,
                fit: 'fill',
              },
            },
          });

          return `${process.env.IMAGES_ABSOLUTE_PATH}${Buffer.from(imageRequest).toString('base64')}`;
        },
      },
      description: DataTypes.TEXT,
      socialMedia: DataTypes.JSON,
      countryCode: DataTypes.CHAR(2),
      countryAreaCode: {
        type: DataTypes.VIRTUAL,
        get() {
          const storeCountry = STORE_COUNTRIES.find(country => country.name === this.getDataValue('countryCode'));
          return (storeCountry && storeCountry.areaCode) || '';
        },
        set() {
          throw new Error('Do not try to set the `countryAreaCode` value!');
        },
      },
      private: DataTypes.BOOLEAN,
      fixedLevels: DataTypes.BOOLEAN,
      cardModule: DataTypes.BOOLEAN,
    },
    {},
  );

  Store.addHook('beforeDestroy', 'destroyDependencies', store => {
    const removeStoreLocation = sequelize.models.StoreLocation.destroy({
      where: { storeId: store.id },
      individualHooks: true,
    });

    const removeStoreLevels = sequelize.models.StoreLevel.destroy({
      where: { storeId: store.id },
      individualHooks: true,
    });

    const removeStoreSettings = sequelize.models.StoreSetting.destroy({
      where: { storeId: store.id },
    });

    const removeEmployeeProfiles = sequelize.models.EmployeeProfile.destroy({
      where: { storeId: store.id },
    });

    const removeCustomerProfiles = sequelize.models.CustomerProfile.destroy({
      where: { storeId: store.id },
    });

    const removeCoupons = sequelize.models.Coupon.destroy({
      where: { storeId: store.id },
    });

    const removePoints = sequelize.models.Point.destroy({
      where: { storeId: store.id },
    });

    return Promise.all([
      removeStoreLocation,
      removeStoreLevels,
      removeStoreSettings,
      removeEmployeeProfiles,
      removeCustomerProfiles,
      removeCoupons,
      removePoints,
    ]);
  });

  Store.associate = function (models) {
    Store.hasMany(models.EmployeeProfile, { foreignKey: 'storeId' });
    Store.hasMany(models.CustomerProfile, { foreignKey: 'storeId' });
    Store.hasMany(models.StoreLocation, { foreignKey: 'storeId' });
    Store.hasMany(models.StoreLevel, { foreignKey: 'storeId' });
    Store.hasMany(models.StoreReward, { foreignKey: 'storeId' });
    Store.hasOne(models.StoreSetting, { foreignKey: 'storeId' });
    Store.hasOne(models.Coupon, { foreignKey: 'storeId' });
  };

  return Store;
};
