'use strict';
module.exports = (sequelize, DataTypes) => {
  const Point = sequelize.define(
    'Point',
    {
      storeId: DataTypes.INTEGER,
      customerId: DataTypes.INTEGER,
      employeeId: DataTypes.INTEGER,
      couponId: DataTypes.INTEGER,
      amount: DataTypes.INTEGER,
    },
    {},
  );

  Point.associate = function (models) {
    Point.belongsTo(models.CustomerProfile, { foreignKey: 'customerId' });
    Point.belongsTo(models.EmployeeProfile, { foreignKey: 'employeeId' });
    Point.belongsTo(models.Coupon, { foreignKey: 'couponId' });
  };

  return Point;
};
