'use strict';
module.exports = (sequelize, DataTypes) => {
  const CustomerLevel = sequelize.define(
    'CustomerLevel',
    {
      customerId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      storeId: DataTypes.STRING,
      level: DataTypes.INTEGER,
      nextLevelPointsDif: DataTypes.INTEGER,
      currentLevelPoints: DataTypes.INTEGER,
      nextLevelPoints: DataTypes.INTEGER,
      activeRewards: DataTypes.INTEGER,
      total: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
      timestamps: false,
    },
  );
  return CustomerLevel;
};
