'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'EmployeeProfiles',
      [
        {
          userId: 4,
          storeId: 1,
          permissionId: 1,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 5,
          storeId: 2,
          permissionId: 1,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 6,
          storeId: 3,
          permissionId: 1,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 7,
          storeId: 4,
          permissionId: 1,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('EmployeeProfiles', null, {});
  },
};
