'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'StoreSettings',
      [
        {
          storeId: 1,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          storeId: 2,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          storeId: 3,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          storeId: 4,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('StoreSettings', null, {});
  },
};
