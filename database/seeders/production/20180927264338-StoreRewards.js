'use strict';

function getPizzeriaGomezRewards() {
  return [
    {
      storeId: 1,
      storeLevelId: 1,
      title: 'Comprá 2 pizzas y te llevás una gaseosa gratis',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 1,
      title: 'Comprá una docena de empanadas y llevate 2 extra de regalo',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 2,
      title: 'Comprá 2 pizzas y te llevás dos gaseosas gratis',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 3,
      title: 'Comprá una docena de empanadas y llevate 2 extra de regalo y una gaseosa gratis',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getHeladeriaSaborRewards() {
  return [
    {
      storeId: 2,
      storeLevelId: 4,
      title: '15% de descuento para compras mayores a $700',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 4,
      title: 'Gastando $500 te ofrecemos un 5% de descuento',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 5,
      title: '15% de descuento para compras mayores a $600',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 5,
      title: 'Gastando $500 te ofrecemos un 10% de descuento',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 6,
      title: '15% de descuento para compras mayores a $500',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 6,
      title: 'Gastando $500 te ofrecemos un 15% de descuento',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getCafeLosHermanosRewards() {
  return [
    {
      storeId: 3,
      storeLevelId: 7,
      title: '15% de descuento los Martes al Mediodía',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 3,
      storeLevelId: 8,
      title: '15% de descuento los Martes y Miércoles al Mediodía.',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 3,
      storeLevelId: 9,
      title: '15% de descuento los Martes, Miércoles y Jueves al Mediodía',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getDecoracionLunaRewards() {
  return [
    {
      storeId: 4,
      storeLevelId: 10,
      title: '10% de descuento en compras superiores a $750.',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 4,
      storeLevelId: 11,
      title: 'Comprando 2 lámparas te llevás un cuadro de regalo.',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 4,
      storeLevelId: 12,
      title: '15% de descuento en compras superiores a $750.',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'StoreRewards',
      [
        ...getPizzeriaGomezRewards(),
        ...getHeladeriaSaborRewards(),
        ...getCafeLosHermanosRewards(),
        ...getDecoracionLunaRewards(),
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('StoreRewards', null, {});
  },
};
