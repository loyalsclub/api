'use strict';

function getPizzeriaGomezLevels() {
  return [
    {
      id: 1,
      storeId: 1,
      level: 1,
      points: 0,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 2,
      storeId: 1,
      level: 2,
      points: 350,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 3,
      storeId: 1,
      level: 3,
      points: 800,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getHeladeriaSaborLevels() {
  return [
    {
      id: 4,
      storeId: 2,
      level: 1,
      points: 0,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 5,
      storeId: 2,
      level: 2,
      points: 250,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 6,
      storeId: 2,
      level: 3,
      points: 600,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getCafeLosHermanosLevels() {
  return [
    {
      id: 7,
      storeId: 3,
      level: 1,
      points: 0,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 8,
      storeId: 3,
      level: 2,
      points: 300,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 9,
      storeId: 3,
      level: 3,
      points: 700,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getDecoracionLunaLevels() {
  return [
    {
      id: 10,
      storeId: 4,
      level: 1,
      points: 0,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 11,
      storeId: 4,
      level: 2,
      points: 400,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 12,
      storeId: 4,
      level: 3,
      points: 1000,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getTuComercioLevels() {
  return [
    {
      id: 13,
      storeId: 6,
      level: 1,
      points: 0,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 14,
      storeId: 6,
      level: 2,
      points: 997,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 15,
      storeId: 6,
      level: 3,
      points: 2500,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getComercioTestsLevels() {
  return [
    {
      id: 16,
      storeId: 7,
      level: 1,
      points: 0,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 17,
      storeId: 7,
      level: 2,
      points: 997,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 18,
      storeId: 7,
      level: 3,
      points: 2500,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'StoreLevels',
      [
        ...getPizzeriaGomezLevels(),
        ...getHeladeriaSaborLevels(),
        ...getCafeLosHermanosLevels(),
        ...getDecoracionLunaLevels(),
        ...getTuComercioLevels(),
        ...getComercioTestsLevels(),
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('StoreLevels', null, {});
  },
};
