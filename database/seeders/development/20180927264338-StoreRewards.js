'use strict';

function getPizzeriaGomezRewards() {
  return [
    {
      storeId: 1,
      storeLevelId: 1,
      title: '1 Gaseosa de Regalo',
      description: 'Comprando tres pizzas',
      usageFrequencyId: 1,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 1,
      title: '2 latas de Cerveza de Regalo',
      description: 'Con la compra de una docena de empanadas',
      usageFrequencyId: 2,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 1,
      title: 'Bienvenido ! Te regalamos 1 Gaseosa',
      description: 'En tu próxima compa',
      usageFrequencyId: 20,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 2,
      title: '1 Gaseosa y 1 Cerveza de Regalo',
      description: 'Comprando 3 pizzas',
      usageFrequencyId: 3,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 2,
      title: 'Te regalamos 2 latas de cerveza',
      description: 'Con la compra de una docena de empanadas',
      usageFrequencyId: 4,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 3,
      title: '1 Gaseosa y 1 Cerveza de Regalo',
      description: 'Comprando 3 pizzas grandes',
      usageFrequencyId: 5,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 1,
      storeLevelId: 3,
      title: 'Llevate 2 empanadas a elección y 2 latas de cerveza de Regalo',
      description: 'Con la compra de una docena de empanadas',
      usageFrequencyId: 6,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getHeladeriaSaborRewards() {
  return [
    {
      storeId: 2,
      storeLevelId: 4,
      title: '15% de descuento',
      description: 'En compras mayores a $1200',
      usageFrequencyId: 7,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 4,
      title: 'Bienvenido ! Llevate 1 Cucurucho de Regalo',
      description: 'Con tu próxima compra',
      usageFrequencyId: 21,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 5,
      title: '15% de descuento',
      description: 'En compras mayores a $900',
      usageFrequencyId: 8,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 2,
      storeLevelId: 6,
      title: '15% de descuento',
      description: 'En compras mayores a $600',
      usageFrequencyId: 9,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getCafeLosHermanosRewards() {
  return [
    {
      storeId: 3,
      storeLevelId: 7,
      title: '10% de descuento',
      description: 'Martes al Mediodía',
      usageFrequencyId: 10,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 3,
      storeLevelId: 7,
      title: 'Bienvenido ! Llevate 1 Tostado de Jamón y Queso de Regalo',
      description: 'Con tu próxima compra',
      usageFrequencyId: 22,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 3,
      storeLevelId: 8,
      title: '15% de descuento',
      description: 'Martes y Miércoles al Mediodía',
      usageFrequencyId: 11,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 3,
      storeLevelId: 9,
      title: '15% de descuento',
      description: 'Martes y Miércoles al Mediodía',
      usageFrequencyId: 12,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 3,
      storeLevelId: 9,
      title: '10% de descuento en merienda',
      description: 'Jueves todo el día',
      usageFrequencyId: 13,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getDecoracionLunaRewards() {
  return [
    {
      storeId: 4,
      storeLevelId: 10,
      title: '6 cuotas sin interés',
      description: 'En lámparas y veladores',
      usageFrequencyId: 14,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 4,
      storeLevelId: 11,
      title: '6 cuotas sin interés',
      description: 'En lámparas y veladores',
      usageFrequencyId: 15,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 4,
      storeLevelId: 11,
      title: '10% de Descuento',
      description: 'En artículos de baño',
      usageFrequencyId: 16,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 4,
      storeLevelId: 12,
      title: '6 cuotas sin interés',
      description: 'En lámparas y veladores',
      usageFrequencyId: 17,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 4,
      storeLevelId: 12,
      title: '10% de Descuento',
      description: 'En artículos de baño',
      usageFrequencyId: 18,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 4,
      storeLevelId: 12,
      title: '3x2: Compra 2 y llevate 3',
      description: 'En productos aromáticos',
      usageFrequencyId: 19,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getTuComercioRewards() {
  return [
    {
      storeId: 6,
      storeLevelId: 13,
      title: '10% de descuento',
      description: 'Todos los Martes y Jueves',
      usageFrequencyId: 23,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 6,
      storeLevelId: 14,
      title: 'Llevate un producto B de Regalo',
      description: 'Comprando dos productos A',
      usageFrequencyId: 24,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 6,
      storeLevelId: 15,
      title: '$100 de bonificación para tu próxima compra',
      description: 'En consumos mayores a $1250',
      usageFrequencyId: 25,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getComercioTestsRewards() {
  return [
    {
      storeId: 7,
      storeLevelId: 16,
      title: '10% de descuento',
      description: 'Todos los Martes y Jueves',
      usageFrequencyId: 26,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 7,
      storeLevelId: 17,
      title: 'Llevate un producto B de Regalo',
      description: 'Comprando dos productos A',
      usageFrequencyId: 27,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      storeId: 7,
      storeLevelId: 18,
      title: '$100 de bonificación para tu próxima compra',
      description: 'En consumos mayores a $1250',
      usageFrequencyId: 28,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'StoreRewards',
      [
        ...getPizzeriaGomezRewards(),
        ...getHeladeriaSaborRewards(),
        ...getCafeLosHermanosRewards(),
        ...getDecoracionLunaRewards(),
        ...getTuComercioRewards(),
        ...getComercioTestsRewards(),
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('StoreRewards', null, {});
  },
};
