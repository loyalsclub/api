'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'Coupons',
      [
        {
          id: 1,
          couponCode: 'avaso7',
          storeId: 1,
          status: 'active',
          points: 200,
          createdAt: '2020-05-25 13:00:00',
          updatedAt: '2020-05-25 13:00:00',
        },
        {
          id: 2,
          couponCode: 'pamon9',
          storeId: 1,
          status: 'paused',
          points: 500,
          createdAt: '2020-05-25 13:00:00',
          updatedAt: '2020-05-25 13:00:00',
        },
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('Coupons', null, {});
  },
};
