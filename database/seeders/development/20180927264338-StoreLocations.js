'use strict';

function getPizzeriaGomezLocation(Sequelize) {
  return {
    storeId: 1,
    website: 'https://pizzeriagomez.com.ar',
    phone: '+1123310528',
    email: 'pizzeriagomez@gmail.com',
    address: 'Av. Gral. Mosconi 3401',
    geo: Sequelize.fn('GeomFromText', 'POINT(-34.5905491 -58.5114169)'),
    createdAt: '2020-08-01 00:00:00',
    updatedAt: '2020-08-01 00:00:00',
  };
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('StoreLocations', [getPizzeriaGomezLocation(Sequelize)], {});
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('StoreLocations', null, {});
  },
};
