'use strict';
const { USAGE_FREQUENCIES } = require('../../../src/modules/core/constants/core.constants');

function getPizzeriaGomezUsageFrequencies() {
  return [
    {
      id: 1,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 2,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 20,
      type: USAGE_FREQUENCIES.ONE_TIME,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 3,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 4,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2,4',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 5,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 6,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getHeladeriaSaborUsageFrequencies() {
  return [
    {
      id: 7,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '1,3',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 21,
      type: USAGE_FREQUENCIES.ONE_TIME,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 8,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '1,2,3,4,5',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 9,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getCafeLosHermanosUsageFrequencies() {
  return [
    {
      id: 10,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 22,
      type: USAGE_FREQUENCIES.ONE_TIME,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 11,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2,3',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 12,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2,3',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 13,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '4',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getDecoracionLunaUsageFrequencies() {
  return [
    {
      id: 14,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '1,3',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 15,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '1,2,3,4',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 16,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '4,5',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 17,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 18,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 19,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '3,4',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getTuComercioUsageFrequencies() {
  return [
    {
      id: 23,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2,4',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 24,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 25,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

function getComercioTestsUsageFrequencies() {
  return [
    {
      id: 26,
      type: USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
      value: '2,4',
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 27,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
    {
      id: 28,
      type: USAGE_FREQUENCIES.FOREVER,
      value: null,
      createdAt: '2020-08-01 00:00:00',
      updatedAt: '2020-08-01 00:00:00',
    },
  ];
}

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'UsageFrequencies',
      [
        ...getPizzeriaGomezUsageFrequencies(),
        ...getHeladeriaSaborUsageFrequencies(),
        ...getCafeLosHermanosUsageFrequencies(),
        ...getDecoracionLunaUsageFrequencies(),
        ...getTuComercioUsageFrequencies(),
        ...getComercioTestsUsageFrequencies(),
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('UsageFrequencies', null, {});
  },
};
