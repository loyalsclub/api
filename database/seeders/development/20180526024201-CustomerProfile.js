'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'CustomerProfiles',
      [
        {
          userId: 8,
          storeId: 1,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 8,
          storeId: 2,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 9,
          storeId: 1,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 9,
          storeId: 2,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 10,
          storeId: 1,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 10,
          storeId: 2,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 10,
          storeId: 3,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          userId: 10,
          storeId: 4,
          status: 'active',
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('CustomerProfiles', null, {});
  },
};
