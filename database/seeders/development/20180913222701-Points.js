'use strict';

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'Points',
      [
        {
          storeId: 1,
          customerId: 1,
          employeeId: 1,
          couponId: null,
          amount: 1500,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          storeId: 2,
          customerId: 2,
          employeeId: null,
          couponId: 1,
          amount: 1000,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
        {
          storeId: 1,
          customerId: 3,
          employeeId: null,
          couponId: 1,
          amount: 500,
          createdAt: '2020-08-01 00:00:00',
          updatedAt: '2020-08-01 00:00:00',
        },
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('Points', null, {});
  },
};
