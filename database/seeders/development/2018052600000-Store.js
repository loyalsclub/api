'use strict';

const { COUNTRY_CODES } = require('../../../src/modules/store/constants/store.constants');

module.exports = {
  up: queryInterface => {
    return queryInterface.bulkInsert(
      'Stores',
      [
        {
          id: 1,
          name: 'Pizzería Gómez',
          alias: 'pizzeria.gomez',
          category: 'gastronomy',
          profileImage: 'testing-files/pizzeria-gomez.jpg',
          description:
            'Somos un negocio familiar que con mucho trabajo y esfuerzo fuimos creciendo para poder dar conocer a nuestras fantabulosas pizzas. No nos gusta alardear, pero el mejor plato del menú es una exquisita pizza rellena que te dejara totalmente pachorra.',
          socialMedia: `{
        "instagram": "https://www.instagram.com/pizzeriagomez/",
        "facebook": "https://www.facebook.com/PizzeriaGomez/",
        "twitter": "https://twitter.com/PizzeriaGomez"
      }`,
          countryCode: COUNTRY_CODES.ARGENTINA,
          private: true,
          fixedLevels: true,
          cardModule: true,
          createdAt: '2020-08-01 12:00:00',
          updatedAt: '2020-08-01 12:00:00',
        },
        {
          id: 2,
          name: 'Heladería Sabor',
          alias: 'heladeria.sabor',
          category: 'gastronomy',
          profileImage: 'testing-files/heladeria-sabor.jpg',
          description:
            'Conocé "Sabor" donde la alquimia de lo artesanal, las recetas familiares atesoradas por generaciones y las mejores materias primas se conjugan en un helado artesanal delicioso, fresco y natural.',
          socialMedia: `{
        "instagram": "https://www.instagram.com/heladeriasaborok/",
        "facebook": "https://www.facebook.com/HeladeriaSabor/",
        "twitter": "https://twitter.com/HeladeriaSabor"
      }`,
          countryCode: COUNTRY_CODES.ARGENTINA,
          createdAt: '2020-08-01 12:00:00',
          updatedAt: '2020-08-01 12:00:00',
        },
        {
          id: 3,
          name: 'Café Los Hermanos',
          alias: 'cafe.los.hermanos',
          category: 'gastronomy',
          profileImage: 'testing-files/cafe-los-hermanos.jpg',
          description:
            'Los tres hermanos: Mateo, Miguel y Sergio llegaron al barrio de Chacarita en 1958 de Santiago de Compostela para hacerse cargo del espacio que funciona desde 1914 y que anteriormente tuvo diferentes nombres. El Café supo abrir las 24 hs cuando la clientela se nutría de los talleres de ropa del barrio y por las noches era parada obligada de músicos de orquestas de tango. Hoy Café Los Hermanos es un Café de lectura de diarios, especialmente el suplemento “Carreras”, y oficina al paso. Para los amantes de la bicicleta el lugar es Bike-friendly.',
          socialMedia: `{
        "instagram": "https://www.instagram.com/cafeloshermanos/",
        "facebook": "https://www.facebook.com/CafeLosHermanos/"
      }`,
          countryCode: COUNTRY_CODES.ARGENTINA,
          createdAt: '2020-08-01 12:00:00',
          updatedAt: '2020-08-01 12:00:00',
        },
        {
          id: 4,
          name: 'Decoración Luna',
          alias: 'decoracion.luna',
          category: 'dress',
          profileImage: 'testing-files/decoracion-luna.jpg',
          description:
            'Somos uno de los Bazares Boutique más top de Argentina. Fundado en el año 2003, fuimos pioneros en Palermo Soho. Contamos con una gran variedad de artículos bajo el concepto de: simplicidad, practicidad y belleza, que definen a Decoración Luna desde sus comienzos.',
          socialMedia: `{
        "instagram": "https://www.instagram.com/decoracionluna/",
        "facebook": "https://www.facebook.com/luna.decoracion/",
        "twitter": "https://twitter.com/DecoracionLuna"
      }`,
          countryCode: COUNTRY_CODES.ARGENTINA,
          createdAt: '2020-08-01 12:00:00',
          updatedAt: '2020-08-01 12:00:00',
        },
        {
          id: 6,
          name: 'Tu Comercio',
          alias: 'tu.comercio',
          category: 'dress',
          profileImage: 'testing-files/decoracion-luna.jpg',
          description:
            'Descripción de tu negocio. En este espacio podrás contar la historia de su comercio, su filosofía, su diferencial de productos, o cualquier otro aspecto que te parezca relevante.',
          socialMedia: `{"facebook": "https://www.facebook.com/okro.ar/", "whatsapp": "https://wa.me/5491166062501", "instagram": "https://www.instagram.com/okro.ar/"}`,
          countryCode: COUNTRY_CODES.ARGENTINA,
          createdAt: '2020-08-01 12:00:00',
          updatedAt: '2020-08-01 12:00:00',
        },
        {
          id: 7,
          name: 'Comercio Tests',
          alias: 'tests.comercio',
          category: 'dress',
          profileImage: 'testing-files/decoracion-luna.jpg',
          description:
            'Descripción de tu negocio. En este espacio podrás contar la historia de su comercio, su filosofía, su diferencial de productos, o cualquier otro aspecto que te parezca relevante.',
          socialMedia: `{"facebook": "https://www.facebook.com/okro.ar/", "whatsapp": "https://wa.me/5491166062501", "instagram": "https://www.instagram.com/okro.ar/"}`,
          countryCode: COUNTRY_CODES.ARGENTINA,
          createdAt: '2020-08-01 12:00:00',
          updatedAt: '2020-08-01 12:00:00',
        },
      ],
      {},
    );
  },

  down: queryInterface => {
    return queryInterface.bulkDelete('Stores', null, {});
  },
};
