'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CustomerProfiles', 'cardId', {
      type: Sequelize.STRING,
      allowNull: true,
      after: 'status',
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('CustomerProfiles', 'cardId');
  },
};
