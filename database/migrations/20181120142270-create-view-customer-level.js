const view_name = 'CustomerLevel';
const query = `
  SELECT
  p.customerId,
  p.storeId,
  MAX(st.level) as level,
  MAX(st.points) as currentLevelPoints,
  IF(MAX(st.level) = 3, 0, MAX(st.levelDifference)) as nextLevelPointsDif,
  MAX(st.levelPoints) as nextLevelPoints,
  COUNT(StoreRewards.id) as activeRewards,
  p.total
  FROM (
    SELECT
      CustomerProfiles.id as customerId,
        CustomerProfiles.storeId,
        COALESCE(SUM(Points.amount) ,0) as total
    FROM CustomerProfiles
      LEFT JOIN Points ON Points.customerId = CustomerProfiles.id
    GROUP BY CustomerProfiles.id
  ) p
  INNER JOIN (
    SELECT
    StoreLevels.id,
    StoreLevels.level,
    StoreLevels.points,
    StoreLevels.storeId,
    IFNULL(ST.points, StoreLevels.points) as levelPoints,
    (IFNULL(ST.points, StoreLevels.points) - StoreLevels.points) as levelDifference
    FROM StoreLevels
    LEFT JOIN StoreLevels as ST ON (ST.storeId = StoreLevels.storeId AND ST.level = (StoreLevels.level + 1))
  ) st ON st.storeId = p.storeId
  LEFT JOIN StoreRewards ON StoreRewards.storeLevelId = st.id
  WHERE p.total >= st.points
  GROUP BY p.customerId;
`;

module.exports = {
  up: function (queryInterface) {
    return queryInterface.sequelize.query(`CREATE OR REPLACE VIEW ${view_name} AS ${query}`);
  },
  down: function (queryInterface) {
    return queryInterface.sequelize.query(`DROP VIEW ${view_name}`);
  },
};
