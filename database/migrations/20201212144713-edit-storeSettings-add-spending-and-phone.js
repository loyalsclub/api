'use strict';

const addColumnPersonalPhone = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('StoreSettings', 'personalPhone', {
      type: Sequelize.STRING,
      after: 'storeId',
    });
  },
  down: queryInterface => queryInterface.removeColumn('StoreSettings', 'personalPhone'),
};

const addColumnSpendingTimes = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('StoreSettings', 'spendingTimes', {
      type: Sequelize.STRING,
      after: 'personalPhone',
    });
  },
  down: queryInterface => queryInterface.removeColumn('StoreSettings', 'spendingTimes'),
};

const addColumnSpendingAmount = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('StoreSettings', 'spendingAmount', {
      type: Sequelize.INTEGER,
      after: 'spendingTimes',
    });
  },
  down: queryInterface => queryInterface.removeColumn('StoreSettings', 'spendingAmount'),
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await addColumnPersonalPhone.up(queryInterface, Sequelize);
    await addColumnSpendingTimes.up(queryInterface, Sequelize);
    await addColumnSpendingAmount.up(queryInterface, Sequelize);
  },

  down: async queryInterface => {
    await addColumnSpendingAmount.down(queryInterface);
    await addColumnSpendingTimes.down(queryInterface);
    await addColumnPersonalPhone.down(queryInterface);
  },
};
