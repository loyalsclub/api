'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Users', 'pushToken', {
      type: Sequelize.STRING,
      after: 'role',
    });
  },

  down: async queryInterface => {
    return queryInterface.removeColumn('Users', 'pushToken');
  },
};
