'use strict';

const { COUNTRY_CODES } = require('../../src/modules/store/constants/store.constants');

const addColumnCountryCode = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Stores', 'countryCode', {
      type: Sequelize.CHAR(2),
      after: 'socialMedia',
    });
  },
  down: queryInterface => queryInterface.removeColumn('Stores', 'countryCode'),
};

const populateColumnCountryCode = {
  up: queryInterface => {
    return queryInterface.sequelize.query(`UPDATE Stores SET countryCode = '${COUNTRY_CODES.ARGENTINA}'`, {
      type: queryInterface.sequelize.QueryTypes.UPDATE,
    });
  },
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await addColumnCountryCode.up(queryInterface, Sequelize);
    await populateColumnCountryCode.up(queryInterface);
  },

  down: async queryInterface => {
    await addColumnCountryCode.down(queryInterface);
  },
};
