'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Users', 'webPushSubscription', {
      type: Sequelize.JSON,
      after: 'pushToken',
    });
  },

  down: async queryInterface => {
    return queryInterface.removeColumn('Users', 'webPushSubscription');
  },
};
