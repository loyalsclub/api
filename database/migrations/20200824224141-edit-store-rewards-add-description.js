'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('StoreRewards', 'description', {
      type: Sequelize.STRING,
      after: 'title',
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('StoreRewards', 'description');
  },
};
