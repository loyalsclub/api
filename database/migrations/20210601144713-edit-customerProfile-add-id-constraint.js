'use strict';

const { CONSTRAINT_USER_STORE_UNIQUE } = require('../../src/modules/customer/constants/customer.constants');

module.exports = {
  up: async queryInterface => {
    return queryInterface.addConstraint('CustomerProfiles', {
      type: 'unique',
      fields: ['userId', 'storeId'],
      name: CONSTRAINT_USER_STORE_UNIQUE,
    });
  },

  down: async queryInterface => {
    return queryInterface.removeConstraint('CustomerProfiles', CONSTRAINT_USER_STORE_UNIQUE);
  },
};
