'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn(
          'Stores',
          'private',
          { type: Sequelize.DataTypes.BOOLEAN, defaultValue: false, after: 'countryCode' },
          { transaction: t },
        ),
        queryInterface.addColumn(
          'Stores',
          'fixedLevels',
          { type: Sequelize.DataTypes.BOOLEAN, defaultValue: false, after: 'private' },
          { transaction: t },
        ),
        queryInterface.addColumn(
          'Stores',
          'cardModule',
          { type: Sequelize.DataTypes.BOOLEAN, defaultValue: false, after: 'fixedLevels' },
          { transaction: t },
        ),
      ]);
    });
  },

  down: queryInterface => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('Stores', 'private', { transaction: t }),
        queryInterface.removeColumn('Stores', 'fixedLevels', { transaction: t }),
        queryInterface.removeColumn('Stores', 'cardModule', { transaction: t }),
      ]);
    });
  },
};
