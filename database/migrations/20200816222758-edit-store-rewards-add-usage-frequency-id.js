'use strict';
const moment = require('moment');

const { USAGE_FREQUENCIES } = require('../../src/modules/core/constants/core.constants');

const addColumnUsageFrequencyId = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('StoreRewards', 'usageFrequencyId', {
      type: Sequelize.STRING,
      after: 'title',
    });
  },
  down: queryInterface => queryInterface.removeColumn('StoreRewards', 'usageFrequencyId'),
};

const insertStoreRewardsMissingUsageFrequencies = {
  up: async queryInterface => {
    const storeRewards = await queryInterface.sequelize.query(
      'SELECT * FROM StoreRewards WHERE usageFrequencyId is NULL',
      {
        type: queryInterface.sequelize.QueryTypes.SELECT,
      },
    );

    const lastUsageFrequency = await queryInterface.sequelize.query(
      'SELECT max(id) as lastUsageFrequencyId FROM UsageFrequencies',
      {
        type: queryInterface.sequelize.QueryTypes.SELECT,
      },
    );

    const nextUsageFrequencyId = lastUsageFrequency[0].lastUsageFrequencyId + 1;

    const newUsageFrequencies = storeRewards.map((_, index) => ({
      id: nextUsageFrequencyId + index,
      type: USAGE_FREQUENCIES.FOREVER,
      createdAt: moment().format('YYYY-MM-DD hh:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD hh:mm:ss'),
    }));

    if (newUsageFrequencies.length > 0) {
      await queryInterface.bulkInsert('UsageFrequencies', newUsageFrequencies);

      await Promise.all(
        storeRewards.map((storeReward, index) =>
          queryInterface.sequelize.query(
            `UPDATE StoreRewards SET usageFrequencyId='${nextUsageFrequencyId + index}' WHERE id=${storeReward.id}`,
            {
              type: queryInterface.sequelize.QueryTypes.UPDATE,
            },
          ),
        ),
      );
    }
  },
  down: async queryInterface => {
    const storeRewards = await queryInterface.sequelize.query(
      'SELECT usageFrequencyId FROM StoreRewards WHERE usageFrequencyId is NOT NULL',
      {
        type: queryInterface.sequelize.QueryTypes.SELECT,
      },
    );

    const ids = storeRewards
      .map(({ usageFrequencyId }) => usageFrequencyId)
      .filter(Boolean)
      .join(',');

    if (ids) {
      await queryInterface.sequelize.query(`DELETE FROM UsageFrequencies WHERE id IN (${ids})`, {
        type: queryInterface.sequelize.QueryTypes.DELETE,
      });
    }
  },
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await addColumnUsageFrequencyId.up(queryInterface, Sequelize);
    await insertStoreRewardsMissingUsageFrequencies.up(queryInterface, Sequelize);
  },

  down: async queryInterface => {
    await insertStoreRewardsMissingUsageFrequencies.down(queryInterface);
    await addColumnUsageFrequencyId.down(queryInterface);
  },
};
