'use strict';
const moment = require('moment');

const { USAGE_FREQUENCIES } = require('../../src/modules/core/constants/core.constants');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Coupons', 'usageFrequencyId', {
      type: Sequelize.STRING,
      after: 'points',
    });

    const coupons = await queryInterface.sequelize.query('SELECT * FROM Coupons', {
      type: queryInterface.sequelize.QueryTypes.SELECT,
    });

    const newUsageFrequencies = coupons.map((_, index) => ({
      id: index + 1,
      type: USAGE_FREQUENCIES.FOREVER,
      createdAt: moment().format('YYYY-MM-DD hh:mm:ss'),
      updatedAt: moment().format('YYYY-MM-DD hh:mm:ss'),
    }));

    if (newUsageFrequencies.length > 0) {
      await queryInterface.bulkInsert('UsageFrequencies', newUsageFrequencies);

      await Promise.all(
        coupons.map((coupon, index) =>
          queryInterface.sequelize.query(`UPDATE Coupons SET usageFrequencyId='${index + 1}' WHERE id=${coupon.id}`, {
            type: queryInterface.sequelize.QueryTypes.UPDATE,
          }),
        ),
      );
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UsageFrequencies', { id: { [Sequelize.Op.ne]: null } });
    await queryInterface.removeColumn('Coupons', 'usageFrequencyId');
  },
};
