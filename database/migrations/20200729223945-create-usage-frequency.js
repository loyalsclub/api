'use strict';

const { USAGE_FREQUENCIES } = require('../../src/modules/core/constants/core.constants');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UsageFrequencies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      type: {
        type: Sequelize.ENUM(
          USAGE_FREQUENCIES.FOREVER,
          USAGE_FREQUENCIES.ONE_TIME,
          USAGE_FREQUENCIES.EVERY_CERTAIN_DAYS,
          USAGE_FREQUENCIES.EVERY_CERTAIN_HOURS,
          USAGE_FREQUENCIES.CERTAIN_WEEK_DAYS,
        ),
        defaultValue: USAGE_FREQUENCIES.FOREVER,
        allowNull: false,
      },
      value: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('UsageFrequencies');
  },
};
